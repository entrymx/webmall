<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlazasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plazas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apertura');
            $table->string('cierre');
            $table->string('logo');
            
            $table->integer('ubicacion_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('plazas', function(Blueprint $table)
        {
            $table->foreign('ubicacion_id')->references('id')->on('ubicacions');

        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plazas');
    }
}
