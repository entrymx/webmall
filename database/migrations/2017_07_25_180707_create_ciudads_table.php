<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCiudadsTable extends Migration
{

    public function up()
    {
        Schema::create('ciudads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');         
            
            $table->integer('estado_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('ciudads', function(Blueprint $table)
        {
            $table->foreign('estado_id')->references('id')->on('estados');

        }); 

    }


    public function down()
    {
        Schema::dropIfExists('ciudads');
    }
}
