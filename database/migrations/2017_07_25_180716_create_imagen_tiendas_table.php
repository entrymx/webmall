<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagenTiendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagen_tiendas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagen');
           
            $table->integer('tienda_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('imagen_tiendas', function(Blueprint $table)
        {
            $table->foreign('tienda_id')->references('id')->on('tiendas');

        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagen_tiendas');
    }
}
