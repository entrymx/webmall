<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUbicacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ubicacions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('domicilio');
            $table->double('latitud');
            $table->double('longitud');
                        
            $table->integer('ciudad_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('ubicacions', function(Blueprint $table)
        {
            $table->foreign('ciudad_id')->references('id')->on('ciudads');

        });         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ubicacions');
    }
}
