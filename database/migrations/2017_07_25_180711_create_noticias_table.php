<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticiasTable extends Migration
{

    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('imagen');
            $table->string('descripcion');
            $table->string('fecha');
            $table->char('tipo', 1);

            $table->integer('plaza_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('noticias', function(Blueprint $table)
        {
            $table->foreign('plaza_id')->references('id')->on('plazas');

        }); 
    }


    public function down()
    {
        Schema::dropIfExists('noticias');
    }
}
