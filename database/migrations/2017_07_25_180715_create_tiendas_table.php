<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiendasTable extends Migration
{

    public function up()
    {
        Schema::create('tiendas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('logo');
            $table->string('apertura');
            $table->string('cierre');
            $table->char('visto', 1);

            $table->integer('categoria_id')->unsigned();
            $table->integer('local_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('tiendas', function(Blueprint $table)
        {
            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->foreign('local_id')->references('id')->on('locals');
            $table->foreign('user_id')->references('id')->on('users');

        }); 
    }
    public function down()
    {
        Schema::dropIfExists('tiendas');
    }
}
