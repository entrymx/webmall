<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagenPlazasTable extends Migration
{

    public function up()
    {
        Schema::create('imagen_plazas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagen');

            $table->integer('plaza_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('imagen_plazas', function(Blueprint $table)
        {
            $table->foreign('plaza_id')->references('id')->on('plazas');

        }); 
    }


    public function down()
    {
        Schema::dropIfExists('imagen_plazas');
    }
}
