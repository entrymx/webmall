<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagenOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagen_owners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagen');
           
            $table->integer('owner_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('imagen_owners', function(Blueprint $table)
        {
            $table->foreign('owner_id')->references('id')->on('owners');

        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagen_owners');
    }
}
