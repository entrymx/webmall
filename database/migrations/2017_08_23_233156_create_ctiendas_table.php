<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCtiendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ctiendas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombres');
            $table->string('apellidos');
            $table->string('correo');
            $table->string('telefono');

            $table->integer('tienda_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('ctiendas', function(Blueprint $table)
        {
            $table->foreign('tienda_id')->references('id')->on('tiendas');

        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ctiendas');
    }
}
