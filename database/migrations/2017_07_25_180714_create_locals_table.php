<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalsTable extends Migration
{

    public function up()
    {
        Schema::create('locals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('estado');

            $table->integer('plaza_id')->unsigned();
            $table->integer('tipo_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('locals', function(Blueprint $table)
        {
            $table->foreign('plaza_id')->references('id')->on('plazas');
            $table->foreign('tipo_id')->references('id')->on('tipos');

        }); 
    }

    public function down()
    {
        Schema::dropIfExists('locals');
    }
}
