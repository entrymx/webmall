<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnersTable extends Migration
{

    public function up()
    {
        Schema::create('owners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');

            $table->string('apertura');
            $table->string('cierre');

			$table->string('categoria');
            $table->string('tags');

            $table->string('comentarios');
            
            $table->string('logo');
            
            $table->integer('tienda_id')->unsigned();
            //$table->integer('user_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('owners', function(Blueprint $table)
        {
            //$table->foreign('categoria_id')->references('id')->on('categorias');
            $table->foreign('tienda_id')->references('id')->on('tiendas');
            //$table->foreign('user_id')->references('id')->on('user');

        }); 
    }
    public function down()
    {
        Schema::dropIfExists('owners');
    }
}
