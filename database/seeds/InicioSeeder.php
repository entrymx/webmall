<?php

use Illuminate\Database\Seeder;

class InicioSeeder extends Seeder
{

    public function run()
    {

	//Estados
		\DB::table('estados')->insert([
			[	'nombre' => 'Durango',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'Jalisco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'Nuevo León',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'CDMX',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'Quintana Roo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'Guanajuato',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			]
		]);
	//------------------------------------------------------------------------


	//Ciudades
		\DB::table('ciudads')->insert([
			[	'nombre' => 'Durango',
                'estado_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'Gomez Palacio',
                'estado_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'Guadalajara',
                'estado_id' => '2',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'Tonalá',
                'estado_id' => '2',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'Monterrey',
                'estado_id' => '3',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'San Nicolás',
                'estado_id' => '3',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'Miguél Hidalgo',
                'estado_id' => '4',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'Benito Juarez',
                'estado_id' => '4',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'Cancún',
                'estado_id' => '5',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'Cozumel',
                'estado_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'León',
                'estado_id' => '4',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			],
			[	'nombre' => 'Guanajuato',
                'estado_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
			]
		]);
	//------------------------------------------------------------------------


    //Ubicaciones
        \DB::table('ubicacions')->insert([
            [   'domicilio' => 'Felipe Pescador',
                'latitud' => '24.0359155',
                'longitud' => '-104.6518712',
                'ciudad_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);
    //------------------------------------------------------------------------


    //Tags
        \DB::table('tags')->insert([
            [   'nombre' => 'Comida',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Restaurant',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Café',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Hamburguesa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Pizza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'HotDog',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Ropa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Deporte',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Dama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Caballero',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Mascota',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);
    //------------------------------------------------------------------------


    //Categoría (de tienda)
        \DB::table('categorias')->insert([
            [   'nombre' => 'Restaurant',
                'imagen' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Ropa',
                'imagen' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Departamental',
                'imagen' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Cine',
                'imagen' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Fast Food',
                'imagen' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);
    //------------------------------------------------------------------------

    //Tipos (de tienda)
        \DB::table('tipos')->insert([
            [   'nombre' => 'Chico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Mediano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [   'nombre' => 'Grande',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);
    //------------------------------------------------------------------------

    //Plazas (de tienda)
        \DB::table('plazas')->insert([
            [   'nombre' => 'Paseo Durango',
                'apertura' => '11:00',
                'cierre' => '21:00',
                'logo' => '',
                'ubicacion_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);
    //------------------------------------------------------------------------


    }
}
