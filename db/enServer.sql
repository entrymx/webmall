-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 25-08-2017 a las 18:00:07
-- Versión del servidor: 5.7.19-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.22-2+ubuntu16.04.1+deb.sury.org+4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de datos: `webmall`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `imagen`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ropa y Accesorios para Dama', '300.png', '2017-08-23 00:54:10', '2017-08-23 00:54:10', NULL),
(2, 'Ropa para Caballeros', '300.png', '2017-08-23 00:54:23', '2017-08-23 00:54:23', NULL),
(3, 'Restaurantes', '300.png', '2017-08-23 00:54:34', '2017-08-23 00:54:34', NULL),
(4, 'Bancos', '', '2017-08-26 00:29:34', '2017-08-26 00:29:34', NULL),
(5, 'Bar', '', '2017-08-26 00:29:53', '2017-08-26 00:29:53', NULL),
(6, 'Departamental', '', '2017-08-26 00:30:16', '2017-08-26 00:30:16', NULL),
(7, 'Cine', '', '2017-08-26 00:30:22', '2017-08-26 00:30:22', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudads`
--

CREATE TABLE `ciudads` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ciudads`
--

INSERT INTO `ciudads` (`id`, `nombre`, `estado_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Victoria de Durango', 1, '2017-08-22 23:57:48', '2017-08-22 23:57:48', NULL),
(2, 'Monterrey', 2, '2017-08-22 23:57:56', '2017-08-22 23:57:56', NULL),
(3, 'San Nicolás de los Garza', 2, '2017-08-22 23:59:35', '2017-08-22 23:59:40', NULL),
(4, 'San Pedro Garza García', 2, '2017-08-22 23:59:57', '2017-08-22 23:59:57', NULL),
(5, 'Guadalajara', 4, '2017-08-25 21:38:53', '2017-08-25 21:38:53', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE `contactos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plaza_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ctiendas`
--

CREATE TABLE `ctiendas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tienda_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Durango', '2017-08-22 23:57:30', '2017-08-22 23:57:30', NULL),
(2, 'Nuevo León', '2017-08-22 23:57:35', '2017-08-22 23:57:35', NULL),
(3, 'Queretaro', '2017-08-22 23:57:39', '2017-08-22 23:57:39', NULL),
(4, 'Jalisco', '2017-08-25 21:37:49', '2017-08-25 21:37:49', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen_plazas`
--

CREATE TABLE `imagen_plazas` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plaza_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `imagen_plazas`
--

INSERT INTO `imagen_plazas` (`id`, `imagen`, `plaza_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'paseo_durango.jpg', 1, '2017-08-23 00:41:39', '2017-08-23 00:41:39', NULL),
(2, 'paseo_sanpedro.jpg', 3, '2017-08-23 00:43:02', '2017-08-23 00:43:02', NULL),
(3, 'galerias_monterrey.jpg', 4, '2017-08-23 00:45:34', '2017-08-23 00:45:34', NULL),
(4, '269b0cfd9f0d9f12f91a801d3f67b8381.jpg', 5, '2017-08-25 23:45:06', '2017-08-25 23:45:06', NULL),
(5, '147c7a7f8b5c766e719ebd83d9d003cfe.jpg', 5, '2017-08-25 23:45:06', '2017-08-25 23:45:06', NULL),
(6, 'bf3685318fa1f01cc429562bf864eda7.webp', 6, '2017-08-25 23:54:14', '2017-08-25 23:54:14', NULL),
(7, '03262ceaaadbbefbf1a9f71c25d05f11u.jpg', 6, '2017-08-25 23:54:14', '2017-08-25 23:54:14', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen_tiendas`
--

CREATE TABLE `imagen_tiendas` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tienda_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `locals`
--

CREATE TABLE `locals` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plaza_id` int(10) UNSIGNED NOT NULL,
  `tipo_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `locals`
--

INSERT INTO `locals` (`id`, `nombre`, `estado`, `plaza_id`, `tipo_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'GM-A001', '0', 4, 2, '2017-08-23 00:47:09', '2017-08-23 00:47:09', NULL),
(2, 'GM-A002', '0', 4, 2, '2017-08-23 00:47:25', '2017-08-23 00:47:25', NULL),
(3, 'PD-001', '0', 1, 1, '2017-08-23 00:47:36', '2017-08-23 00:50:57', NULL),
(4, 'PD-002', '0', 1, 1, '2017-08-23 00:48:41', '2017-08-23 00:50:42', NULL),
(5, 'PSP-A001', '0', 3, 1, '2017-08-23 00:51:17', '2017-08-23 00:51:17', NULL),
(6, 'PSP-A002', '0', 3, 1, '2017-08-23 00:51:28', '2017-08-23 00:51:28', NULL),
(7, 'GM-A003', '0', 5, 4, '2017-08-26 00:00:56', '2017-08-26 00:00:56', NULL),
(8, 'GM-A004', '0', 4, 1, '2017-08-26 00:01:23', '2017-08-26 00:03:36', NULL),
(9, 'PD-003', '0', 1, 3, '2017-08-26 00:02:46', '2017-08-26 00:02:46', NULL),
(10, 'PD-004', '0', 1, 5, '2017-08-26 00:03:06', '2017-08-26 00:03:06', NULL),
(11, 'PSP-001', '0', 3, 2, '2017-08-26 00:04:16', '2017-08-26 00:04:16', NULL),
(12, 'PSP-002', '0', 3, 5, '2017-08-26 00:04:26', '2017-08-26 00:04:26', NULL),
(13, 'PSP-003', '0', 3, 4, '2017-08-26 00:04:37', '2017-08-26 00:04:37', NULL),
(14, 'VD-001', '0', 6, 1, '2017-08-26 00:11:32', '2017-08-26 00:11:32', NULL),
(15, 'VD-002', '0', 6, 1, '2017-08-26 00:12:11', '2017-08-26 00:12:11', NULL),
(16, 'VD-003', '0', 6, 1, '2017-08-26 00:13:00', '2017-08-26 00:13:00', NULL),
(17, 'VD-004', '0', 6, 2, '2017-08-26 00:13:28', '2017-08-26 00:13:28', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_07_02_230147_migration_cartalyst_sentinel', 1),
(2, '2017_07_25_180652_create_tipos_table', 1),
(3, '2017_07_25_180703_create_categorias_table', 1),
(4, '2017_07_25_180704_create_tags_table', 1),
(5, '2017_07_25_180705_create_estados_table', 1),
(6, '2017_07_25_180707_create_ciudads_table', 1),
(7, '2017_07_25_180708_create_ubicacions_table', 1),
(8, '2017_07_25_180709_create_plazas_table', 1),
(9, '2017_07_25_180711_create_noticias_table', 1),
(10, '2017_07_25_180712_create_imagen_plazas_table', 1),
(11, '2017_07_25_180714_create_locals_table', 1),
(12, '2017_07_25_180715_create_tiendas_table', 1),
(13, '2017_07_25_180716_create_imagen_tiendas_table', 1),
(14, '2017_07_25_180718_create_tag_tienda_table', 1),
(15, '2017_07_26_152154_create_watchdogs_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plaza_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plazas`
--

CREATE TABLE `plazas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apertura` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cierre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ubicacion_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `plazas`
--

INSERT INTO `plazas` (`id`, `nombre`, `apertura`, `cierre`, `logo`, `ubicacion_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Paseo Durango', '11:00', '21:00', '9ecdf921e863d462389e996b2c5dcf00o.png', 3, '2017-08-23 00:41:39', '2017-08-26 00:58:52', NULL),
(2, 'Galerías Monterrey', '', '', '', 2, '2017-08-23 00:42:37', '2017-08-23 00:45:13', '2017-08-23 00:45:13'),
(3, 'Paseo San Pedro', '10:00', '22:00', 'b92d30bb28f6824a611b64446304dac3o.jpg', 1, '2017-08-23 00:43:02', '2017-08-26 00:59:28', NULL),
(4, 'Galerias Monterrey', '09:00', '23:00', '2d1300001f59849ee06b25bd182d99d9d.jpg', 2, '2017-08-23 00:45:34', '2017-08-25 23:57:48', NULL),
(5, 'Expo Guadalajara', '00:00', '00:00', '170a4bbdf84642058baae47e0dd3fa88a.jpg', 4, '2017-08-25 23:45:06', '2017-08-26 00:59:47', NULL),
(6, 'Veranda', '00:00', '00:00', '', 5, '2017-08-25 23:54:14', '2017-08-25 23:54:14', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tags`
--

INSERT INTO `tags` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Banco', '2017-08-26 00:15:01', '2017-08-26 00:15:01', NULL),
(2, 'Deposito', '2017-08-26 00:15:30', '2017-08-26 00:15:30', NULL),
(3, 'Retiro', '2017-08-26 00:15:43', '2017-08-26 00:15:43', NULL),
(4, 'Comida', '2017-08-26 00:15:52', '2017-08-26 00:15:52', NULL),
(5, 'Fast Food', '2017-08-26 00:16:02', '2017-08-26 00:16:02', NULL),
(6, 'Hambuguesa', '2017-08-26 00:16:11', '2017-08-26 00:16:11', NULL),
(7, 'Pizza', '2017-08-26 00:16:21', '2017-08-26 00:16:21', NULL),
(8, 'Donas', '2017-08-26 00:16:25', '2017-08-26 00:16:25', NULL),
(9, 'Café', '2017-08-26 00:16:32', '2017-08-26 00:16:32', NULL),
(10, 'Smoothies', '2017-08-26 00:17:10', '2017-08-26 00:17:10', NULL),
(11, 'Carne', '2017-08-26 00:17:52', '2017-08-26 00:17:52', NULL),
(12, 'Verde', '2017-08-26 00:17:58', '2017-08-26 00:17:58', NULL),
(13, 'Vegano', '2017-08-26 00:18:07', '2017-08-26 00:18:07', NULL),
(14, 'Ensalada', '2017-08-26 00:18:13', '2017-08-26 00:18:13', NULL),
(15, 'Ensalada', '2017-08-26 00:18:16', '2017-08-26 00:18:16', NULL),
(16, 'Ropa', '2017-08-26 00:18:28', '2017-08-26 00:18:28', NULL),
(17, 'Dama', '2017-08-26 00:18:33', '2017-08-26 00:18:33', NULL),
(18, 'Caballero', '2017-08-26 00:18:38', '2017-08-26 00:18:38', NULL),
(19, 'Bar', '2017-08-26 00:18:58', '2017-08-26 00:18:58', NULL),
(20, 'Cerveza', '2017-08-26 00:19:03', '2017-08-26 00:19:03', NULL),
(21, 'Alcohol', '2017-08-26 00:19:11', '2017-08-26 00:19:11', NULL),
(22, 'Alitas', '2017-08-26 00:19:15', '2017-08-26 00:19:15', NULL),
(23, 'Papas', '2017-08-26 00:19:22', '2017-08-26 00:19:22', NULL),
(24, 'Exposiciones', '2017-08-26 00:19:41', '2017-08-26 00:19:41', NULL),
(25, 'Mascotas', '2017-08-26 00:33:36', '2017-08-26 00:33:36', NULL),
(26, 'Perro', '2017-08-26 00:33:40', '2017-08-26 00:33:40', NULL),
(27, 'Gato', '2017-08-26 00:33:45', '2017-08-26 00:33:45', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tag_tienda`
--

CREATE TABLE `tag_tienda` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL,
  `tienda_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tag_tienda`
--

INSERT INTO `tag_tienda` (`id`, `tag_id`, `tienda_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 16, 1, NULL, NULL, NULL),
(2, 17, 1, NULL, NULL, NULL),
(3, 18, 1, NULL, NULL, NULL),
(4, 1, 2, NULL, NULL, NULL),
(5, 2, 2, NULL, NULL, NULL),
(6, 3, 2, NULL, NULL, NULL),
(7, 25, 3, NULL, NULL, NULL),
(8, 26, 3, NULL, NULL, NULL),
(9, 27, 3, NULL, NULL, NULL),
(10, 4, 4, NULL, NULL, NULL),
(11, 5, 4, NULL, NULL, NULL),
(12, 6, 4, NULL, NULL, NULL),
(13, 1, 5, NULL, NULL, NULL),
(14, 2, 5, NULL, NULL, NULL),
(15, 3, 5, NULL, NULL, NULL),
(16, 1, 6, NULL, NULL, NULL),
(17, 2, 6, NULL, NULL, NULL),
(18, 3, 6, NULL, NULL, NULL),
(19, 1, 7, NULL, NULL, NULL),
(20, 2, 7, NULL, NULL, NULL),
(21, 3, 7, NULL, NULL, NULL),
(22, 1, 8, NULL, NULL, NULL),
(23, 2, 8, NULL, NULL, NULL),
(24, 3, 8, NULL, NULL, NULL),
(25, 4, 9, NULL, NULL, NULL),
(26, 5, 9, NULL, NULL, NULL),
(27, 7, 9, NULL, NULL, NULL),
(28, 11, 9, NULL, NULL, NULL),
(29, 16, 10, NULL, NULL, NULL),
(30, 17, 10, NULL, NULL, NULL),
(31, 18, 10, NULL, NULL, NULL),
(32, 4, 11, NULL, NULL, NULL),
(33, 5, 11, NULL, NULL, NULL),
(34, 6, 11, NULL, NULL, NULL),
(35, 11, 11, NULL, NULL, NULL),
(36, 19, 11, NULL, NULL, NULL),
(37, 20, 11, NULL, NULL, NULL),
(38, 21, 11, NULL, NULL, NULL),
(39, 22, 11, NULL, NULL, NULL),
(40, 23, 11, NULL, NULL, NULL),
(41, 4, 12, NULL, NULL, NULL),
(42, 5, 12, NULL, NULL, NULL),
(43, 6, 12, NULL, NULL, NULL),
(44, 11, 12, NULL, NULL, NULL),
(45, 19, 12, NULL, NULL, NULL),
(46, 20, 12, NULL, NULL, NULL),
(47, 21, 12, NULL, NULL, NULL),
(48, 22, 12, NULL, NULL, NULL),
(49, 23, 12, NULL, NULL, NULL),
(50, 4, 13, NULL, NULL, NULL),
(51, 5, 13, NULL, NULL, NULL),
(52, 6, 13, NULL, NULL, NULL),
(53, 11, 13, NULL, NULL, NULL),
(54, 19, 13, NULL, NULL, NULL),
(55, 20, 13, NULL, NULL, NULL),
(56, 21, 13, NULL, NULL, NULL),
(57, 22, 13, NULL, NULL, NULL),
(58, 23, 13, NULL, NULL, NULL),
(59, 4, 14, NULL, NULL, NULL),
(60, 5, 14, NULL, NULL, NULL),
(61, 6, 14, NULL, NULL, NULL),
(62, 11, 14, NULL, NULL, NULL),
(63, 19, 14, NULL, NULL, NULL),
(64, 20, 14, NULL, NULL, NULL),
(65, 21, 14, NULL, NULL, NULL),
(66, 22, 14, NULL, NULL, NULL),
(67, 23, 14, NULL, NULL, NULL),
(68, 4, 15, NULL, NULL, NULL),
(69, 5, 15, NULL, NULL, NULL),
(70, 6, 15, NULL, NULL, NULL),
(71, 11, 15, NULL, NULL, NULL),
(72, 23, 15, NULL, NULL, NULL),
(73, 4, 16, NULL, NULL, NULL),
(74, 5, 16, NULL, NULL, NULL),
(75, 7, 16, NULL, NULL, NULL),
(76, 11, 16, NULL, NULL, NULL),
(77, 23, 16, NULL, NULL, NULL),
(78, 25, 17, NULL, NULL, NULL),
(79, 26, 17, NULL, NULL, NULL),
(80, 27, 17, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiendas`
--

CREATE TABLE `tiendas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cierre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apertura` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categoria_id` int(10) UNSIGNED NOT NULL,
  `local_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tiendas`
--

INSERT INTO `tiendas` (`id`, `nombre`, `cierre`, `apertura`, `logo`, `categoria_id`, `local_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Liverpool', '00:00', '00:00', '6b829aa516fdd2df489085e4da75f99b3.jpg', 6, 3, '2017-08-26 00:32:07', '2017-08-26 00:32:07', NULL),
(2, 'Banorte', '00:00', '00:00', '13ef4b4f4c3ddb0d1cac91115bcefc3ae.gif', 4, 4, '2017-08-26 00:32:39', '2017-08-26 00:32:39', NULL),
(3, '+Cota', '00:00', '00:00', 'be7bba27870e5c959631355e4c21c24f2.png', 6, 9, '2017-08-26 00:35:06', '2017-08-26 01:21:24', NULL),
(4, 'Burguer King', '00:00', '00:00', '5cbfffee1d302f23407389d2f7e0aa2fd.png', 3, 10, '2017-08-26 00:40:13', '2017-08-26 01:10:11', NULL),
(5, 'Banorte', '00:00', '00:00', 'c8e805587dd0ec0a24640b66bf00c446e.gif', 4, 1, '2017-08-26 00:57:19', '2017-08-26 00:57:19', NULL),
(6, 'BBVA', '00:00', '00:00', '9dfcd4b939b8a1ca17fb5334bcf260b0a.jpg', 4, 2, '2017-08-26 00:57:32', '2017-08-26 00:57:32', NULL),
(7, 'Banamex', '00:00', '00:00', '52ab8ae5bfaa1f2b3361b0056bc5ac40x.gif', 4, 5, '2017-08-26 00:58:11', '2017-08-26 00:58:11', NULL),
(8, 'Banamex', '00:00', '00:00', '2552d5b08186aafe389f80f1222bbf3ax.gif', 4, 14, '2017-08-26 01:00:18', '2017-08-26 01:00:18', NULL),
(9, 'Little Caesars', '00:00', '00:00', '1b6a362e04fd08648d78c8b74f5a3fd4o.png', 3, 6, '2017-08-26 01:03:52', '2017-08-26 01:21:38', NULL),
(10, 'Sears', '00:00', '00:00', '22bc67803d1439fca691d63ad64564d16.jpg', 6, 7, '2017-08-26 01:15:14', '2017-08-26 01:15:35', NULL),
(11, 'wings army', '00:00', '00:00', '29f9305ee8ee7ce94fdc6fc486c1d5dfy.jpg', 5, 8, '2017-08-26 01:16:17', '2017-08-26 01:20:44', NULL),
(12, 'wings army', '00:00', '00:00', '38acd2d689c544f0ae0234a1649758b5y.jpg', 5, 5, '2017-08-26 01:16:45', '2017-08-26 01:20:55', NULL),
(13, 'wings army', '00:00', '00:00', 'e5af065df58f659b7a851a4e86f99c50y.jpg', 5, 16, '2017-08-26 01:17:09', '2017-08-26 01:21:04', NULL),
(14, 'Lucky Black', '00:00', '00:00', '8316974ecbaf26ae75fd9db98054b35by.jpg', 5, 15, '2017-08-26 01:17:23', '2017-08-26 01:20:24', NULL),
(15, 'wings army', '00:00', '00:00', '6e79698bf0b202d30f85a5c68170af0a7.png', 3, 6, '2017-08-26 01:18:16', '2017-08-26 01:18:16', NULL),
(16, 'Little Caesars', '00:00', '00:00', '4fb5ef7c57b936e71e5ee41951c852bdo.png', 3, 17, '2017-08-26 01:19:20', '2017-08-26 01:19:20', NULL),
(17, '+Cota', '00:00', '00:00', '977eb2db29b4a191ca5f618ee4f963a42.png', 6, 13, '2017-08-26 01:19:55', '2017-08-26 01:19:55', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE `tipos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Alimentos', '2017-08-23 00:46:16', '2017-08-23 00:46:16', NULL),
(2, 'Banco', '2017-08-23 00:46:24', '2017-08-26 00:00:22', NULL),
(3, 'Mascotas', '2017-08-23 00:46:30', '2017-08-23 00:46:30', NULL),
(4, 'Isla', '2017-08-23 00:46:34', '2017-08-23 00:46:34', NULL),
(5, 'Departamental', '2017-08-25 23:59:20', '2017-08-25 23:59:20', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubicacions`
--

CREATE TABLE `ubicacions` (
  `id` int(10) UNSIGNED NOT NULL,
  `domicilio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `ciudad_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ubicacions`
--

INSERT INTO `ubicacions` (`id`, `domicilio`, `latitud`, `longitud`, `ciudad_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Av. José Vasconcelos #402 esq. Gómez Morín, Del Valle', 25.6520114, -100.3599671, 4, '2017-08-23 00:16:52', '2017-08-23 00:16:52', NULL),
(2, 'Av Insurgentes 2500, Vista Hermosa, 64620 ', 25.6811081, -100.3549519, 2, '2017-08-23 00:17:16', '2017-08-23 00:17:16', NULL),
(3, 'Av. Felipe Pescador 1401, Esperanza', 24.0352325, -104.65167300000002, 1, '2017-08-23 00:22:44', '2017-08-23 00:22:44', NULL),
(4, 'Verde Valle', 20.6529143, -103.39176399999997, 5, '2017-08-25 21:39:30', '2017-08-25 21:39:30', NULL),
(5, 'Boulevard Guadiana 98 Lomas del Parque', 24.020293, -104.690156, 1, '2017-08-25 21:40:22', '2017-08-25 21:40:22', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `watchdogs`
--

CREATE TABLE `watchdogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `movimiento_id` int(10) UNSIGNED NOT NULL,
  `movimiento_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ciudads`
--
ALTER TABLE `ciudads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ciudads_estado_id_foreign` (`estado_id`);

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contactos_plaza_id_foreign` (`plaza_id`);

--
-- Indices de la tabla `ctiendas`
--
ALTER TABLE `ctiendas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ctiendas_tienda_id_foreign` (`tienda_id`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `imagen_plazas`
--
ALTER TABLE `imagen_plazas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `imagen_plazas_plaza_id_foreign` (`plaza_id`);

--
-- Indices de la tabla `imagen_tiendas`
--
ALTER TABLE `imagen_tiendas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `imagen_tiendas_tienda_id_foreign` (`tienda_id`);

--
-- Indices de la tabla `locals`
--
ALTER TABLE `locals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `locals_plaza_id_foreign` (`plaza_id`),
  ADD KEY `locals_tipo_id_foreign` (`tipo_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `noticias_plaza_id_foreign` (`plaza_id`);

--
-- Indices de la tabla `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`);

--
-- Indices de la tabla `plazas`
--
ALTER TABLE `plazas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plazas_ubicacion_id_foreign` (`ubicacion_id`);

--
-- Indices de la tabla `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indices de la tabla `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indices de la tabla `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tag_tienda`
--
ALTER TABLE `tag_tienda`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indices de la tabla `tiendas`
--
ALTER TABLE `tiendas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tiendas_categoria_id_foreign` (`categoria_id`),
  ADD KEY `tiendas_local_id_foreign` (`local_id`);

--
-- Indices de la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ubicacions`
--
ALTER TABLE `ubicacions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ubicacions_ciudad_id_foreign` (`ciudad_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `watchdogs`
--
ALTER TABLE `watchdogs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `ciudads`
--
ALTER TABLE `ciudads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `ctiendas`
--
ALTER TABLE `ctiendas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `imagen_plazas`
--
ALTER TABLE `imagen_plazas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `imagen_tiendas`
--
ALTER TABLE `imagen_tiendas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `locals`
--
ALTER TABLE `locals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `plazas`
--
ALTER TABLE `plazas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `tag_tienda`
--
ALTER TABLE `tag_tienda`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT de la tabla `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tiendas`
--
ALTER TABLE `tiendas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `tipos`
--
ALTER TABLE `tipos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `ubicacions`
--
ALTER TABLE `ubicacions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `watchdogs`
--
ALTER TABLE `watchdogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ciudads`
--
ALTER TABLE `ciudads`
  ADD CONSTRAINT `ciudads_estado_id_foreign` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`);

--
-- Filtros para la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD CONSTRAINT `contactos_plaza_id_foreign` FOREIGN KEY (`plaza_id`) REFERENCES `plazas` (`id`);

--
-- Filtros para la tabla `ctiendas`
--
ALTER TABLE `ctiendas`
  ADD CONSTRAINT `ctiendas_tienda_id_foreign` FOREIGN KEY (`tienda_id`) REFERENCES `tiendas` (`id`);

--
-- Filtros para la tabla `imagen_plazas`
--
ALTER TABLE `imagen_plazas`
  ADD CONSTRAINT `imagen_plazas_plaza_id_foreign` FOREIGN KEY (`plaza_id`) REFERENCES `plazas` (`id`);

--
-- Filtros para la tabla `imagen_tiendas`
--
ALTER TABLE `imagen_tiendas`
  ADD CONSTRAINT `imagen_tiendas_tienda_id_foreign` FOREIGN KEY (`tienda_id`) REFERENCES `tiendas` (`id`);

--
-- Filtros para la tabla `locals`
--
ALTER TABLE `locals`
  ADD CONSTRAINT `locals_plaza_id_foreign` FOREIGN KEY (`plaza_id`) REFERENCES `plazas` (`id`),
  ADD CONSTRAINT `locals_tipo_id_foreign` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`);

--
-- Filtros para la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD CONSTRAINT `noticias_plaza_id_foreign` FOREIGN KEY (`plaza_id`) REFERENCES `plazas` (`id`);

--
-- Filtros para la tabla `plazas`
--
ALTER TABLE `plazas`
  ADD CONSTRAINT `plazas_ubicacion_id_foreign` FOREIGN KEY (`ubicacion_id`) REFERENCES `ubicacions` (`id`);

--
-- Filtros para la tabla `tiendas`
--
ALTER TABLE `tiendas`
  ADD CONSTRAINT `tiendas_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`),
  ADD CONSTRAINT `tiendas_local_id_foreign` FOREIGN KEY (`local_id`) REFERENCES `locals` (`id`);

--
-- Filtros para la tabla `ubicacions`
--
ALTER TABLE `ubicacions`
  ADD CONSTRAINT `ubicacions_ciudad_id_foreign` FOREIGN KEY (`ciudad_id`) REFERENCES `ciudads` (`id`);
COMMIT;
