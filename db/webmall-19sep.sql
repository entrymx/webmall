-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 19-09-2017 a las 18:33:44
-- Versión del servidor: 5.7.19-0ubuntu0.16.04.1
-- Versión de PHP: 5.6.31-4+ubuntu16.04.1+deb.sury.org+4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `webmall`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'sUIFJWdvoHADXL8KydVcDehY3SUA55HH', 1, '2017-09-08 22:22:25', '2017-09-08 22:22:25', '2017-09-08 22:22:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `imagen`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ropa y Accesorios para Dama', '300.png', '2017-08-23 00:54:10', '2017-08-23 00:54:10', NULL),
(2, 'Ropa para Caballeros', '300.png', '2017-08-23 00:54:23', '2017-08-23 00:54:23', NULL),
(3, 'Restaurantes', '300.png', '2017-08-23 00:54:34', '2017-08-23 00:54:34', NULL),
(4, 'Bancos', 'bbva.jpg', '2017-08-26 00:29:34', '2017-09-15 04:03:11', NULL),
(5, 'Bar', 'banorte.gif', '2017-08-26 00:29:53', '2017-09-15 04:03:21', NULL),
(6, 'Departamental', '', '2017-08-26 00:30:16', '2017-08-26 00:30:16', NULL),
(7, 'Cine', '', '2017-08-26 00:30:22', '2017-08-26 00:30:22', NULL),
(8, '', '', '2017-09-15 03:33:35', '2017-09-15 03:33:44', '2017-09-15 03:33:44'),
(9, '', '', '2017-09-15 03:33:41', '2017-09-15 03:43:10', '2017-09-15 03:43:10'),
(10, '', '', '2017-09-15 03:41:04', '2017-09-15 03:43:11', '2017-09-15 03:43:11'),
(11, 'Casino', '', '2017-09-15 03:42:50', '2017-09-15 03:42:50', NULL),
(12, 'Hamburguesas', 'download.png', '2017-09-15 04:09:42', '2017-09-15 04:11:03', '2017-09-15 04:11:03'),
(13, 'banco', '', '2017-09-15 22:55:11', '2017-09-15 22:55:11', NULL),
(14, 'revolucion', '', '2017-09-15 23:16:26', '2017-09-15 23:16:26', NULL),
(15, '13', '', '2017-09-15 23:16:41', '2017-09-15 23:16:41', NULL),
(16, '1', '', '2017-09-19 03:29:41', '2017-09-19 03:29:41', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudads`
--

CREATE TABLE `ciudads` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ciudads`
--

INSERT INTO `ciudads` (`id`, `nombre`, `estado_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Victoria de Durango', 1, '2017-08-22 23:57:48', '2017-08-22 23:57:48', NULL),
(2, 'Monterrey', 2, '2017-08-22 23:57:56', '2017-08-22 23:57:56', NULL),
(3, 'San Nicolás de los Garza', 2, '2017-08-22 23:59:35', '2017-08-22 23:59:40', NULL),
(4, 'San Pedro Garza García', 2, '2017-08-22 23:59:57', '2017-08-22 23:59:57', NULL),
(5, 'Guadalajara', 4, '2017-08-25 21:38:53', '2017-08-25 21:38:53', NULL),
(6, 'Durango', 1, '2017-09-12 00:41:01', '2017-09-12 00:41:01', NULL),
(7, 'Batununggal', 5, '2017-09-12 02:19:27', '2017-09-12 02:19:27', NULL),
(8, 'La Concha', 6, '2017-09-12 02:19:51', '2017-09-12 02:19:51', NULL),
(9, 'Rosarito', 7, '2017-09-12 02:20:29', '2017-09-12 02:20:29', NULL),
(10, 'Rosarito', 1, '2017-09-12 02:22:04', '2017-09-12 02:22:04', NULL),
(11, 'r', 7, '2017-09-15 04:38:04', '2017-09-15 04:38:04', NULL),
(12, 'La Paz', 4, '2017-09-15 04:40:40', '2017-09-15 04:47:44', NULL),
(13, 'La Paz', 1, '2017-09-15 04:40:49', '2017-09-15 04:40:49', NULL),
(14, 'Monterrey', 7, '2017-09-15 04:47:59', '2017-09-15 04:47:59', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE `contactos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plaza_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`id`, `nombres`, `apellidos`, `correo`, `telefono`, `plaza_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Roberto Ignacio', 'Esparza Soto', 'asd@asd.com', '618 814 15 16', 7, '2017-09-14 04:24:36', '2017-09-14 04:24:36', NULL),
(2, 'Rojo', 'Ad', 'web@rojoad.com', '911', 7, '2017-09-14 20:38:10', '2017-09-14 20:38:46', NULL),
(3, 'AS', 'DAS', 'asd@asd.asd', 'asd', 5, '2017-09-14 20:39:06', '2017-09-14 22:05:33', NULL),
(4, 'Daniela', 'Fernadez', 'dani@fer.ca', '987', 1, '2017-09-14 22:02:52', '2017-09-14 22:02:52', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ctiendas`
--

CREATE TABLE `ctiendas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tienda_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ctiendas`
--

INSERT INTO `ctiendas` (`id`, `nombres`, `apellidos`, `correo`, `telefono`, `tienda_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Roberto', '', '', '', NULL, '2017-09-13 03:54:42', '2017-09-16 02:54:52', '2017-09-16 02:54:52'),
(2, 'Pedro', 'Garcia', 'pgarcia@asd.com', '8141516', 5, '2017-09-13 03:54:50', '2017-09-16 02:54:35', NULL),
(3, 'raúkl', '', '', '', NULL, '2017-09-13 03:54:56', '2017-09-16 02:54:55', '2017-09-16 02:54:55'),
(4, 'Joselito', 'niño', 'asd@asd.asd', '321', 18, '2017-09-13 03:55:03', '2017-09-14 22:40:50', '2017-09-14 22:40:50'),
(5, 'Roberto Ignacio', 'Esparza Soto', 'asd@asd.com', '8745414', 17, '2017-09-14 04:25:05', '2017-09-14 04:25:05', NULL),
(6, 'Cruz', 'esp', 'curz@asd.asd', '6987', 4, '2017-09-14 22:38:02', '2017-09-14 22:40:51', '2017-09-14 22:40:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Durango', '2017-08-22 23:57:30', '2017-08-22 23:57:30', NULL),
(2, 'Nuevo León', '2017-08-22 23:57:35', '2017-08-22 23:57:35', NULL),
(3, 'Queretaro', '2017-08-22 23:57:39', '2017-08-22 23:57:39', NULL),
(4, 'Jalisco', '2017-08-25 21:37:49', '2017-08-25 21:37:49', NULL),
(5, 'Jawa Barat', '2017-09-12 02:19:27', '2017-09-12 02:19:27', NULL),
(6, 'Coahuila de Zaragoza', '2017-09-12 02:19:51', '2017-09-12 02:19:51', NULL),
(7, 'Baja California', '2017-09-12 02:20:29', '2017-09-12 02:20:29', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen_plazas`
--

CREATE TABLE `imagen_plazas` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plaza_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `imagen_plazas`
--

INSERT INTO `imagen_plazas` (`id`, `imagen`, `plaza_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'paseo_durango.jpg', 1, '2017-08-23 00:41:39', '2017-08-23 00:41:39', NULL),
(2, 'paseo_sanpedro.jpg', 3, '2017-08-23 00:43:02', '2017-08-23 00:43:02', NULL),
(3, 'galerias_monterrey.jpg', 4, '2017-08-23 00:45:34', '2017-08-23 00:45:34', NULL),
(4, '269b0cfd9f0d9f12f91a801d3f67b8381.jpg', 5, '2017-08-25 23:45:06', '2017-08-25 23:45:06', NULL),
(5, '147c7a7f8b5c766e719ebd83d9d003cfe.jpg', 5, '2017-08-25 23:45:06', '2017-08-25 23:45:06', NULL),
(6, 'bf3685318fa1f01cc429562bf864eda7.webp', 6, '2017-08-25 23:54:14', '2017-08-25 23:54:14', NULL),
(7, '03262ceaaadbbefbf1a9f71c25d05f11u.jpg', 6, '2017-08-25 23:54:14', '2017-08-25 23:54:14', NULL),
(8, 'b1eedc91cb97738fb089a146c95f8fbbd.png', 9, '2017-09-13 23:22:24', '2017-09-13 23:22:24', NULL),
(9, '571fd85132e26617de8ec17035b50a4e2.jpg', 9, '2017-09-13 23:22:24', '2017-09-13 23:22:24', NULL),
(10, '03f5019164a3c3e17b1c71296c10081a2.png', 9, '2017-09-13 23:22:24', '2017-09-13 23:22:24', NULL),
(11, '11c1dfd8ada47f06015f8393ee1df5c53.jpg', 9, '2017-09-13 23:22:24', '2017-09-13 23:22:24', NULL),
(12, '01cb92d399499a33e43feb44d59a9444d.jpg', 8, '2017-09-13 23:31:16', '2017-09-13 23:31:16', NULL),
(13, '3a8adc2986057e2672f161f0b2f115354.jpg', 8, '2017-09-13 23:31:16', '2017-09-13 23:31:16', NULL),
(14, 'd42d558e0acc4f734915a870265f73c36.jpg', 8, '2017-09-13 23:31:16', '2017-09-13 23:31:16', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen_tiendas`
--

CREATE TABLE `imagen_tiendas` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tienda_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `imagen_tiendas`
--

INSERT INTO `imagen_tiendas` (`id`, `imagen`, `tienda_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '8201ecce7821840e4314d6ea6511b3fad.jpg', 20, '2017-09-13 23:17:23', '2017-09-13 23:17:23', NULL),
(2, '9cb10441ca673a45664d1ed49fcc6b15d.png', 20, '2017-09-13 23:17:23', '2017-09-13 23:17:23', NULL),
(3, '5bdb9de30d654f0e51ac4309eed761382.jpg', 20, '2017-09-13 23:17:23', '2017-09-13 23:17:23', NULL),
(4, '92481dd22dc6f9ce87e83f00adee50172.png', 20, '2017-09-13 23:17:23', '2017-09-13 23:17:23', NULL),
(5, '665f53fed785e68d7dd1c5a0fdf61d5d3.jpg', 20, '2017-09-13 23:17:24', '2017-09-13 23:17:24', NULL),
(6, '66ea91a2ec1acd8625af0fa862cde3543.jpg', 3, '2017-09-19 03:29:41', '2017-09-19 03:29:41', NULL),
(7, '7feb2647edea1b8c2c0619cc1827e1874.jpg', 3, '2017-09-19 03:29:41', '2017-09-19 03:29:41', NULL),
(8, 'e00cdbf493a38b7113e16830321b8e6a5.jpg', 3, '2017-09-19 03:29:41', '2017-09-19 03:29:41', NULL),
(9, '44b2e1bb605d5b4075e23cf7f44ffc3a6.jpg', 3, '2017-09-19 03:29:41', '2017-09-19 03:29:41', NULL),
(10, 'fdb795004f067e2f5ee854ccc50aff07s.jpg', 3, '2017-09-19 03:29:41', '2017-09-19 03:29:41', NULL),
(11, '178921993ca06b712eaa1b4e27109073s.png', 3, '2017-09-19 03:29:41', '2017-09-19 03:29:41', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `locals`
--

CREATE TABLE `locals` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plaza_id` int(10) UNSIGNED NOT NULL,
  `tipo_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `locals`
--

INSERT INTO `locals` (`id`, `nombre`, `estado`, `plaza_id`, `tipo_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'GM-A001', '0', 4, 2, '2017-08-23 00:47:09', '2017-08-23 00:47:09', NULL),
(2, 'GM-A002', '0', 4, 2, '2017-08-23 00:47:25', '2017-08-23 00:47:25', NULL),
(3, 'PD-001', '0', 1, 1, '2017-08-23 00:47:36', '2017-08-23 00:50:57', NULL),
(4, 'PD-002', '0', 1, 1, '2017-08-23 00:48:41', '2017-08-23 00:50:42', NULL),
(5, 'PSP-A001', '0', 3, 1, '2017-08-23 00:51:17', '2017-08-23 00:51:17', NULL),
(6, 'PSP-A002', '0', 3, 1, '2017-08-23 00:51:28', '2017-08-23 00:51:28', NULL),
(7, 'GM-A003', '0', 5, 4, '2017-08-26 00:00:56', '2017-08-26 00:00:56', NULL),
(8, 'GM-A004', '0', 4, 1, '2017-08-26 00:01:23', '2017-08-26 00:03:36', NULL),
(9, 'PD-003', '0', 1, 3, '2017-08-26 00:02:46', '2017-08-26 00:02:46', NULL),
(10, 'PD-004', '0', 1, 5, '2017-08-26 00:03:06', '2017-08-26 00:03:06', NULL),
(11, 'PSP-001', '0', 3, 2, '2017-08-26 00:04:16', '2017-08-26 00:04:16', NULL),
(12, 'PSP-002', '0', 3, 5, '2017-08-26 00:04:26', '2017-08-26 00:04:26', NULL),
(13, 'PSP-003', '0', 3, 4, '2017-08-26 00:04:37', '2017-08-26 00:04:37', NULL),
(14, 'VD-001', '0', 6, 1, '2017-08-26 00:11:32', '2017-08-26 00:11:32', NULL),
(15, 'VD-002', '0', 6, 1, '2017-08-26 00:12:11', '2017-08-26 00:12:11', NULL),
(16, 'VD-003', '0', 6, 1, '2017-08-26 00:13:00', '2017-08-26 00:13:00', NULL),
(17, 'VD-004', '0', 6, 2, '2017-08-26 00:13:28', '2017-08-26 00:13:28', NULL),
(18, 'SB', '', 1, 5, '2017-09-13 03:53:26', '2017-09-13 03:53:26', NULL),
(19, 'solo', '', 1, 1, '2017-09-13 20:52:43', '2017-09-15 02:51:45', NULL),
(20, '', '', 9, 1, '2017-09-14 04:33:00', '2017-09-14 04:33:05', '2017-09-14 04:33:05'),
(21, '666', '0', 9, 1, '2017-09-15 00:10:29', '2017-09-15 02:51:37', NULL),
(22, '33-12', '0', 6, 4, '2017-09-15 02:48:22', '2017-09-15 02:48:22', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_07_02_230147_migration_cartalyst_sentinel', 1),
(2, '2017_07_25_180652_create_tipos_table', 1),
(3, '2017_07_25_180703_create_categorias_table', 1),
(4, '2017_07_25_180704_create_tags_table', 1),
(5, '2017_07_25_180705_create_estados_table', 1),
(6, '2017_07_25_180707_create_ciudads_table', 1),
(7, '2017_07_25_180708_create_ubicacions_table', 1),
(8, '2017_07_25_180709_create_plazas_table', 1),
(9, '2017_07_25_180711_create_noticias_table', 1),
(10, '2017_07_25_180712_create_imagen_plazas_table', 1),
(11, '2017_07_25_180714_create_locals_table', 1),
(12, '2017_07_25_180715_create_tiendas_table', 1),
(13, '2017_07_25_180716_create_imagen_tiendas_table', 1),
(14, '2017_07_25_180718_create_tag_tienda_table', 1),
(15, '2017_07_26_152154_create_watchdogs_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plaza_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `imagen`, `descripcion`, `fecha`, `plaza_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'apertura', 'download.jpg', 'gran apertura mañana', '12/4152', 1, '2017-09-09 03:22:56', '2017-09-15 21:47:42', '2017-09-15 21:47:42'),
(2, '', '', '', '', 1, '2017-09-15 20:46:45', '2017-09-15 21:44:09', '2017-09-15 21:44:09'),
(3, 'Nueva tienda', '', 'se abre Burguer king', 'viernes 15 09 2017', 5, '2017-09-15 21:44:00', '2017-09-15 21:52:18', NULL),
(4, 'Grito', 'download 2.png', 'este viernes ven a dar el grito', 'jueves 14 09 2017', 3, '2017-09-15 22:02:58', '2017-09-15 22:13:39', NULL),
(5, 'Amplición', 'descarga.jpg', 'Paseo Durango crece con una gran ampliación', 'sábado 30 09 2017', 1, '2017-09-19 02:19:20', '2017-09-19 02:19:20', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(4, 1, '9P6bXui6T63a01pbkmaBffpErLpL4WlY', '2017-09-09 03:17:08', '2017-09-09 03:17:08'),
(5, 1, 'jxN7vZA1IFbgByeknXeDBYunIZ1obvZR', '2017-09-11 19:55:30', '2017-09-11 19:55:30'),
(6, 1, 'TUT3VhQh5eA2YifaDXbyGQ4ezFEfD664', '2017-09-12 20:38:32', '2017-09-12 20:38:32'),
(7, 1, '2IQGHPW15vaehCkLK81HCZaMIDAuIjwc', '2017-09-13 01:23:19', '2017-09-13 01:23:19'),
(8, 1, 'nP1FWPEUqSyP5L4D8FHJ83CqTWZpQ0OB', '2017-09-13 20:47:26', '2017-09-13 20:47:26'),
(9, 1, 'yBIa35dFcB2EduF2OxvsE9505momUJjH', '2017-09-14 20:28:34', '2017-09-14 20:28:34'),
(10, 1, 'J1kC0aCN43moUXTEO7XSUdmeT7EUU0d3', '2017-09-15 02:39:00', '2017-09-15 02:39:00'),
(11, 1, 'RYZRkxxVc7j4xcwKXFUEopVGgUPZfvFd', '2017-09-15 20:33:40', '2017-09-15 20:33:40'),
(12, 1, 'iZ0PUuoRY6cFVCD3PDxy8uBOP3tvt4pb', '2017-09-18 20:24:29', '2017-09-18 20:24:29'),
(13, 1, 'HCiHnf9dne4zDiFnGZk61oOkuD5PadIW', '2017-09-18 23:30:11', '2017-09-18 23:30:11'),
(14, 1, 'yGsQKj1bUuTbfwCVsRj0skYKaSW9eUUc', '2017-09-18 23:39:43', '2017-09-18 23:39:43'),
(15, 1, 'HYmwu1Q1xVQ0Eh9BLalhh0TxLiRk5sci', '2017-09-19 03:38:11', '2017-09-19 03:38:11'),
(16, 1, 'Ry8ox9svJKE02Ts84GHH48vu6Pm8kbyp', '2017-09-19 21:02:58', '2017-09-19 21:02:58'),
(17, 1, 'cqgtNiBwhfyRbtkFmD5ynGncAvY2zsiC', '2017-09-19 21:57:53', '2017-09-19 21:57:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plazas`
--

CREATE TABLE `plazas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apertura` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cierre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ubicacion_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `plazas`
--

INSERT INTO `plazas` (`id`, `nombre`, `apertura`, `cierre`, `logo`, `ubicacion_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Paseo Durango', '11:00', '21:00', '9ecdf921e863d462389e996b2c5dcf00o.png', 3, '2017-08-23 00:41:39', '2017-08-26 00:58:52', NULL),
(2, 'Galerías Monterrey', '', '', '', 2, '2017-08-23 00:42:37', '2017-08-23 00:45:13', '2017-08-23 00:45:13'),
(3, 'Paseo San Pedro', '09:00', '22:00', 'b92d30bb28f6824a611b64446304dac3o.jpg', 1, '2017-08-23 00:43:02', '2017-09-15 00:03:23', NULL),
(4, 'Galerias Monterrey', '09:00', '23:00', '2d1300001f59849ee06b25bd182d99d9d.jpg', 2, '2017-08-23 00:45:34', '2017-08-25 23:57:48', NULL),
(5, 'Expo Guadalajara', '00:00', '00:00', '170a4bbdf84642058baae47e0dd3fa88a.jpg', 4, '2017-08-25 23:45:06', '2017-08-26 00:59:47', NULL),
(6, 'Veranda', '00:00', '00:00', '', 5, '2017-08-25 23:54:14', '2017-08-25 23:54:14', NULL),
(7, 'Plaza de Toros Alejandra', '00:00', '00:00', '', 8, '2017-09-12 00:41:25', '2017-09-12 00:41:25', NULL),
(8, 'lo que sea', '00:00', '00:00', '30e31066cb53ab0980afa5f5b9c894c9s.png', 9, '2017-09-12 04:21:37', '2017-09-13 23:31:16', NULL),
(9, 'asd', '00:00', '00:00', '37210c8437c21e7ca0b4936529f88974d.png', 10, '2017-09-12 04:23:35', '2017-09-13 23:22:24', NULL),
(10, 'vacia', '00:00', '00:00', '', 11, '2017-09-14 23:52:11', '2017-09-14 23:52:11', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrador', NULL, '2017-09-08 22:22:24', '2017-09-08 22:22:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2017-09-08 22:22:25', '2017-09-08 22:22:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tags`
--

INSERT INTO `tags` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Banco', '2017-08-26 00:15:01', '2017-09-15 04:11:29', NULL),
(2, 'Deposito', '2017-08-26 00:15:30', '2017-08-26 00:15:30', NULL),
(3, 'Retiro', '2017-08-26 00:15:43', '2017-08-26 00:15:43', NULL),
(4, 'Comida', '2017-08-26 00:15:52', '2017-08-26 00:15:52', NULL),
(5, 'Fast Food', '2017-08-26 00:16:02', '2017-08-26 00:16:02', NULL),
(6, 'Hambuguesa', '2017-08-26 00:16:11', '2017-08-26 00:16:11', NULL),
(7, 'Pizza', '2017-08-26 00:16:21', '2017-08-26 00:16:21', NULL),
(8, 'Donas', '2017-08-26 00:16:25', '2017-08-26 00:16:25', NULL),
(9, 'Café', '2017-08-26 00:16:32', '2017-08-26 00:16:32', NULL),
(10, 'Smoothies', '2017-08-26 00:17:10', '2017-09-15 04:14:36', '2017-09-15 04:14:36'),
(11, 'Carne', '2017-08-26 00:17:52', '2017-08-26 00:17:52', NULL),
(12, 'Verde', '2017-08-26 00:17:58', '2017-08-26 00:17:58', NULL),
(13, 'Vegano', '2017-08-26 00:18:07', '2017-08-26 00:18:07', NULL),
(14, 'Ensalada', '2017-08-26 00:18:13', '2017-08-26 00:18:13', NULL),
(16, 'Ropa', '2017-08-26 00:18:28', '2017-08-26 00:18:28', NULL),
(17, 'Dama', '2017-08-26 00:18:33', '2017-08-26 00:18:33', NULL),
(18, 'Caballero', '2017-08-26 00:18:38', '2017-08-26 00:18:38', NULL),
(19, 'Bar', '2017-08-26 00:18:58', '2017-08-26 00:18:58', NULL),
(20, 'Cerveza', '2017-08-26 00:19:03', '2017-08-26 00:19:03', NULL),
(22, 'Alitas', '2017-08-26 00:19:15', '2017-08-26 00:19:15', NULL),
(23, 'Papas', '2017-08-26 00:19:22', '2017-08-26 00:19:22', NULL),
(24, 'Exposiciones', '2017-08-26 00:19:41', '2017-08-26 00:19:41', NULL),
(25, 'Mascotas', '2017-08-26 00:33:36', '2017-08-26 00:33:36', NULL),
(26, 'Perro', '2017-08-26 00:33:40', '2017-08-26 00:33:40', NULL),
(27, 'Gato', '2017-08-26 00:33:45', '2017-08-26 00:33:45', NULL),
(28, 'Vegetarianos', '2017-09-12 22:34:36', '2017-09-12 22:34:36', NULL),
(29, 'Quinoa', '2017-09-12 22:34:36', '2017-09-12 22:34:36', NULL),
(30, 'dedos de queso', '2017-09-13 22:25:32', '2017-09-13 22:25:32', NULL),
(31, 'china', '2017-09-15 03:20:37', '2017-09-15 03:20:37', NULL),
(32, 'oriental', '2017-09-15 03:20:37', '2017-09-15 03:20:37', NULL),
(33, 'japonesa', '2017-09-15 03:20:37', '2017-09-15 03:20:37', NULL),
(34, 'sushi', '2017-09-15 03:20:37', '2017-09-15 03:20:37', NULL),
(35, 'Tacos', '2017-09-15 04:14:04', '2017-09-15 04:14:30', NULL),
(36, 'nuevo', '2017-09-15 22:58:45', '2017-09-15 22:58:45', NULL),
(37, 'indep', '2017-09-15 23:13:54', '2017-09-15 23:13:54', NULL),
(38, 'brunch', '2017-09-15 23:14:13', '2017-09-15 23:14:13', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tag_tienda`
--

CREATE TABLE `tag_tienda` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL,
  `tienda_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tag_tienda`
--

INSERT INTO `tag_tienda` (`id`, `tag_id`, `tienda_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 16, 1, NULL, NULL, NULL),
(4, 1, 2, NULL, NULL, NULL),
(5, 2, 2, NULL, NULL, NULL),
(6, 3, 2, NULL, NULL, NULL),
(7, 25, 3, NULL, NULL, NULL),
(8, 26, 3, NULL, NULL, NULL),
(9, 27, 3, NULL, NULL, NULL),
(10, 4, 4, NULL, NULL, NULL),
(11, 5, 4, NULL, NULL, NULL),
(12, 6, 4, NULL, NULL, NULL),
(13, 1, 5, NULL, NULL, NULL),
(14, 2, 5, NULL, NULL, NULL),
(15, 3, 5, NULL, NULL, NULL),
(16, 1, 6, NULL, NULL, NULL),
(17, 2, 6, NULL, NULL, NULL),
(18, 3, 6, NULL, NULL, NULL),
(19, 1, 7, NULL, NULL, NULL),
(20, 2, 7, NULL, NULL, NULL),
(21, 3, 7, NULL, NULL, NULL),
(22, 1, 8, NULL, NULL, NULL),
(23, 2, 8, NULL, NULL, NULL),
(24, 3, 8, NULL, NULL, NULL),
(25, 4, 9, NULL, NULL, NULL),
(26, 5, 9, NULL, NULL, NULL),
(27, 7, 9, NULL, NULL, NULL),
(28, 11, 9, NULL, NULL, NULL),
(29, 16, 10, NULL, NULL, NULL),
(30, 17, 10, NULL, NULL, NULL),
(31, 18, 10, NULL, NULL, NULL),
(32, 4, 11, NULL, NULL, NULL),
(33, 5, 11, NULL, NULL, NULL),
(34, 6, 11, NULL, NULL, NULL),
(35, 11, 11, NULL, NULL, NULL),
(36, 19, 11, NULL, NULL, NULL),
(37, 20, 11, NULL, NULL, NULL),
(38, 21, 11, NULL, NULL, NULL),
(39, 22, 11, NULL, NULL, NULL),
(40, 23, 11, NULL, NULL, NULL),
(41, 4, 12, NULL, NULL, NULL),
(42, 5, 12, NULL, NULL, NULL),
(43, 6, 12, NULL, NULL, NULL),
(44, 11, 12, NULL, NULL, NULL),
(45, 19, 12, NULL, NULL, NULL),
(46, 20, 12, NULL, NULL, NULL),
(47, 21, 12, NULL, NULL, NULL),
(48, 22, 12, NULL, NULL, NULL),
(49, 23, 12, NULL, NULL, NULL),
(50, 4, 13, NULL, NULL, NULL),
(51, 5, 13, NULL, NULL, NULL),
(52, 6, 13, NULL, NULL, NULL),
(53, 11, 13, NULL, NULL, NULL),
(54, 19, 13, NULL, NULL, NULL),
(55, 20, 13, NULL, NULL, NULL),
(56, 21, 13, NULL, NULL, NULL),
(57, 22, 13, NULL, NULL, NULL),
(58, 23, 13, NULL, NULL, NULL),
(59, 4, 14, NULL, NULL, NULL),
(60, 5, 14, NULL, NULL, NULL),
(61, 6, 14, NULL, NULL, NULL),
(62, 11, 14, NULL, NULL, NULL),
(63, 19, 14, NULL, NULL, NULL),
(64, 20, 14, NULL, NULL, NULL),
(65, 21, 14, NULL, NULL, NULL),
(66, 22, 14, NULL, NULL, NULL),
(67, 23, 14, NULL, NULL, NULL),
(68, 4, 15, NULL, NULL, NULL),
(69, 5, 15, NULL, NULL, NULL),
(70, 6, 15, NULL, NULL, NULL),
(71, 11, 15, NULL, NULL, NULL),
(72, 23, 15, NULL, NULL, NULL),
(73, 4, 16, NULL, NULL, NULL),
(74, 5, 16, NULL, NULL, NULL),
(75, 7, 16, NULL, NULL, NULL),
(76, 11, 16, NULL, NULL, NULL),
(77, 23, 16, NULL, NULL, NULL),
(78, 25, 17, NULL, NULL, NULL),
(79, 26, 17, NULL, NULL, NULL),
(80, 27, 17, NULL, NULL, NULL),
(81, 4, 18, NULL, NULL, NULL),
(82, 6, 18, NULL, NULL, NULL),
(83, 12, 18, NULL, NULL, NULL),
(84, 13, 18, NULL, NULL, NULL),
(85, 14, 18, NULL, NULL, NULL),
(86, 28, 18, NULL, NULL, NULL),
(87, 29, 18, NULL, NULL, NULL),
(88, 7, 19, NULL, NULL, NULL),
(93, 22, 20, NULL, NULL, NULL),
(94, 30, 20, NULL, NULL, NULL),
(95, 5, 21, NULL, NULL, NULL),
(96, 4, 21, NULL, NULL, NULL),
(97, 31, 21, NULL, NULL, NULL),
(98, 32, 21, NULL, NULL, NULL),
(99, 33, 21, NULL, NULL, NULL),
(100, 34, 21, NULL, NULL, NULL),
(104, 1, 23, NULL, NULL, NULL),
(105, 22, 23, NULL, NULL, NULL),
(106, 38, 23, NULL, NULL, NULL),
(107, 4, 22, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'global', NULL, '2017-09-18 23:30:05', '2017-09-18 23:30:05'),
(2, NULL, 'ip', '127.0.0.1', '2017-09-18 23:30:05', '2017-09-18 23:30:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiendas`
--

CREATE TABLE `tiendas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cierre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apertura` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categoria_id` int(10) UNSIGNED NOT NULL,
  `local_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tiendas`
--

INSERT INTO `tiendas` (`id`, `nombre`, `cierre`, `apertura`, `logo`, `categoria_id`, `local_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Liverpool', '00:00', '00:00', '6b829aa516fdd2df489085e4da75f99b3.jpg', 1, 3, '2017-08-26 00:32:07', '2017-09-15 22:58:30', NULL),
(2, 'Banorte', '00:00', '00:00', '13ef4b4f4c3ddb0d1cac91115bcefc3ae.gif', 4, 4, '2017-08-26 00:32:39', '2017-08-26 00:32:39', NULL),
(3, '+Cota', '00:00', '00:00', 'be7bba27870e5c959631355e4c21c24f2.png', 16, 9, '2017-08-26 00:35:06', '2017-09-19 03:29:41', NULL),
(4, 'Burguer King', '00:00', '00:00', '5cbfffee1d302f23407389d2f7e0aa2fd.png', 3, 10, '2017-08-26 00:40:13', '2017-08-26 01:10:11', NULL),
(5, 'Banorte', '00:00', '00:00', 'c8e805587dd0ec0a24640b66bf00c446e.gif', 4, 1, '2017-08-26 00:57:19', '2017-08-26 00:57:19', NULL),
(6, 'BBVA', '00:00', '00:00', '9dfcd4b939b8a1ca17fb5334bcf260b0a.jpg', 4, 2, '2017-08-26 00:57:32', '2017-08-26 00:57:32', NULL),
(7, 'Banamex', '00:00', '00:00', '52ab8ae5bfaa1f2b3361b0056bc5ac40x.gif', 4, 5, '2017-08-26 00:58:11', '2017-08-26 00:58:11', NULL),
(8, 'Banamex', '00:00', '00:00', '2552d5b08186aafe389f80f1222bbf3ax.gif', 4, 14, '2017-08-26 01:00:18', '2017-08-26 01:00:18', NULL),
(9, 'Little Caesars', '00:00', '00:00', '1b6a362e04fd08648d78c8b74f5a3fd4o.png', 3, 6, '2017-08-26 01:03:52', '2017-08-26 01:21:38', NULL),
(10, 'Sears', '00:00', '00:00', '22bc67803d1439fca691d63ad64564d16.jpg', 6, 7, '2017-08-26 01:15:14', '2017-08-26 01:15:35', NULL),
(11, 'wings army', '00:00', '00:00', '29f9305ee8ee7ce94fdc6fc486c1d5dfy.jpg', 5, 8, '2017-08-26 01:16:17', '2017-08-26 01:20:44', NULL),
(12, 'wings army', '00:00', '00:00', '38acd2d689c544f0ae0234a1649758b5y.jpg', 5, 5, '2017-08-26 01:16:45', '2017-08-26 01:20:55', NULL),
(13, 'wings army', '00:00', '00:00', 'e5af065df58f659b7a851a4e86f99c50y.jpg', 5, 16, '2017-08-26 01:17:09', '2017-08-26 01:21:04', NULL),
(14, 'Lucky Black', '00:00', '00:00', '8316974ecbaf26ae75fd9db98054b35by.jpg', 5, 15, '2017-08-26 01:17:23', '2017-08-26 01:20:24', NULL),
(15, 'wings army', '00:00', '00:00', '6e79698bf0b202d30f85a5c68170af0a7.png', 3, 6, '2017-08-26 01:18:16', '2017-08-26 01:18:16', NULL),
(16, 'Little Caesars', '00:00', '00:00', '4fb5ef7c57b936e71e5ee41951c852bdo.png', 3, 17, '2017-08-26 01:19:20', '2017-08-26 01:19:20', NULL),
(17, '+Cota', '00:00', '00:00', '977eb2db29b4a191ca5f618ee4f963a42.png', 6, 13, '2017-08-26 01:19:55', '2017-08-26 01:19:55', NULL),
(18, 'La cosecha', '00:00', '00:00', '', 3, 1, '2017-09-12 22:34:36', '2017-09-12 22:34:36', NULL),
(19, 'pizza', '00:00', '00:00', '', 1, 1, '2017-09-13 01:45:54', '2017-09-13 01:45:54', NULL),
(20, 'Modelo', '21:00', '10:00', 'd9804ec9e6259bfa83225d63f8e00e10s.jpg', 1, 19, '2017-09-13 03:56:33', '2017-09-13 23:29:58', NULL),
(21, 'kiromi', '23:00', '11:00', '', 3, 11, '2017-09-15 03:20:37', '2017-09-15 03:20:37', NULL),
(22, 'ricardo castro', '00:00', '00:00', '', 16, 18, '2017-09-15 22:55:52', '2017-09-20 04:24:20', NULL),
(23, 'dsa', '00:00', '00:00', '', 15, 12, '2017-09-15 22:57:16', '2017-09-15 23:16:41', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE `tipos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Alimentos', '2017-08-23 00:46:16', '2017-08-23 00:46:16', NULL),
(2, 'Banco', '2017-08-23 00:46:24', '2017-08-26 00:00:22', NULL),
(3, 'Mascotas', '2017-08-23 00:46:30', '2017-08-23 00:46:30', NULL),
(4, 'Isla', '2017-08-23 00:46:34', '2017-08-23 00:46:34', NULL),
(5, 'Departamental', '2017-08-25 23:59:20', '2017-08-25 23:59:20', NULL),
(6, 'Doble', '2017-09-15 02:59:43', '2017-09-15 02:59:43', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubicacions`
--

CREATE TABLE `ubicacions` (
  `id` int(10) UNSIGNED NOT NULL,
  `domicilio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `ciudad_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ubicacions`
--

INSERT INTO `ubicacions` (`id`, `domicilio`, `latitud`, `longitud`, `ciudad_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Av. José Vasconcelos #402 esq. Gómez Morín, Del Valle', 25.6520114, -100.3599671, 4, '2017-08-23 00:16:52', '2017-08-23 00:16:52', NULL),
(2, 'Av Insurgentes 2500, Vista Hermosa, 64620 ', 25.6811081, -100.3549519, 2, '2017-08-23 00:17:16', '2017-08-23 00:17:16', NULL),
(3, 'Av. Felipe Pescador 1401, Esperanza', 24.0352325, -104.65167300000002, 1, '2017-08-23 00:22:44', '2017-08-23 00:22:44', NULL),
(4, 'Verde Valle', 20.6529143, -103.39176399999997, 5, '2017-08-25 21:39:30', '2017-08-25 21:39:30', NULL),
(5, 'Boulevard Guadiana 98 Lomas del Parque', 24.020293, -104.690156, 1, '2017-08-25 21:40:22', '2017-08-25 21:40:22', NULL),
(6, 'asd', 19.3003448, -99.14682599999998, 5, '2017-09-09 04:07:12', '2017-09-09 04:07:12', NULL),
(7, 'San Ignacio, 34030 Durango, Dgo., México', 24.0436548, -104.66078820000001, 6, '2017-09-12 00:41:01', '2017-09-12 00:41:01', NULL),
(8, 'Zona Centro, Durango, Dgo., México', 24.0277202, -104.65317590000001, 10, '2017-09-12 00:41:25', '2017-09-12 02:22:04', NULL),
(9, 'La Concha, Coah., México', 25.6286424, -103.37982499999998, 8, '2017-09-12 04:21:37', '2017-09-12 04:21:37', NULL),
(10, 'Felipe Pescador 1401, Zona Centro, Durango, Dgo., México', 24.0359155, -104.6518712, 1, '2017-09-12 04:23:35', '2017-09-12 04:23:35', NULL),
(11, 'Felipe Pescador 1401, Zona Centro, Durango, Dgo., México', 24.0359155, -104.6518712, 1, '2017-09-14 23:52:11', '2017-09-14 23:52:11', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `last_login`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES
(1, 'web@rojoad.com', '$2y$10$6ZOiHtOq1zJolQ.fwEqnW.B4ENt/0MSgeVra6HsrlFWzz7RJr/bh2', NULL, '2017-09-19 21:57:53', 'Rojo', 'Advertising', '2017-09-08 22:22:25', '2017-09-19 21:57:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `watchdogs`
--

CREATE TABLE `watchdogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `movimiento_id` int(10) UNSIGNED NOT NULL,
  `movimiento_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ciudads`
--
ALTER TABLE `ciudads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ciudads_estado_id_foreign` (`estado_id`);

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contactos_plaza_id_foreign` (`plaza_id`);

--
-- Indices de la tabla `ctiendas`
--
ALTER TABLE `ctiendas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ctiendas_tienda_id_foreign` (`tienda_id`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `imagen_plazas`
--
ALTER TABLE `imagen_plazas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `imagen_plazas_plaza_id_foreign` (`plaza_id`);

--
-- Indices de la tabla `imagen_tiendas`
--
ALTER TABLE `imagen_tiendas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `imagen_tiendas_tienda_id_foreign` (`tienda_id`);

--
-- Indices de la tabla `locals`
--
ALTER TABLE `locals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `locals_plaza_id_foreign` (`plaza_id`),
  ADD KEY `locals_tipo_id_foreign` (`tipo_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `noticias_plaza_id_foreign` (`plaza_id`);

--
-- Indices de la tabla `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`);

--
-- Indices de la tabla `plazas`
--
ALTER TABLE `plazas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plazas_ubicacion_id_foreign` (`ubicacion_id`);

--
-- Indices de la tabla `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indices de la tabla `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indices de la tabla `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tag_tienda`
--
ALTER TABLE `tag_tienda`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indices de la tabla `tiendas`
--
ALTER TABLE `tiendas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tiendas_categoria_id_foreign` (`categoria_id`),
  ADD KEY `tiendas_local_id_foreign` (`local_id`);

--
-- Indices de la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ubicacions`
--
ALTER TABLE `ubicacions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ubicacions_ciudad_id_foreign` (`ciudad_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `watchdogs`
--
ALTER TABLE `watchdogs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `ciudads`
--
ALTER TABLE `ciudads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `ctiendas`
--
ALTER TABLE `ctiendas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `imagen_plazas`
--
ALTER TABLE `imagen_plazas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `imagen_tiendas`
--
ALTER TABLE `imagen_tiendas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `locals`
--
ALTER TABLE `locals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `plazas`
--
ALTER TABLE `plazas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT de la tabla `tag_tienda`
--
ALTER TABLE `tag_tienda`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT de la tabla `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tiendas`
--
ALTER TABLE `tiendas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `tipos`
--
ALTER TABLE `tipos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `ubicacions`
--
ALTER TABLE `ubicacions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `watchdogs`
--
ALTER TABLE `watchdogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ciudads`
--
ALTER TABLE `ciudads`
  ADD CONSTRAINT `ciudads_estado_id_foreign` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`);

--
-- Filtros para la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD CONSTRAINT `contactos_plaza_id_foreign` FOREIGN KEY (`plaza_id`) REFERENCES `plazas` (`id`);

--
-- Filtros para la tabla `ctiendas`
--
ALTER TABLE `ctiendas`
  ADD CONSTRAINT `ctiendas_tienda_id_foreign` FOREIGN KEY (`tienda_id`) REFERENCES `tiendas` (`id`);

--
-- Filtros para la tabla `imagen_plazas`
--
ALTER TABLE `imagen_plazas`
  ADD CONSTRAINT `imagen_plazas_plaza_id_foreign` FOREIGN KEY (`plaza_id`) REFERENCES `plazas` (`id`);

--
-- Filtros para la tabla `imagen_tiendas`
--
ALTER TABLE `imagen_tiendas`
  ADD CONSTRAINT `imagen_tiendas_tienda_id_foreign` FOREIGN KEY (`tienda_id`) REFERENCES `tiendas` (`id`);

--
-- Filtros para la tabla `locals`
--
ALTER TABLE `locals`
  ADD CONSTRAINT `locals_plaza_id_foreign` FOREIGN KEY (`plaza_id`) REFERENCES `plazas` (`id`),
  ADD CONSTRAINT `locals_tipo_id_foreign` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`);

--
-- Filtros para la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD CONSTRAINT `noticias_plaza_id_foreign` FOREIGN KEY (`plaza_id`) REFERENCES `plazas` (`id`);

--
-- Filtros para la tabla `plazas`
--
ALTER TABLE `plazas`
  ADD CONSTRAINT `plazas_ubicacion_id_foreign` FOREIGN KEY (`ubicacion_id`) REFERENCES `ubicacions` (`id`);

--
-- Filtros para la tabla `tiendas`
--
ALTER TABLE `tiendas`
  ADD CONSTRAINT `tiendas_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`),
  ADD CONSTRAINT `tiendas_local_id_foreign` FOREIGN KEY (`local_id`) REFERENCES `locals` (`id`);

--
-- Filtros para la tabla `ubicacions`
--
ALTER TABLE `ubicacions`
  ADD CONSTRAINT `ubicacions_ciudad_id_foreign` FOREIGN KEY (`ciudad_id`) REFERENCES `ciudads` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
