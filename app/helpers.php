<?php

/*********************/
//GoogleMaps Key
define('KEY',"AIzaSyC3oQKgM7XmUrJYsGIraOF9tVDsR9-Ev-U");

define('imgFormat', '.png');
define('imgCatW', '350');
define('imgCatH', '250');
define('logoPlazaW', '100');
define('logoPlazaH', '100');
define('fotoPlazaW', '200');
define('fotoPlazaH', '200');
define('logoTiendaW', '150');
define('logoTiendaH', '150');
define('fotoTiendaW', '400');
define('fotoTiendaH', '400');
define('imagenNotiW', '700');
define('imagenNotiH', '100');

define('HELP_MAIL', 'ayuda@Webmall');
define('FROM_MAIL', 'no_replay@Webmall');
define('USER_MAIL', 'Web Mall');
define('SITIO', 'https://dev.webmall');

/********************/

define('ADMIN_ROUTE', '/adm/');
define('OWNER_ROUTE', '/own/');
$sitio = '/';

// Site Root
define('ROOT', $sitio);

//Path To Theme
//define('PATH_TO_THEME', bloginfo('template_directory').'/');
//$theme = '/villa/';
//define('PATH_TO_THEME', ROOT.'wp-content/themes'.$theme);
define('PATH_TO_THEME', ROOT);

//Css Root
define('CSS', PATH_TO_THEME . 'css/public/');
define('STYLE', '<link rel="stylesheet" type="text/css" href="' . CSS . 'style.css" media="screen and (min-width:768px)">');
define('STYLE_MOBILE', '<link rel="stylesheet" type="text/css" href="' . CSS. 'style-mobile.css" media="screen and (max-width:769px)">');
define('BOOTSTRAPCSS', '<link rel="stylesheet" type="text/css" href="' . CSS . 'bootstrap.min.css">');
define('BOOTSTRAPCSSTHEME', '<link rel="stylesheet" type="text/css" href="' . CSS . 'bootstrap-theme.min.css">');
define('TITULO', 'Webmall');

//Javascript root
define('JS', PATH_TO_THEME . 'js/public/');
//Image Directory
define('IMG', PATH_TO_THEME . 'img/public/');
define('IMG_PROMO', IMG . 'promos/');
define('IMG_LOGO', IMG . 'logos/');
define('IMG_UPLOAD_PROMO', $_SERVER['DOCUMENT_ROOT'] . IMG_PROMO);
define('IMG_UPLOAD_LOGO', $_SERVER['DOCUMENT_ROOT'] . IMG_LOGO);

define('INC', PATH_TO_THEME . 'include/');

//JQuery Ver 1.12.4
define('JQUERY', '<script type="text/javascript"
src="'.JS.'jquery/1.12.4/jquery.min.js"></script>');

//BOOTSTRAP JS
define('BOOTSTRAPJS', '<script src="' . JS . 'bootstrap.min.js"></script>');

// JQuery Map FWK
define('JQUERY_GMAP', '<script src="' . JS . 'jquery.gmap.min.js"></script>');

//FontAwesome
define('FONT_AWESOME', '<link rel="stylesheet" type="text/css" href="' . CSS . 'font-awesome.min.css" >');

//Themify Icons
define('THEMIFY_ICONS', '<link rel="stylesheet" type="text/css" href="' . CSS .'themify-icons.css" >');

$navbar = 
'<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
      <div class="navbar-header">
            <a class="navbar-brand" href="#"></a>
                </div>
                    <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Inicio</a></li>
                                <li><a href="#">Promociones</a></li>
                                <li><a href="#">Eventos</a></li>
                                <li><a href="#">Plazas</a></li>
                                <li><a href="#">Contacto</a></li>
                                <li><a href="/login">Admin</a></li>
                    </ul>
        </div>
    </div>
</nav>';
define('NAVBAR', $navbar);

// Google Maps Library
//$gmaps_api_key = 'AIzaSyAcF2FvwKSn-iDC9PasOkx5OzZr9EpOJUQ';
//$gmaps = "<script src='https://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.22&key=".$gmaps_api_key."'></script>";
define('GMAPS', '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true&key=AIzaSyAsKek6Ljg0rg5JSFNcqfufmejDm1HmlBo"></script>');

/**
 * JQuery Code for map embed
 * @param type $lat
 * @param type $long
 * @param type $scrollweel 
 * @param type $type (ROADMAP, SATELLITE, HYBRID, TERRAIN*)
 * @param type $zoom
 * @return string
 */
function embedMapJS($lat, $long, $scrollweel = 'false', $type = 'ROADMAP', $zoom = 14, $text = '_address') {
    $mapJS = '<script type="text/javascript">';
//    $mapJS .= 'var loc = [{lat:'.$lat.',long:'.$long.'}];';
    $mapJS .=
            '$(function() {
        $("#map").gMap({
            controls: {
                panControl: true,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                overviewMapControl: false
            },
            scrolwheel: ' . $scrollweel . ',
            maptype: "' . $type . '",        
            latitude: ' . $lat . ',
            longitude: ' . $long . ',
            zoom: ' . $zoom . ',
            markers: [{latitude: ' . $lat . ',longitude:' . $long . ',html:"' . $text . '", popup:true}]
        });
    });
</script>';
    return $mapJS;
}

/**
 * HTML Code for map embed
 * @param type $width
 * @param type $height
 * @return string
 */
function embedMap($width = '100%', $height = '250') {
    $map = '<div id="map" style="with:' . $width . ';height:' . $height . 'px;"></div>';
    return $map;
}
