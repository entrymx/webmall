<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Kyslik\ColumnSortable\Sortable;

class Local extends Model
{
    use SoftDeletes, Sortable;
    public $timestamps = 'true';


	public function Plaza(){
		return $this->belongsTo('\Webmall\Plaza');
	}

	public function Tipo(){
		return $this->belongsTo('\Webmall\Tipo');
	}

	public function Tienda(){
		return $this->hasMany('\Webmall\Tienda');
	}

    public $sortable = [
        'nombre',
        'estado',
        'plaza_id',
        'tipo_id'
    ];
}
