<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Kyslik\ColumnSortable\Sortable;

class Ciudad extends Model
{
    use SoftDeletes, Sortable;//

    public $timestamps = 'true';

	public function Estado(){
		return $this->belongsTo('\Webmall\Estado');
	}

	public function Ubicacion(){
		return $this->hasMany('\Webmall\Ubicacion');
	}

    public $sortable = [
        'nombre',
        'estado_id'
    ];
}
