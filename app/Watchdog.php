<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Watchdog extends Model
{
    use SoftDeletes;
	
    public function movimiento()
    {
        return $this->morphTo();
    }


}
