<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Kyslik\ColumnSortable\Sortable;

class Tipo extends Model
{
    use SoftDeletes, Sortable;
    public $timestamps = 'true';


	public function Local(){
		return $this->hasMany('\Webmall\Local');
	}



    public $sortable = [
        'nombre'
    ];

}
