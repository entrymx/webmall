<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Kyslik\ColumnSortable\Sortable;

class Ubicacion extends Model
{
    use SoftDeletes, Sortable;

    public $timestamps = 'true';

	public function Ciudad(){
		return $this->belongsTo('\Webmall\Ciudad');
	}

	public function Plaza(){
		return $this->hasMany('\Webmall\Plaza');
	}

    public $sortable = [
        'domicilio',
        'latitud',
        'longitud',
        'ciudad_id'
    ];
}
