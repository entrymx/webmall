<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Kyslik\ColumnSortable\Sortable;

class Noticia extends Model
{
    use SoftDeletes, Sortable;
    public $timestamps = 'true';


	public function Plaza(){
		return $this->belongsTo('\Webmall\Plaza');
	}

    public $sortable = [
        'titulo',
        'descripcion',
        'fecha',
        'tipo',
        'plaza_id'
    ];

}
