<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Kyslik\ColumnSortable\Sortable;

class Ctienda extends Model
{
    use SoftDeletes, Sortable;//

    public $timestamps = 'true';

	public function Tienda(){
		return $this->belongsTo('\Webmall\Tienda');
	}

    public $sortable = [
        'nombres',
        'apellidos',
        'correo',
        'telefono',
        'plaza_id'
    ];

}
