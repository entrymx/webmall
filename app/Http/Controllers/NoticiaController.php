<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Noticia;
use Webmall\Plaza;
use Webmall\Http\Requests\NoticiaRequest;
use Image;
class NoticiaController extends Controller
{
    public function index()
    {
        
        $noticias = Noticia::sortable('nombre', 'NOMBRE')->paginate(10);
        
        $view = \View('admin.noticia.index');

        $view->noticias = $noticias; 
        return $view;

    }

    public function create()
    {
        $plazas = Plaza::all();
       
        $view = \View('admin.noticia.create');
        $view->plazas = $plazas; 

        return $view;  
    }

    public function store(NoticiaRequest $request)
    {
        $noticia = new Noticia;
        $noticia->imagen = "";

        if( $imagenData = $request->get('imagen-img') ){

            $info = base64_decode(preg_replace("#^data:image/\w+;base64,#i", '', $imagenData));

            $img = Image::make($info);
            $name = (md5( str_random(5).time() ).imgFormat);
            $img->save(public_path('/noti/'.$name));

        }else{
            $name = "";
        }

        //dd($request->get('imagen-img') );

        /*
		if($imagen = $request->file('foto')){
	        $name = $imagen->getClientOriginalName();
	        $imagen->move('noti',$imagen->getClientOriginalName());
	        $noticia->imagen = $name;
		}*/
        
        $noticia->imagen = $name;
        $noticia->titulo = $request->titulo;
        $noticia->descripcion = $request->descripcion;
        $noticia->fecha = $request->fecha;
        $noticia->plaza_id = $request->plaza;
        $noticia->tipo = $request->tipo;
        
        $noticia->save();


        return redirect(ADMIN_ROUTE . 'noticia');

    }

    public function edit($id)
    {        
        $noticia = Noticia::find($id);
        $plazas = Plaza::all();
       
        $view = \View('admin.noticia.edit');
        $view->noticia = $noticia; 
        $view->plazas = $plazas; 

        return $view;       
    }

    public function update(NoticiaRequest $request, $id)
    {

        $noticia = Noticia::find($id);
/*
		if($imagen = $request->file('foto')){
	        $name = $imagen->getClientOriginalName();
	        $imagen->move('noti',$imagen->getClientOriginalName());
	        $noticia->imagen = $name;
		}
*/
        if( $imagenData = $request->get('imagen-img') ){

            $info = base64_decode(preg_replace("#^data:image/\w+;base64,#i", '', $imagenData));

            $img = Image::make($info);
            $name = (md5( str_random(5).time() ).imgFormat);
            $img->save(public_path('/noti/'.$name));
             $noticia->imagen = $name;
        }



        $noticia->titulo = $request->titulo;
        $noticia->descripcion = $request->descripcion;
        $noticia->fecha = $request->fecha;
        $noticia->plaza_id = $request->plaza;
        $noticia->tipo = $request->tipo;

        $noticia->save();


        return redirect(ADMIN_ROUTE . 'noticia');

    }


    public function destroy($id)
    {
        Noticia::find($id)->delete();

        return redirect(ADMIN_ROUTE . 'noticia');
    }

    public function borrarImagen($id)
    {
        $Tienda = Noticia::find($id);
        $Tienda->imagen = "";
        $Tienda->save();

        return redirect(ADMIN_ROUTE . 'noticia');
    }

}
