<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Local;
use Webmall\Plaza;
use Webmall\Tienda;
use Webmall\Tipo;
use Webmall\Http\Requests\LocalRequest;

class LocalController extends Controller
{
    public function index()
    {
        
        $locals = Local::sortable('nombre', 'NOMBRE')->paginate(10);

        $view = \View('admin.local.index');

        $view->locals = $locals; 
        return $view;

    }

    public function locales(Request $request)
    {
        $text = $request->text;
        $locals = Local::whereHas('tipo',function($query) use($text){
                            $query->where('tipos.nombre', 'like', '%'.$text.'%');
                        })->
                        orwhereHas('plaza',function($query) use($text){
                            $query->where('plazas.nombre', 'like', '%'.$text.'%');
                        })->
                        orWhere('nombre', 'like', '%'.$text.'%')->
                        //where('plaza_id',$id)->
                        sortable('nombre', 'NOMBRE')->
                        paginate(10);
       
        $view = \View('admin.local.locales');
        $view->locals = $locals; 
        
        return $view; 
    }

    public function create()
    {
        $plazas = Plaza::orderBy('nombre')->get();
        $tipos = Tipo::orderBy('nombre')->get();

        $view = \View('admin.local.create');
        $view->plazas = $plazas; 
        $view->tipos = $tipos; 

        return $view;  
    }

    public function store(LocalRequest $request)
    {
        $local = new Local;
        $local->nombre = $request->nombre;
        //$local->estado = $request->estado;
        $local->estado = 0;
        $local->tipo_id = $request->tipo;
        $local->plaza_id = $request->plaza;

        $local->save();

        return redirect(ADMIN_ROUTE . 'local');

    }

    public function edit($id)
    {        
        $local = Local::find($id);

        $plazas = Plaza::orderBy('nombre')->get();
        $tipos = Tipo::orderBy('nombre')->get();

        $view = \View('admin.local.edit');
        $view->local = $local; 
        $view->plazas = $plazas; 
        $view->tipos = $tipos; 

        return $view;      
    }

    public function update(LocalRequest $request, $id)
    {
        $local = Local::find($id);

        $local->nombre = $request->nombre;
        //$local->estado = $request->estado;
        $local->estado = 0;
        $local->tipo_id = $request->tipo;

        $local->plaza_id = $request->plaza;

        $local->update();

        return redirect(ADMIN_ROUTE . 'local');
    }


    public function destroy(Request $request ,$id)
    {
        $local = Local::find($id);
        if($local->tienda->count() >0 ){
            $tienda = $local->tienda->first();
            \File::delete(public_path().'/tie/'.$tienda->logo);

            foreach ($tienda->imagen as $key => $img) {
                \File::delete(public_path().'/tie/'.$img->imagen);
                $img->delete();
            }
            
            $tienda->delete();
            }

        $local->delete();


        if($request->ajax()){
                return "AJAX";
        }

        return redirect(ADMIN_ROUTE . 'local');
    }
}
