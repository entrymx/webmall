<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Ciudad;
use Webmall\Estado;
use Webmall\Plaza;
use Webmall\Tienda;
use Webmall\Http\Requests\EstadoRequest;

class EstadoController extends Controller
{
    public function index()
    {
        
        $estados = Estado::sortable('nombre', 'NOMBRE')->paginate(10);

        $view = \View('admin.estado.index');

        $view->estados = $estados; 
        return $view;

    }

    public function create()
    {
        $view = \View('admin.estado.create');

        //$view->estados = $estados; 
        return $view;
    }

    public function store(EstadoRequest $request)
    {
        $estado = new Estado;
        $estado->nombre = $request->nombre;
        $estado->save();

        return redirect(ADMIN_ROUTE . 'estado');

    }

    public function show($id)
    {
        $estado = Estado::find($id);

        $view = \View('admin.estado.show');
        $view->estado = $estado; 

        return $view; 
    }

    public function ciudades(Request $request)
    {
        $id = $request->id;
        $ciudades = Ciudad::where('estado_id',$id)
                            ->sortable('nombres', 'NOMBRES')
                            ->paginate(5);

        $view = \View('admin.estado.ciudades');

        $view->ciudades = $ciudades; 
        return $view;

    }

    public function plazas(Request $request)
    {

        //return 'hola';
        $id = $request->id;
        $plazas = Plaza::whereHas('ubicacion',function($query) use($id){
                            $query->whereHas('ciudad',function($query) use($id){
                                $query->where('ciudads.estado_id', $id);
                            });
                        })
                        ->sortable('nombre', 'NOMBRE')->paginate(5);

        $view = \View('admin.estado.plazas');
        $view->plazas = $plazas; 
        
        return $view; 
    }

    public function tiendas(Request $request)
    {
        $id = $request->id;
        $tiendas = Tienda:: whereHas('local',function($query) use($id){
                                $query->whereHas('plaza',function($query) use($id){
                                    $query->whereHas('ubicacion',function($query) use($id){
                                        $query->whereHas('ciudad',function($query) use($id){
                                            $query->where('ciudads.estado_id', $id);
                                        });
                                    });    
                                });
                            })   
                            ->sortable('nombre', 'NOMBRE')->paginate(5);

        $view = \View('admin.estado.tiendas');
        $view->tiendas = $tiendas; 
        
        return $view; 
    }

    public function edit($id)
    {        
        $estado = Estado::find($id);
    	
    	$view = \View('admin.estado.edit');

        $view->estado = $estado; 
        
        return $view;        
    }

    public function update(EstadoRequest $request, $id)
    {
        $estado = Estado::find($id);
        $estado->nombre = $request->nombre;
        $estado->save();

        return redirect(ADMIN_ROUTE . 'estado');
    }


    public function destroy($id)
    {
        $estado = Estado::find($id);
        $estado->delete();

        return redirect(ADMIN_ROUTE . 'estado');
    }
}
