<?php

namespace Webmall\Http\Controllers;

use Webmall\Http\Requests\SesionRequest;
use Sentinel;
use Carbon\Carbon;

use Webmall\User;

use Illuminate\Http\Request;

class SesionController extends Controller
{

	public function loginGet(){

		if(\Sentinel::guest()){
			return \view('public.login.login');
		}

		if(\Sentinel::getUser()->roles()->first()->slug == "admin"){
			return redirect(ADMIN_ROUTE);
		}

		if(\Sentinel::getUser()->roles()->first()->slug == "owner"){
			return redirect(OWNER_ROUTE);
		}
	}

	public function loginPost(SesionRequest $request){

		if(Sentinel::authenticate($request->all())){
			return redirect('/login');
		}else{
			return redirect()->back()->withErrors(["Los datos de acceso son incorrectos"]);
		}
	}

	public function logout(){
		Sentinel::logout(null, true);
		return redirect('/login');
	}


    public function olvidada(Request $request){
        
        if(! $user = User::where('email',$request->email)->first()){
			return redirect()->back()->withErrors(["Ese correo no está registrado"]);
		}else{
	
            $t =Carbon::today();

            $hash = hash('sha256', $user->id."-olvidada-".$t);


            $data = array(
                "nombre" => $user->first_name,
                "apellido" =>  $user->last_name,
                "correo" =>  $user->email,
                "link" =>  "/login/".$user->id."/".$hash,
            );
        
            \Mail::send('emails.olvidada', $data, function ($message) use ($user) {
        
                $message->from(FROM_MAIL, USER_MAIL );
        
                $message->to($user->email)->subject('Recuperar contraseña Webmall');

            });

			return redirect()->back()->with('status',"Revisa en tu correo un enlace para restaurar tu contraseña.");

 		}

    }

    public function restaurar($id, $hash){

        $t =Carbon::today();

        $h = hash('sha256', $id."-olvidada-".$t);

        //$user = User::find($id);
        $user = Sentinel::findById($id);


        if($hash == $h && $user != null){
            $pass = str_random(8);


            //dump($pass);


            Sentinel::update($user, array('password' => $pass));


            $data = array(
                //"nombre" => $user->nombre(),
                //"apellido" =>  $user->apellido(),
                "correo" =>  $user->email,
                "pass" =>  $pass,
                "user" => $user
            );
        
            \Mail::send('emails.restaurar', $data, function ($message) use ($user) {
        
                $message->from('rojo@unes.edu.mx', 'España Digital');
        
                $message->to($user->email)->subject('Nueva contraseña - España Digital');

            });

            return redirect('/login')->with('status',"Contraseña cambiada, revisa tu correo eletrónico");
        }else{
            return redirect('/login')->withErrors(["Ocurrió un problema inesperado"]);
        }
    } 

}
