<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Categoria;
use Webmall\Ctienda;
use Webmall\Owner;
use Webmall\Tag;
use Webmall\Tienda;
use Webmall\User;
use Image;

class OwnerController extends Controller
{
	public function index()
	{
        $user = User::find(\Sentinel::getUser()->id);

        


        //$tiendas = $user->tienda->sortable('nombre', 'NOMBRE')->paginate(10);
        //$tiendas = Tienda::where('user_id',$user->id)->sortable('nombre', 'NOMBRE')->paginate(10);//all();//with('estado')->get();
        //dd($tiendas);

        $view = \View('owner.index');

        //$view->tiendas = $tiendas; 
        return $view;

	}

    public function tiendas(Request $request)
    {
        $text = $request->text;
        $user = User::find(\Sentinel::getUser()->id);

        $tiendas = Tienda::where('user_id',$user->id)->
        					/*
        					whereHas('categoria',function($query) use($text){
                                $query->where('categorias.nombre', 'like', '%'.$text.'%');
                            })->
                            orwhereHas('local',function($query) use($text){
                                $query->where('locals.nombre', 'like', '%'.$text.'%');
                            })->
                            orwhereHas('local',function($query) use($text){
                                $query->whereHas('plaza',function($query) use($text){
                                    $query->where('plazas.nombre', 'like', '%'.$text.'%');
                                });
                            })->
                            orWhere('nombre', 'like', '%'.$text.'%')->
                            //where('plaza_id',$id)->
                            */
                            sortable('nombre', 'NOMBRE')->
                            paginate(10);
/*        
        $tiendas = Tienda:: whereHas('local',function($query) use($id){
                                $query->whereHas('plaza',function($query) use($id){
                                    $query->whereHas('ubicacion',function($query) use($id){
                                        $query->where('ubicacions.ciudad_id', $id);
                                    });   

*/

        $view = \View('owner.owners');
        $view->tiendas = $tiendas; 
        
        return $view; 
    }

    public function show($id)
    {

        $user = User::find(\Sentinel::getUser()->id);
        $tienda = Tienda::find($id);

        if($tienda->user_id != $user->id ){
            abort(403);
        }

        //dump($id);
        //dump($tienda);

        if( $owner = Owner::where('tienda_id',$id)->first() ){
            //dd('sí');
        }else{
            //dd('no');
            $owner = new Owner;
            $owner->tienda_id = $id;
            $owner->nombre = "";
            $owner->apertura = "";
            $owner->cierre = "";
            $owner->categoria = "";
            $owner->tags = "";
            $owner->comentarios = "";
            $owner->logo = "";

            $owner->save();
        }
        //dd();
        /*
        dump( $owner->tags );
        dump( substr ( $owner->tags , 2 ,  - 2 ) );
        dump( explode('","', substr ( $owner->tags , 2 , - 2 ) ) );

        die();
        */
        $view = \View('owner.show');
        $view->tienda = $tienda; 
        $view->owner = $owner; 

        return $view; 
    }

    public function update(Request $request, $id)
    {
        //dd($request->all());

        $user = User::find(\Sentinel::getUser()->id);
        $tienda = Tienda::find($id);

        if($tienda->user_id != $user->id ){
            abort(403);
        }

        $tienda->visto = 0;
        $tienda->save();

        if( $owner = Owner::where('tienda_id',$id)->first() ){
            
            //dump($owner->tags);
            $owner->nombre = $request->nombre;
            $owner->tags = json_encode($request->tags);
            $owner->categoria = $request->categoria;
            $owner->apertura = $request->apertura;
            $owner->cierre = $request->cierre;
            $owner->comentarios = $request->comentarios;
        
            if( $imagenData = $request->get('logo-img') ){
                $info = base64_decode(preg_replace("#^data:image/\w+;base64,#i", '', $imagenData));
                $img = Image::make($info);
                $name = (md5( str_random(5).time() ).'.png');
                $img->save(public_path('/tie/'.$name));
                $owner->logo = $name;
            }
        
            $owner->save();
        }

        return redirect(OWNER_ROUTE . 'tienda/'.$id);
    }


    public function contactos(Request $request)
    {
        $id = $request->id;
        $ctiendas = Ctienda::where('tienda_id',$id)
                            ->sortable('nombres', 'NOMBRES')
                            ->paginate(10);

        $view = \View('owner.contactos');

        $view->ctiendas = $ctiendas; 
        return $view;

    }

    public function edit($id)
    {

        $user = User::find(\Sentinel::getUser()->id);
        $tienda = Tienda::find($id);

        if($tienda->user_id != $user->id ){
            abort(403);
        }

        //dump($id);
        //dump($tienda);

        if( $owner = Owner::where('tienda_id',$id)->first() ){
            //dd('sí');
        }else{
            //dd('no');
            $owner = new Owner;
            $owner->tienda_id = $id;
            $owner->nombre = "";
            $owner->apertura = "";
            $owner->cierre = "";
            $owner->categoria = "";
            $owner->tags = "";
            $owner->comentarios = "";
            $owner->logo = "";

            $owner->save();
        }
        
        
        $tags = Tag::all('id','nombre as text');
        $categorias = Categoria::all();
/*
        dump( $tienda->tag->pluck('nombre') );
        dump( unserialize($owner->tags) );

        die();
*/
        $view = \View('owner.edit');
        $view->categorias = $categorias; 
        $view->tags = $tags; 
        $view->tienda = $tienda; 
        $view->owner = $owner; 

        return $view; 
    }

}
