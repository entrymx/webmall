<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Ctienda;
use Webmall\Tienda;
use Webmall\Http\Requests\CtiendaRequest;

class CtiendaController extends Controller
{
    public function index()
    {
        $ctiendas = Ctienda::sortable('nombres', 'NOMBRES')->paginate(10);

        $view = \View('admin.ctienda.index');

        $view->ctiendas = $ctiendas; 
        return $view;

    }

    public function create()
    {
        $view = \View('admin.ctienda.create');

        $view->tiendas = Tienda::all(); 
        return $view;
    }

    public function store(CTiendaRequest $request)
    {
        $ctienda = new Ctienda;
        $ctienda->nombres = $request->nombres;
        $ctienda->apellidos = $request->apellidos;
        $ctienda->correo = $request->correo;
        $ctienda->telefono = $request->telefono;
        if($request->tienda){
            $ctienda->tienda_id = $request->tienda;
        }
        else{
            $ctienda->tienda_id = null;
        }        $ctienda->save();

        return redirect(ADMIN_ROUTE . 'ctienda');

    }

    public function edit($id)
    {        
        $ctienda = Ctienda::find($id);
    	
    	$view = \View('admin.ctienda.edit');

        $view->ctienda = $ctienda; 
        $view->tiendas = Tienda::all(); 

        return $view;        
    }

    public function update(CTiendaRequest $request, $id)
    {
        $ctienda = Ctienda::find($id);
        $ctienda->nombres = $request->nombres;
        $ctienda->apellidos = $request->apellidos;
        $ctienda->correo = $request->correo;
        $ctienda->telefono = $request->telefono;
        
        if($request->tienda){
            $ctienda->tienda_id = $request->tienda;
        }
        else{
            $ctienda->tienda_id = null;
        }
        $ctienda->save();

        return redirect(ADMIN_ROUTE . 'ctienda');
    }


    public function destroy($id)
    {
        $ctienda = Ctienda::find($id);
        $ctienda->delete();

        return redirect(ADMIN_ROUTE . 'ctienda');
    }
}