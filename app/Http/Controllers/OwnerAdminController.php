<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Webmall\Http\Requests\OnwerRequest;

use Webmall\Tienda;
use Webmall\User;

class OwnerAdminController extends Controller
{
	public function index()
	{
		$users = Sentinel::getUserRepository()->
				with('roles.users')->

				whereHas('roles',function($query){
					$query->where('roles.slug', 'owner');
				})->
				get();


	
		$owners = User::find( $users->pluck('id')->toArray() ); 

        $view = \View('admin.owner.index');

        $view->owners = $owners;
        return $view;
    }

    public function owners(Request $request)
    {
        $text = $request->text;

		$users = Sentinel::getUserRepository()->
				whereHas('roles',function($query){
					$query->where('roles.slug', 'owner');
				})->
				orwhere('first_name', 'like', '%'.$text.'%')->
				//sortable('first_name', 'NOMBRE')->
                //paginate(10);
				get();


		$owners = 	//User:://find( $users->pluck('id')->toArray() )->
					User::find( $users->pluck('id')->toArray() );
					/*->
					where('first_name', 'like', '%'.$text.'%')->
					//sortable('first_name', 'NOMBRE')->
            		//paginate(10);
					get()->

		/*
        $users = Tienda::whereHas('categoria',function($query) use($text){
                                $query->where('categorias.nombre', 'like', '%'.$text.'%');
                            })->
                            orwhereHas('local',function($query) use($text){
                                $query->where('locals.nombre', 'like', '%'.$text.'%');
                            })->
                            orwhereHas('local',function($query) use($text){
                                $query->whereHas('plaza',function($query) use($text){
                                    $query->where('plazas.nombre', 'like', '%'.$text.'%');
                                });
                            })->
                            orWhere('nombre', 'like', '%'.$text.'%')->
                            //where('plaza_id',$id)->
                            sortable('nombre', 'NOMBRE')->
                            paginate(10);
		*/

        $view = \View('admin.owner.owners');
        $view->owners = $owners; 
        
        return $view; 
    }

    public function create()
    {
        $tiendas = Tienda::where('user_id',null)->get();

        $view = \View('admin.owner.create');
        $view->tiendas = $tiendas; 

        return $view;  
    }

    public function store(OnwerRequest $request)
    {

    	$tiendas = Tienda::find($request->tienda);

		$owner = Sentinel::findRoleBySlug('owner');


        $pass = str_random(5);

		$credentials = [
		    'first_name'    => $request->first_name,
		    'last_name'    => $request->last_name,
		    'email'    => $request->email,
		    'password' => $pass
		];
		$user = Sentinel::registerAndActivate($credentials);
		$owner->users()->attach($user);

        if($tiendas != null){
			foreach ($tiendas as $key => $t) {
				$t->user_id = $user->id;
				$t->save();
			}
		}

        $data = array(
            "nombre" => $user->first_name,
            "apellido" =>  $user->last_name,
            "correo" =>  $user->email,
            "pass" =>  $pass
        );
    
        \Mail::send('emails.ownerNuevo', $data, function ($message) use($data){
    
            $message->from('rojo@unes.edu.mx', 'España Digital');
    
            $message->to($data['correo'])->subject('Bienvenido a España Digital');

        });


        

        return redirect(ADMIN_ROUTE . 'owner');

    }

    public function edit($id)
    {        
        $tiendas = Tienda::	where('user_id',null)->
        					orWhere('user_id',$id)->
        					get();
        $owner = User::find($id);

        $view = \View('admin.owner.edit');
        $view->tiendas = $tiendas; 
        $view->owner = $owner; 

        return $view;        
    }


    public function update(OnwerRequest $request, $id)
    {
        
        $owner = User::find($id);

		$tiendas = Tienda::	Where('user_id',$id)->
        					get();


		$owner->first_name    = $request->first_name;
		$owner->last_name    = $request->last_name;
		$owner->email    = $request->email;
		$owner->save();

    	foreach ($tiendas as $key => $t) {
	   		$t->user_id = null;
    		$t->save();
    	}

    	$tiendas = Tienda::find($request->tienda);

    	//dd($tiendas);
    	if($tiendas != null){
	    	foreach ($tiendas as $key => $t) {
	    		$t->user_id = $id;
	    		$t->save();
	    	}
    	}

        return redirect(ADMIN_ROUTE . 'owner');

    }


    public function destroy($id)
    {
        User::find($id)->delete();

        return redirect(ADMIN_ROUTE . 'owner');
    }


}
