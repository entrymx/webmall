<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Local;
use Webmall\Plaza;
use Webmall\Tipo;

class LocalController extends Controller
{
    public function index()
    {
        
        $locals = Local::all();//with('estado')->get();

        $view = \View('admin.local.index');

        $view->locals = $locals; 
        return $view;

    }

    public function create()
    {
        $plazas = Plaza::orderBy('nombre')->get();
        $tipos = Tipo::orderBy('nombre')->get();

        $view = \View('admin.local.create');
        $view->plazas = $plazas; 
        $view->tipos = $tipos; 

        return $view;  
    }

    public function store(Request $request)
    {
        $local = new Local;
        $local->nombre = $request->nombre;
        $local->estado = $request->estado;
        $local->tipo_id = $request->tipo;
        $local->plaza_id = $request->plaza;

        $local->save();

        return redirect(ADMIN_ROUTE . 'local');

    }

    public function edit($id)
    {        
        $local = Local::find($id);

        $plazas = Plaza::orderBy('nombre')->get();
        $tipos = Tipo::orderBy('nombre')->get();

        $view = \View('admin.local.edit');
        $view->local = $local; 
        $view->plazas = $plazas; 
        $view->tipos = $tipos; 

        return $view;      
    }

    public function update(Request $request, $id)
    {
        $local = Local::find($id);

        $local->nombre = $request->nombre;
        $local->estado = $request->estado;
        $local->tipo_id = $request->tipo;
        $local->plaza_id = $request->plaza;        

        $local->update();

        return redirect(ADMIN_ROUTE . 'local');
    }


    public function destroy($id)
    {
        Local::find($id)->delete();

        return redirect(ADMIN_ROUTE . 'local');
    }
}
