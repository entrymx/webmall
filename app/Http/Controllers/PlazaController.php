<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Ciudad;
use Webmall\Contacto;
use Webmall\Estado;
use Webmall\ImagenPlaza;
use Webmall\Local;
use Webmall\Noticia;
use Webmall\Plaza;
use Webmall\Tienda;
use Webmall\Ubicacion;
use Webmall\Http\Requests\PlazaRequest;
use Image;

class PlazaController extends Controller
{
    public function index()
    {
        
        $plazas = Plaza::sortable('nombre', 'NOMBRE')->paginate(10);//all();//with('estado')->get();
        //all();//with('estado')->get();

        $view = \View('admin.plaza.index');

        $view->plazas = $plazas; 
        return $view;

    }

    public function create()
    {
        $ubicaciones = Ubicacion::orderBy('domicilio')->get();

        $view = \View('admin.plaza.create');
        $view->ubicaciones = $ubicaciones; 
        $view->contactos = Contacto::where("plaza_id",null)->get();

        return $view;  
    }

    public function show($id)
    {
        $plaza = Plaza::find($id);

        $view = \View('admin.plaza.show');
        $view->plaza = $plaza; 

        return $view; 
    }

    public function contactos(Request $request)
    {
        $id = $request->id;
        $contactos = Contacto::where('plaza_id',$id)
                            ->sortable('nombres', 'NOMBRES')
                            ->paginate(10);

        $view = \View('admin.plaza.contactos');

        $view->contactos = $contactos; 
        return $view;

    }

    public function tiendas(Request $request)
    {
        $id = $request->id;
        $tiendas = Tienda:: whereHas('local',function($query) use($id){
                            $query->where('locals.plaza_id', $id);
                            })
                            ->sortable('nombre', 'NOMBRE')->paginate(10);

        $view = \View('admin.plaza.tiendas');
        $view->tiendas = $tiendas; 
        
        return $view; 
    }

    public function locales(Request $request)
    {
        $id = $request->id;
        $locals = Local::where('plaza_id',$id)
                        ->sortable('nombre', 'NOMBRE')
                        ->paginate(10);
       
        $view = \View('admin.plaza.locales');
        $view->locals = $locals; 
        
        return $view; 
    }

    public function noticias(Request $request)
    {
        $id = $request->id;
        $noticias = Noticia::where('plaza_id',$id)
                        ->sortable('titulo', 'TITULO')
                        ->paginate(10);
       
        $view = \View('admin.plaza.noticias');
        $view->noticias = $noticias; 
        
        return $view; 
    }

    public function store(PlazaRequest $request)
    {

        $estado = Estado::where('nombre', $request->estado)->first();
        if($estado == null){
            $estado = new Estado;
            $estado->nombre = $request->estado;
            $estado->save();
        }
        
        $ciudad = Ciudad::where([ ['nombre', $request->ciudad], ['estado_id', $estado->id] ])->first();
        if($ciudad == null){
            $ciudad = new Ciudad;
            $ciudad->nombre = $request->ciudad;
            $ciudad->estado_id = $estado->id;
            $ciudad->save();
        }
        

        $ubicacion = new Ubicacion;
        $ubicacion->domicilio   = $request->domicilio;
        $ubicacion->latitud     = $request->latitud;
        $ubicacion->longitud    = $request->longitud;
        $ubicacion->ciudad_id   = $ciudad->id;
        $ubicacion->save();



        if( $imagenData = $request->get('logo-img') ){

            $info = base64_decode(preg_replace("#^data:image/\w+;base64,#i", '', $imagenData));

            $img = Image::make($info);
            $name = (md5( str_random(5).time() ).imgFormat);
            $img->save(public_path('/pla/'.$name));

        }else{
            $name = "";
        }

        $plaza = new Plaza;
        $plaza->nombre = $request->nombre;
        $plaza->apertura = $request->apertura;
        $plaza->cierre = $request->cierre;
        $plaza->ubicacion_id = $ubicacion->id;
        $plaza->logo = $name;
        $plaza->save();

        for ($i=1; $i <= $request->get('foto-id') ; $i++) { 
            
            if($imagenData = $request->get('foto-'.$i.'-img')){
                $info = base64_decode(preg_replace("#^data:image/\w+;base64,#i", '', $imagenData));

                $img = Image::make($info);
                $name = (md5( str_random(5).time() ).imgFormat);
                $img->save(public_path('/pla/'.$name));

                $img = new ImagenPlaza;
                $img->plaza_id = $plaza->id;
                $img->imagen = $name;
                $img->save();
            }
        }

        if( $request->contacto){
            foreach($request->contacto as $c){
                $c = Contacto::find($c);
                $c->plaza_id = $plaza->id;
                $c->save();
            }
        }

        return redirect(ADMIN_ROUTE . 'plaza');

    }

    public function edit($id)
    {        
        $plaza = Plaza::find($id);

        $ubicaciones = Ubicacion::orderBy('domicilio')->get();

        $view = \View('admin.plaza.edit');
        $view->ubicaciones = $ubicaciones; 
        $view->plaza = $plaza; 
        $view->contactos = Contacto::where("plaza_id",null)->
                                    orWhere("plaza_id",$plaza->id)->
                                    get();

        return $view;      
    }

    public function update(PlazaRequest $request, $id)
    {

        $plaza = Plaza::find($id);
        $plaza->nombre = $request->nombre;
        $plaza->apertura = $request->apertura;
        $plaza->cierre = $request->cierre;

        $estado = Estado::where('nombre', $request->estado)->first();
        if($estado == null){
            $estado = new Estado;
            $estado->nombre = $request->estado;
            $estado->save();
        }
        
        $ciudad = Ciudad::where([ ['nombre', $request->ciudad], ['estado_id', $estado->id] ])->first();
        if($ciudad == null){
            $ciudad = new Ciudad;
            $ciudad->nombre = $request->ciudad;
            $ciudad->estado_id = $estado->id;
            $ciudad->save();
        }


        $ubicacion = Ubicacion::find($plaza->ubicacion_id);
        $ubicacion->domicilio   = $request->domicilio;
        $ubicacion->latitud     = $request->latitud;
        $ubicacion->longitud    = $request->longitud;
        $ubicacion->ciudad_id   = $ciudad->id;
        $ubicacion->save();


        if( $imagenData = $request->get('logo-img') ){
            $info = base64_decode(preg_replace("#^data:image/\w+;base64,#i", '', $imagenData));
            $img = Image::make($info);
            $name = (md5( str_random(5).time() ).'.png');
            $img->save(public_path('/pla/'.$name));
            $plaza->logo = $name;
        }


        $plaza->save();

        for ($i=1; $i <= $request->get('foto-id') ; $i++) { 
            
            if($imagenData = $request->get('foto-'.$i.'-img')){
                $info = base64_decode(preg_replace("#^data:image/\w+;base64,#i", '', $imagenData));

                $img = Image::make($info);
                $name = (md5( str_random(5).time() ).'.png');
                $img->save(public_path('/pla/'.$name));

                $img = new ImagenPlaza;
                $img->plaza_id = $plaza->id;
                $img->imagen = $name;
                $img->save();
            }
        }

        $contactos = Contacto::where("plaza_id",$plaza->id)->get();
        
        if( $contactos->count() > 0 ){
            foreach($contactos as $c){
                
                $c1 = Contacto::find($c->id);
                $c1->plaza_id = null;
                $c1->save();
                
            }
        }


        if( $request->contacto){
            foreach($request->contacto as $c){
                $c = Contacto::find($c);
                $c->plaza_id = $plaza->id;
                $c->save();
            }
        }        


        return redirect(ADMIN_ROUTE . 'plaza');
    }

    public function destroy($id)
    {
        $plaza = Plaza::find($id);

        foreach ($plaza->local as $key => $local) {
            if($local->tienda->count() >0 ){
                $tienda = $local->tienda->first();
                \File::delete(public_path().'/tie/'.$tienda->logo);

                foreach ($tienda->imagen as $key => $img) {
                    \File::delete(public_path().'/tie/'.$img->imagen);
                    $img->delete();
                }

                $tienda->delete();
            }

            $local->delete();
        }

        foreach ($plaza->noticia as $key => $noticia) {
            \File::delete(public_path().'/noti/'.$noticia->imagen);
            $noticia->delete();
        }
        
        if($plaza->logo){
            \File::delete(public_path().'/plaza/'.$plaza->logo);
        }
        if($plaza->ubicacion){
            $plaza->ubicacion->delete();
        }
        return redirect(ADMIN_ROUTE . 'plaza');
    }

    public function borrarLogo($id)
    {
        $Plaza = Plaza::find($id);
        \File::delete(public_path().'/pla/'.$Plaza->logo);
        $Plaza->logo = "";
        $Plaza->save();

        return redirect(ADMIN_ROUTE . 'plaza');
    }

    public function borrarImagen($id)
    {     
        $img = ImagenPlaza::find($id);
        \File::delete(public_path().'/pla/'.$img->imagen);
        $img->delete();
        
        return redirect(ADMIN_ROUTE . 'plaza');
    }


}
