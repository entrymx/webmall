<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Ciudad;
use Webmall\Estado;
use Webmall\Ubicacion;

class UbicacionController extends Controller
{
    public function index()
    {
        
        $ubicaciones = Ubicacion::all();//with('estado')->get();

        $view = \View('admin.ubicacion.index');

        $view->ubicaciones = $ubicaciones; 
        return $view;

    }

    public function create()
    {
        $ciudades = Ciudad::orderBy('nombre')->get();
        $estados = Estado::orderBy('nombre')->get();
        
        $view = \View('admin.ubicacion.create');

        $view->ciudades = $ciudades; 
        $view->estados = $estados; 
        return $view;
    }

    public function store(Request $request)
    {
        
        $ubicacion = new Ubicacion;
        $ubicacion->domicilio = $request->domicilio;
        $ubicacion->ciudad_id = $request->ciudad;
        $ubicacion->longitud = $request->longitud;
        $ubicacion->latitud = $request->latitud;

        $ubicacion->save();

        return redirect(ADMIN_ROUTE . 'ubicacion');

    }

    public function edit($id)
    {        
        $ubicacion = Ubicacion::find($id);
        $ciudades = Ciudad::orderBy('nombre')->get();

        $view = \View('admin.ubicacion.edit');

        $view->ciudades = $ciudades; 
        $view->ubicacion = $ubicacion; 

        return $view;        
    }

    public function update(Request $request, $id)
    {
        $ubicacion = Ubicacion::find($id);
        $ubicacion->domicilio = $request->domicilio;
        $ubicacion->ciudad_id = $request->ciudad;
        $ubicacion->longitud = $request->longitud;
        $ubicacion->latitud = $request->latitud;

        $ubicacion->save();

        return redirect(ADMIN_ROUTE . 'ubicacion');
    }


    public function destroy($id)
    {
        $ubicacion = Ubicacion::find($id);
        $ubicacion->delete();

        return redirect(ADMIN_ROUTE . 'ubicacion');
    }
}
