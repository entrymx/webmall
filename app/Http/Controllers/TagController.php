<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Tag;
use Webmall\Tienda;
use Webmall\Http\Requests\TagRequest;

class TagController extends Controller
{
    public function index()
    {
        
        //$tags = Tag::orderBy('nombre')->paginate(5);//->get();
        $tags = Tag::sortable('nombre', 'NOMBRE')->paginate(10);//->get();

        $view = \View('admin.tag.index');

        $view->tags = $tags; 
        return $view;

    }

    public function create()
    {
        $view = \View('admin.tag.create');

        //$view->tags = $tags; 
        return $view;
    }

    public function store(TagRequest $request)
    {
        $tag = new Tag;
        $tag->nombre = $request->nombre;
        $tag->save();

        return redirect(ADMIN_ROUTE . 'tag');

    }

    public function show($id)
    {
        $tag = Tag::find($id);
        //dd($tag->tienda->first()->pivot->tag_id);

        //$tiendas = Tienda::whereHas('tag')->get();
        
        //dd($tiendas);

        $tiendas = Tienda::whereHas('tag', function ($query) use($id) {
            $query->where('tags.id',$id);
        })//->get();
         ->sortable('nombre', 'NOMBRE')
                    ->paginate(5);

        $view = \View('admin.tag.show');
        
        $view->tag = $tag; 
        $view->tiendas = $tiendas; 

        return $view; 
    }

    public function edit($id)
    {        
        $tag = Tag::find($id);
    	
    	$view = \View('admin.tag.edit');

        $view->tag = $tag; 
        
        return $view;        
    }

    public function update(TagRequest $request, $id)
    {
        $tag = Tag::find($id);
        $tag->nombre = $request->nombre;
        $tag->save();

        return redirect(ADMIN_ROUTE . 'tag');
    }


    public function destroy(Request $request, $id)
    {
        $tag = Tag::find($id);


		/* Si se elimina no afecta el listado de las tiendas
		if( $tag->tienda->count() > 0 ){
			return (0);
		}
		*/
		$tag->delete();

		if($request->ajax()){
			return "AJAX";
		}

		return redirect(ADMIN_ROUTE . 'tag');
    }
}
