<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Contacto;
use Webmall\Plaza;
use Webmall\Http\Requests\ContactoRequest;

class ContactoController extends Controller
{
    public function index()
    {
        $contactos = Contacto::sortable('nombres', 'NOMBRES')->paginate(10);

        $view = \View('admin.contacto.index');

        $view->contactos = $contactos; 
        return $view;

    }

    public function create()
    {
        $view = \View('admin.contacto.create');

        $view->plazas = Plaza::all(); 
        return $view;
    }

    public function store(ContactoRequest $request)
    {
        $contacto = new Contacto;
        $contacto->nombres = $request->nombres;
        $contacto->apellidos = $request->apellidos;
        $contacto->correo = $request->correo;
        $contacto->telefono = $request->telefono;
        if($request->plaza){
            $contacto->plaza_id = $request->plaza;
        }
        else{
            $contacto->plaza_id = null;
        }
        $contacto->save();

        return redirect(ADMIN_ROUTE . 'contacto');

    }

    public function edit($id)
    {        
        $contacto = Contacto::find($id);
    	
    	$view = \View('admin.contacto.edit');

        $view->contacto = $contacto; 
        $view->plazas = Plaza::all(); 

        return $view;        
    }

    public function update(ContactoRequest $request, $id)
    {
        $contacto = Contacto::find($id);
        $contacto->nombres = $request->nombres;
        $contacto->apellidos = $request->apellidos;
        $contacto->correo = $request->correo;
        $contacto->telefono = $request->telefono;
        
        if($request->plaza){
            $contacto->plaza_id = $request->plaza;
        }
        else{
            $contacto->plaza_id = null;
        }
        $contacto->save();

        return redirect(ADMIN_ROUTE . 'contacto');
    }


    public function destroy($id)
    {
        $contacto = Contacto::find($id);
        $contacto->delete();

        return redirect(ADMIN_ROUTE . 'contacto');
    }
}