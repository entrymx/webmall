<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Tipo;
use Webmall\Http\Requests\TipoRequest;

class TipoController extends Controller
{
    public function index()
    {
        
        $tipos = Tipo::sortable('nombre', 'NOMBRE')->paginate(10);

        $view = \View('admin.tipo.index');

        $view->tipos = $tipos; 
        return $view;

    }

    public function create()
    {
        $view = \View('admin.tipo.create');

        //$view->tipos = $tipos; 
        return $view;
    }

    public function store(TipoRequest $request)
    {
        $tipo = new Tipo;
        $tipo->nombre = $request->nombre;
        $tipo->save();

        return redirect(ADMIN_ROUTE . 'tipo');

    }

    public function edit($id)
    {        
        $tipo = Tipo::find($id);
    	
    	$view = \View('admin.tipo.edit');

        $view->tipo = $tipo; 
        
        return $view;        
    }

    public function update(TipoRequest $request, $id)
    {
        $tipo = Tipo::find($id);
        $tipo->nombre = $request->nombre;
        $tipo->save();

        return redirect(ADMIN_ROUTE . 'tipo');
    }


    public function destroy(Request $request,$id)
    {
        $tipo = Tipo::find($id);

        if( $tipo->local->count() > 0 ){
            return (0);
        }

        $tipo->delete();

        if($request->ajax()){
                return "AJAX";
        }

        return redirect(ADMIN_ROUTE . 'tipo');
    }
}
