<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Tag;
use Webmall\Tienda;
use Webmall\Plaza;

class BuscadorController extends Controller
{
    
    public function index(){
        $plazas = Plaza::all();
        $view = \View('public.index');
        $view->plazas = $plazas;
        return $view;
    }

    public function buscar(Request $request)
    {

        $lat = $request->lat;
        $lng = $request->lng;




        $text = $request->text;
        
        if( ctype_space($text) || $text == ""){
            $tiendas = collect(new Tienda);
        }
        else{
            $tiendas = Tienda:: whereHas('tag',function($query) use($text){
                                    $query->where('tags.nombre', 'like', '%'.$text.'%');
                                })->
                                orwhereHas('categoria',function($query) use($text){
                                    $query->where('categorias.nombre', 'like', '%'.$text.'%');
                                })->
                                orWhere('nombre', 'like', '%'.$text.'%')->
                                //orWhere('tienda.locals.plazas.nombre', 'like', '%'.$text.'%')->
                                take(5)->
                                get();

        }

        $destinos = "";
        $distancia = [];


        if($tiendas->count() > 0 && ($lat != 0 || $lng != 0) ){

            foreach ($tiendas as $key => $t) {
                $destinos.=($t->local->plaza->ubicacion->latitud.",".$t->local->plaza->ubicacion->longitud."|");
            }

            $resultados = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?'.
                                                //'units=metric&'.
                                                'origins='.$lat.','.$lng.
                                                '&destinations='.
                                                $destinos.
                                                '&key='. KEY), true);

            foreach ($resultados['rows'][0]['elements'] as $key => $a) {
                $distancia[] = ($a['distance']['text']." a ".$a['duration']['text']);
            }
            
        }


        $view = \View('public.buscador.resultados');
        $view->tiendas = $tiendas;             
        $view->distancia = $distancia;             
        return $view;
    }

    public function sugerencia(Request $request)
    {

        $text = $request->text;
        
        if( ctype_space($text) || $text == ""){
            $tags = collect(new Tag);
        }
        else{
            $tags = Tag::where('tags.nombre', 'like', '%'.$text.'%')
                        ->take(5)
                        ->get();
                        //->pluck('nombre', 'id');
        }

        //dd($tags);

        $view = \View('public.buscador.sugerencia');
        $view->tags = $tags;             
        return $view;
    }

    public function getTiendas(Request $request)
    {


        $text = $request->id;
        $plaza = Plaza::find($text);
        //$tiendas = Plaza::find($request->id)->tienda;
        $tiendas = Tienda:: whereHas('local',function($query) use($text){
                                    $query->where('locals.plaza_id', $text);
                                })->                                
                                get();

        //dump($tiendas);

        $lat = $request->lat;
        $lng = $request->lng;

        if($lat == 0){
           $lat = $plaza->ubicacion->latitud; 
        }

        if($lng == 0){
           $lng = $plaza->ubicacion->longitud; 
        }

        $view = \View('public.buscador.tiendas');

        $view->tiendas = $tiendas; 
        $view->plaza = $plaza; 
        $view->lat = $lat; 
        $view->lng = $lng; 
        return $view;
    }

    public function plaza($id)
    {
        if( !$plaza = Plaza::find($id)->load('noticia', 'imagen', 'ubicacion') ){
            abort(404);
        }
        return $plaza;

    }

    public function tienda($id)
    {
        
        if( !$tienda = Tienda::find($id)->load('local.plaza.ubicacion', 'imagen', 'tag', 'categoria') ){
            abort(404);
        }
        return $tienda;
    }

}
