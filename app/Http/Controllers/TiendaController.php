<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Categoria;
use Webmall\Ctienda;
use Webmall\ImagenTienda;
use Webmall\Local;
use Webmall\Plaza;
use Webmall\Tienda;
use Webmall\Tag;
use Webmall\Owner;
use Webmall\Http\Requests\TiendaRequest;
use Image;

class TiendaController extends Controller
{
    public function index()
    {
        
        $tiendas = Tienda::sortable('nombre', 'NOMBRE')->paginate(10);//all();//with('estado')->get();

        $view = \View('admin.tienda.index');

        $view->tiendas = $tiendas; 
        return $view;
    }

    public function tiendas(Request $request)
    {
        $text = $request->text;
        $tiendas = Tienda::whereHas('categoria',function($query) use($text){
                                $query->where('categorias.nombre', 'like', '%'.$text.'%');
                            })->
                            orwhereHas('local',function($query) use($text){
                                $query->where('locals.nombre', 'like', '%'.$text.'%');
                            })->
                            orwhereHas('local',function($query) use($text){
                                $query->whereHas('plaza',function($query) use($text){
                                    $query->where('plazas.nombre', 'like', '%'.$text.'%');
                                });
                            })->
                            orWhere('nombre', 'like', '%'.$text.'%')->
                            //where('plaza_id',$id)->
                            sortable('nombre', 'NOMBRE')->
                            paginate(10);
        /*        
        $tiendas = Tienda:: whereHas('local',function($query) use($id){
                                $query->whereHas('plaza',function($query) use($id){
                                    $query->whereHas('ubicacion',function($query) use($id){
                                        $query->where('ubicacions.ciudad_id', $id);
                                    });   

        */

        $view = \View('admin.tienda.tiendas');
        $view->tiendas = $tiendas; 
        
        return $view; 
    }

    public function create()
    {
        $locals = Local::all();
        $tags = Tag::all('id','nombre as text');
        $plazas = Plaza::all();
        $categorias = Categoria::all();

        $view = \View('admin.tienda.create');
        $view->categorias = $categorias; 
        $view->locals = $locals; 
        $view->tags = $tags; 
        $view->plazas = $plazas; 
        $view->contactos = Ctienda::where("tienda_id",null)->get();

        return $view;  
    }

    public function show($id)
    {
        $tienda = Tienda::find($id);
        
        if( !$owner = Owner::where('tienda_id',$id)->first()){
            $owner = null;
        }

        $tienda->visto = 1;
        $tienda->save();

        $view = \View('admin.tienda.show');
        $view->tienda = $tienda; 
        $view->owner = $owner; 

        return $view; 
    }

    public function contactos(Request $request)
    {
        $id = $request->id;
        $ctiendas = Ctienda::where('tienda_id',$id)
                            ->sortable('nombres', 'NOMBRES')
                            ->paginate(10);

        $view = \View('admin.tienda.contactos');

        $view->ctiendas = $ctiendas; 
        return $view;
    }

    public function store(TiendaRequest $request)
    {

        $cat = Categoria::where('nombre',$request->categoria)->first();
            if( ! $cat ){
                //dump($t);
                $cat = new Categoria();
                $cat->nombre = $request->categoria;
                $cat->imagen = "";
                $cat->save();
            }
        
        if( $imagenData = $request->get('logo-img') ){

            $info = base64_decode(preg_replace("#^data:image/\w+;base64,#i", '', $imagenData));

            $img = Image::make($info);
            $name = (md5( str_random(5).time() ).imgFormat);
            $img->save(public_path('/tie/'.$name));

        }else{
            $name = "";
        }


        $tienda = new Tienda;
        $tienda->nombre = $request->nombre;
        $tienda->local_id = $request->local;
        $tienda->apertura = $request->apertura;
        $tienda->cierre = $request->cierre;
        $tienda->categoria_id = $cat->id;
        $tienda->logo = $name;
        $tienda->visto = 1;
        $tienda->save();



        for ($i=1; $i <= $request->get('foto-id') ; $i++) { 
            
            if($imagenData = $request->get('foto-'.$i.'-img')){
                $info = base64_decode(preg_replace("#^data:image/\w+;base64,#i", '', $imagenData));

                $img = Image::make($info);
                $name = (md5( str_random(5).time() ).imgFormat);
                $img->save(public_path('/tie/'.$name));

                $img = new ImagenTienda;
                $img->tienda_id = $tienda->id;
                $img->imagen = $name;
                $img->save();
            }
        }



        $tags = [];
        if($request->tags){        
            foreach ($request->tags as $key => $t) {
                $tag = Tag::where('nombre',$t)->first();
                if( ! $tag ){
                    //dump($t);
                    $tag = new Tag();
                    $tag->nombre = $t;
                    $tag->save();
                }

                $tags[] = $tag->id;
            }
        }
        $tienda->tag()->sync($tags);


        if( $request->contacto){
            foreach($request->contacto as $c){
                
                $c = Ctienda::find($c);
                $c->tienda_id = $tienda->id;
                $c->save();
                
            }
        }

        return redirect(ADMIN_ROUTE . 'tienda');
    }

    public function edit($id)
    {        
        $tienda = Tienda::find($id);
        //dd($tienda);
        $locals = Local::all();
        $tags = Tag::all('id','nombre as text');
        $plazas = Plaza::all();
        $categorias = Categoria::all();

        $view = \View('admin.tienda.edit');
        $view->categorias = $categorias; 
        $view->locals = $locals; 
        $view->tienda = $tienda;
        $view->plazas = $plazas; 
        $view->tags = $tags; 
        $view->contactos = Ctienda::where("tienda_id",null)->
                                    orWhere("tienda_id",$tienda->id)->
                                    get();
        return $view;       
    }

    public function update(TiendaRequest $request, $id)
    {
        //dump($request->categoria);
        
        $cat = Categoria::where('nombre',$request->categoria)->first();

        //dump($cat);

            if( ! $cat ){
                $cat = new Categoria();
                $cat->nombre = $request->categoria;
                $cat->imagen = "";
                $cat->save();
            }


            //die();
        $tienda = Tienda::find($id);

        $tienda->nombre = $request->nombre;
        $tienda->local_id = $request->local;
        $tienda->apertura = $request->apertura;
        $tienda->cierre = $request->cierre;
        $tienda->categoria_id = $cat->id;
        
        if( $imagenData = $request->get('logo-img') ){
            $info = base64_decode(preg_replace("#^data:image/\w+;base64,#i", '', $imagenData));
            $img = Image::make($info);
            $name = (md5( str_random(5).time() ).'.png');
            $img->save(public_path('/tie/'.$name));

            \File::delete(public_path().'/tie/'.$tienda->logo);
            $tienda->logo = $name;
        }
        
        $tienda->save();

        for ($i=1; $i <= $request->get('foto-id') ; $i++) { 
            
            if($imagenData = $request->get('foto-'.$i.'-img')){
                $info = base64_decode(preg_replace("#^data:image/\w+;base64,#i", '', $imagenData));

                $img = Image::make($info);
                $name = (md5( str_random(5).time() ).'.png');
                $img->save(public_path('/tie/'.$name));

                $img = new ImagenTienda;
                $img->tienda_id = $tienda->id;
                $img->imagen = $name;
                $img->save();
            }
        }

        $tags = [];
        if($request->tags){        
            foreach ($request->tags as $key => $t) {
                $tag = Tag::where('nombre',$t)->first();
                if( ! $tag ){
                    //dump($t);
                    $tag = new Tag();
                    $tag->nombre = $t;
                    $tag->save();
                }

                $tags[] = $tag->id;
            }
        }
        $tienda->tag()->sync($tags);

        return redirect(ADMIN_ROUTE . 'tienda');
    }

    public function destroy(Request $request, $id)
    {
        $tienda = Tienda::find($id);

        \File::delete(public_path().'/tie/'.$tienda->logo);

        foreach ($tienda->imagen as $key => $img) {
            \File::delete(public_path().'/tie/'.$img->imagen);
            $img->delete();
        }
        
        $tienda->delete();

        if($request->ajax()){
                return "AJAX";
        }

        return redirect(ADMIN_ROUTE . 'tienda');
    }

    public function borrarLogo($id)
    {
        $Tienda = Tienda::find($id);
        \File::delete(public_path().'/tie/'.$Tienda->logo);
        $Tienda->logo = "";
        $Tienda->save();

        return redirect(ADMIN_ROUTE . 'tienda');
    }


    public function borrarImagen($id)
    {
        $img = ImagenTienda::find($id);
        \File::delete(public_path().'/tie/'.$img->imagen);
        $img->delete();
        
        
        return redirect(ADMIN_ROUTE . 'tienda');
    }


    public function getTiendas(){
        return Tienda::all();
    }

    public function locales(Request $request)
    {
        $locals = Local::where('plaza_id',$request->id)->has('tienda','<',1)
                        ->orWhere([ ['id',$request->local], ['plaza_id',$request->id] ])
                        ->get();
        $view = \View('admin.tienda.locales');
        
        if($locals->count() < 1){
            $local = new Local;
            $local->id = 0; 
            $local->nombre = "Sin Locales disponibles";

            $locals = collect();
            $locals->push($local);
        }

        $view->locals = $locals;
        return $view;
    }

    public function tags(Request $request){
        return Tag::where('nombre','LIKE','%'.$request->q."%")
                    ->get( array( 'id','nombre as text') );
    }
}

