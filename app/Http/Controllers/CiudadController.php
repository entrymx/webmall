<?php

namespace Webmall\Http\Controllers;

use Illuminate\Http\Request;
use Webmall\Ciudad;
use Webmall\Estado;
use Webmall\Plaza;
use Webmall\Tienda;
use Webmall\Http\Requests\CiudadRequest;

class CiudadController extends Controller
{
    public function index()
    {
        
        $ciudades = Ciudad::sortable('nombre', 'NOMBRE')->paginate(10);

        $view = \View('admin.ciudad.index');

        $view->ciudades = $ciudades; 
        return $view;

    }

    public function create()
    {
        $estados = Estado::orderBy('nombre')->get();

        $view = \View('admin.ciudad.create');

        $view->estados = $estados; 
        return $view;
    }

    public function store(CiudadRequest $request)
    {

        $ciudad = Ciudad::where([ ['nombre', $request->nombre], ['estado_id', $request->estado] ])->first();
        if($ciudad == null){
            $ciudad = new Ciudad;
            $ciudad->nombre = $request->nombre;
            $ciudad->estado_id = $request->estado;
            $ciudad->save();
            return redirect(ADMIN_ROUTE . 'ciudad');
        }else{
            return back()->withErrors(['nombre'=> 'La ciudad ya existe'])->withInput();
        }

    }

    public function show($id)
    {
        $ciudad = Ciudad::find($id);

        $view = \View('admin.ciudad.show');
        $view->ciudad = $ciudad; 

        return $view; 
    }

    public function plazas(Request $request)
    {

        //return 'hola';
        $id = $request->id;
        $plazas = Plaza::whereHas('ubicacion',function($query) use($id){
                            $query->where('ubicacions.ciudad_id', $id);
                        })
                        ->sortable('nombre', 'NOMBRE')->paginate(5);

        $view = \View('admin.ciudad.plazas');
        $view->plazas = $plazas; 
        
        return $view; 
    }

    public function tiendas(Request $request)
    {
        $id = $request->id;
        $tiendas = Tienda:: whereHas('local',function($query) use($id){
                                $query->whereHas('plaza',function($query) use($id){
                                    $query->whereHas('ubicacion',function($query) use($id){
                                         $query->where('ubicacions.ciudad_id', $id);
                                    });    
                                });
                            })   
                            ->sortable('nombre', 'NOMBRE')->paginate(5);

        $view = \View('admin.ciudad.tiendas');
        $view->tiendas = $tiendas; 
        
        return $view; 
    }

    public function edit($id)
    {        
        $estados = Estado::orderBy('nombre')->get();
        $ciudad = Ciudad::find($id);

        $view = \View('admin.ciudad.edit');

        $view->ciudad = $ciudad; 
        $view->estados = $estados; 

        return $view;        
    }

    public function update(CiudadRequest $request, $id)
    {

        $ciudad = Ciudad::where([ ['nombre', $request->nombre], ['estado_id', $request->estado] ])->first();
        if($ciudad == null || $ciudad->id == $id){
            $ciudad = Ciudad::find($id);
            $ciudad->nombre = $request->nombre;
            $ciudad->estado_id = $request->estado;
            $ciudad->save();
            return redirect(ADMIN_ROUTE . 'ciudad');
        }else{
            return back()->withErrors(['nombre'=> 'La ciudad ya existe'])->withInput();
        }

    }


    public function destroy($id)
    {
        $ciudad = Ciudad::find($id);
        $ciudad->delete();

        return redirect(ADMIN_ROUTE . 'ciudad');
    }
}
