<?php

namespace Webmall\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Webmall\Categoria;
use Webmall\Tienda;
use Webmall\Http\Requests\CategoriaRequest;
use Image;

class CategoriaController extends Controller
{
    public function index()
    {
        
        $categorias = Categoria::sortable('nombre', 'NOMBRE')->paginate(10);

        $view = \View('admin.categoria.index');

        $view->categorias = $categorias; 
        return $view;

    }

    public function create()
    {
        $view = \View('admin.categoria.create');

        //$view->categorias = $categorias; 
        return $view;
    }

    public function store(CategoriaRequest $request)
    {
        if( $imagenData = $request->get('imagen-cropped') ){

            $info = base64_decode(preg_replace("#^data:image/\w+;base64,#i", '', $imagenData));

            $img = Image::make($info);
            $name = (md5( str_random(5).time() ).'.png');
            $img->save(public_path('/cat/'.$name));

        }else{
            $name = "";
        }
        $categoria = new Categoria;
        $categoria->nombre = $request->nombre;
        $categoria->imagen = $name;
        $categoria->save();

        return redirect(ADMIN_ROUTE . 'categoria');

    }

    public function show($id)
    {
        $categoria = Categoria::find($id);
        $tiendas = Tienda::where('categoria_id',$id)
                    ->sortable('nombre', 'NOMBRE')
                    ->paginate(5);

        $view = \View('admin.categoria.show');
        
        $view->categoria = $categoria; 
        $view->tiendas = $tiendas; 

        return $view; 
    }

    
    public function edit($id)
    {        
        $categoria = Categoria::find($id);
    	
    	$view = \View('admin.categoria.edit');

        $view->categoria = $categoria; 
        
        return $view;        
    }

    public function update(CategoriaRequest $request, $id)
    {

        $imagenData = $request->get('imagen-cropped');
        $info = base64_decode(($imagenData));

        //dd($request->get('imagen-cropped'));



        $categoria = Categoria::find($id);
        $categoria->nombre = $request->nombre;


        if( $imagenData = $request->get('imagen-cropped') ){
            \File::delete(public_path().'/cat/'.$categoria->imagen);

            $info = base64_decode(preg_replace("#^data:image/\w+;base64,#i", '', $imagenData));

            $img = Image::make($info);
            $name = (md5( str_random(5).time() ).'.png');
            $img->save(public_path('/cat/'.$name));
            $categoria->imagen = $name;
            //return $img->response();
        }else{
            $name = "";
        }

        $categoria->save();

        return redirect(ADMIN_ROUTE . 'categoria');
    }


    public function destroy(Request $request, $id)
    {
        $categoria = Categoria::find($id);
        

        if( $categoria->tienda->count() > 0 ){
            return (0);
        }

        \File::delete(public_path().'/cat/'.$categoria->imagen);
        $categoria->imagen = "";
        $categoria->save();
        $categoria->delete();

        if($request->ajax()){
                return "AJAX";
        }


        return redirect(ADMIN_ROUTE . 'categoria');
    }


    public function borrarImagen($id)
    {
        $categoria = Categoria::find($id);
        \File::delete(public_path().'/cat/'.$categoria->imagen);
        $categoria->imagen = "";
        $categoria->save();

        return redirect(ADMIN_ROUTE . 'categoria');
    }

}
