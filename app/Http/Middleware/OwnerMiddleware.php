<?php

namespace Webmall\Http\Middleware;

use Sentinel;
use Closure;

class OwnerMiddleware
{

    public function handle($request, Closure $next)
    {
        //return $next($request);

        if(Sentinel::check() && (Sentinel::getUser()->roles()->first()->slug == 'owner')){
            return $next($request);        
        }else{
            abort(403);
        }     

    }
}
