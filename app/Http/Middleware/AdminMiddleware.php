<?php

namespace Webmall\Http\Middleware;

use Sentinel;
use Closure;

class AdminMiddleware
{

    public function handle($request, Closure $next)
    {
        //return $next($request);

        if(Sentinel::check() && (Sentinel::getUser()->roles()->first()->slug == 'admin')){
            return $next($request);        
        }else{
            abort(403);
        }     

    }
}
