<?php

namespace Webmall\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SesionRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required',//|email',
            'password' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.required'  => 'Ingresa tu Usuario.',
            'password.required'  => 'Ingresa tu Contraseña.'
        ];
    }

}
