<?php

namespace Webmall\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NoticiaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }   
    public function rules()
    {        
        $ciudad = \Webmall\Ciudad::find($this->ciudad);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'titulo' => 'required',
                    'fecha'  => 'required',
                    'plaza'  => 'required'
                ];
            }
            case 'PUT':
            {
                return [
                    'titulo' => 'required',
                    'fecha'  => 'required',
                    'plaza'  => 'required'
                ];
            }
            case 'PATCH':
            {
                return [
                    'titulo' => 'required',
                    'fecha'  => 'required',
                    'plaza'  => 'required'          
                ];
            }
            default:break;
        }

    }

    public function messages()
    {
        return [
            'required'  => 'Este campo es requerido.',
            'unique'    => 'Este nombre de plaza ya se está usando, ingresa uno diferente.'
        ];
    }
}
