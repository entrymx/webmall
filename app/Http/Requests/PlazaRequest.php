<?php

namespace Webmall\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlazaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {        
        $plaza = \Webmall\Plaza::find($this->plaza);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'nombre' => 'required|unique:plazas',
                    'apertura' => 'required',
                    'cierre' => 'required',

                    'domicilio'=> 'required',
                    'latitud'=> 'required',
                    'longitud'=> 'required',
                    'estado'=> 'required',
                    'ciudad'=> 'required'

                ];
            }
            case 'PUT':
            {
                return [
                    'nombre' => 'required|unique:plazas,nombre,'.$plaza->id,
                    'apertura' => 'required',
                    'cierre' => 'required',

                    'domicilio'=> 'required',
                    'latitud'=> 'required',
                    'longitud'=> 'required',
                    'estado'=> 'required',
                    'ciudad'=> 'required'

                    //'username' => 'required|unique:users,username,'.$coordinador->user_id,

                ];
            }
            case 'PATCH':
            {
                return [
                    'nombre' => 'required|unique:plazas,nombre,'.$plaza->id,
                    'apertura' => 'required',
                    'cierre' => 'required',

                    'domicilio'=> 'required',
                    'latitud'=> 'required',
                    'longitud'=> 'required',
                    'estado'=> 'required',
                    'ciudad'=> 'required'

                    //'username' => 'required|unique:users,username,'.$coordinador->user_id,

                ];
            }
            default:break;
        }

    }

    public function messages()
    {
        return [
            'required'  => 'Este campo es requerido.',
            'unique'    => 'Este nombre de plaza ya se está usando, ingresa uno diferente.'
        ];
    }
}
