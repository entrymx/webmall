<?php

namespace Webmall\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OnwerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {        

        $owner  = \Webmall\User::find($this->owner);
        //$cliente = Cliente::find($this->clientes);

        //dd($user);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|email|unique:users'        
                ];
            }
            case 'PUT':
            {
                return [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|email|unique:users,email,'.$owner->id
                    //'email' => 'required|email|unique:users,email,'.$cliente->id,
                ];
            }
            case 'PATCH':
            {
                return [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|email|unique:users,email,'.$owner->id
                ];
            }
            default:break;
        }

    }

    public function messages()
    {
        return [
            'required'  => 'Este campo es requerido.',
            'email'     => 'Debe ser una dirección de correo válida.',
            'unique'    => 'Este usuario ya se está usando, ingresa uno diferente.'
        ];
    }
}
