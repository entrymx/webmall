<?php

namespace Webmall\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }   
    public function rules()
    {        
        $tag = \Webmall\Tag::find($this->tag);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'nombre' => 'required|unique:tags'
                ];
            }
            case 'PUT':
            {
                return [
                    'nombre' => 'required|unique:tags,nombre,'.$tag->id
                ];
            }
            case 'PATCH':
            {
                return [
                    'nombre' => 'required|unique:tags,nombre,'.$tag->id                   
                ];
            }
            default:break;
        }

    }

    public function messages()
    {
        return [
            'required'  => 'Este campo es requerido.',
            'unique'    => 'Este nombre de plaza ya se está usando, ingresa uno diferente.'
        ];
    }
}
