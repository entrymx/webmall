<?php

namespace Webmall\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CiudadRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }   
    public function rules()
    {        
        $ciudad = \Webmall\Ciudad::find($this->ciudad);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'nombre' => 'required'//|unique:ciudads'
                ];
            }
            case 'PUT':
            {
                return [
                    'nombre' => 'required'//|unique:ciudads,nombre,'.$ciudad->id
                ];
            }
            case 'PATCH':
            {
                return [
                    'nombre' => 'required'//|unique:ciudads,nombre,'.$ciudad->id                   
                ];
            }
            default:break;
        }

    }

    public function messages()
    {
        return [
            'required'  => 'Este campo es requerido.',
            'unique'    => 'Este nombre de plaza ya se está usando, ingresa uno diferente.'
        ];
    }
}
