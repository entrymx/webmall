<?php

namespace Webmall\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LocalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {        
        $plaza = \Webmall\Plaza::find($this->plaza);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'nombre' => 'required',
                    //'estado' => 'required',
                    
                    'plaza' => 'required',
                    'tipo'=> 'required'
                ];
            }
            case 'PUT':
            {
                return [
                    'nombre' => 'required',
                    //'estado' => 'required',
                    
                    'plaza' => 'required',
                    'tipo'=> 'required'
                ];
            }
            case 'PATCH':
            {
                return [
                    'nombre' => 'required',
                    //'estado' => 'required',
                    
                    'plaza' => 'required',
                    'tipo'=> 'required'
                ];
            }
            default:break;
        }

    }

    public function messages()
    {
        return [
            'required'  => 'Este campo es requerido.',
            'unique'    => 'Este nombre de plaza ya se está usando, ingresa uno diferente.'
        ];
    }
}

