<?php

namespace Webmall\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {        
        $categoria = \Webmall\Categoria::find($this->categorium);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'nombre' => 'required|unique:categorias'
                ];
            }
            case 'PUT':
            {
                return [
                    'nombre' => 'required|unique:categorias,nombre,'.$categoria->id
                ];
            }
            case 'PATCH':
            {
                return [
                    'nombre' => 'required|unique:categorias,nombre,'.$categoria->id                   
                ];
            }
            default:break;
        }

    }

    public function messages()
    {
        return [
            'required'  => 'Este campo es requerido.',
            'unique'    => 'Este nombre de plaza ya se está usando, ingresa uno diferente.'
        ];
    }
}