<?php

namespace Webmall\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TipoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {        
        $tipo = \Webmall\Tipo::find($this->tipo);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'nombre' => 'required|unique:tipos'
                ];
            }
            case 'PUT':
            {
                return [
                    'nombre' => 'required|unique:tipos,nombre,'.$tipo->id,
                ];
            }
            case 'PATCH':
            {
                return [
                    'nombre' => 'required|unique:tipos,nombre,'.$tipo->id,
                ];
            }
            default:break;
        }

    }

    public function messages()
    {
        return [
            'required'  => 'Este campo es requerido.',
            'unique'    => 'Este tipo ya se está usando, ingresa uno diferente.'
        ];
    }
}
