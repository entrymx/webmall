<?php

namespace Webmall\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EstadoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }   
    public function rules()
    {        
        $estado = \Webmall\Estado::find($this->estado);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'nombre' => 'required|unique:estados'
                ];
            }
            case 'PUT':
            {
                return [
                    'nombre' => 'required|unique:estados,nombre,'.$estado->id
                ];
            }
            case 'PATCH':
            {
                return [
                    'nombre' => 'required|unique:estados,nombre,'.$estado->id                   
                ];
            }
            default:break;
        }

    }

    public function messages()
    {
        return [
            'required'  => 'Este campo es requerido.',
            'unique'    => 'Este nombre de plaza ya se está usando, ingresa uno diferente.'
        ];
    }
}

