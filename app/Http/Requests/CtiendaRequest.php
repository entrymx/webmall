<?php

namespace Webmall\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CtiendaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {        
        //$plaza = \Webmall\Plaza::find($this->plaza);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'nombres' => 'required',
                    'apellidos' => 'required',
                    'correo' => 'required|email',

                    'telefono'=> 'required',
                    'tienda'=> 'required'
                ];
            }
            case 'PUT':
            {
                return [
                    'nombres' => 'required',
                    'apellidos' => 'required',
                    'correo' => 'required|email',

                    'telefono'=> 'required',
                    'tienda'=> 'required'
                ];
            }
            case 'PATCH':
            {
                return [
                    'nombres' => 'required',
                    'apellidos' => 'required',
                    'correo' => 'required|email',

                    'telefono'=> 'required',
                    'tienda'=> 'required'
                ];
            }
            default:break;
        }

    }

    public function messages()
    {
        return [
            'required'  => 'Este campo es requerido.',
            'email'    => 'Tiene que ser un correo válido'
        ];
    }
}
