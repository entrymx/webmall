<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ImagenOwner extends Model
{
    use SoftDeletes;
    public $timestamps = 'true';


	public function Owner(){
		return $this->belongsTo('\Webmall\Owner');
	}

}
