<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Kyslik\ColumnSortable\Sortable;

class Owner extends Model
{
	use SoftDeletes, Sortable;
    public $timestamps = 'true';
    
	public function Tienda(){
		return $this->hasMany('\Webmall\Tienda');
	}  

    public function Imagen(){
        return $this->hasMany('\Webmall\ImagenOwner');
    }


/*
    public $sortable = [
        'nombre',
        'cierre',
        'apertura',
        'categoria_id',
        'local_id',
        'user_id'
    ];
    */
}
