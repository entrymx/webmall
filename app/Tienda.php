<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Kyslik\ColumnSortable\Sortable;

class Tienda extends Model
{
	use SoftDeletes, Sortable;
    public $timestamps = 'true';
    
	public function Categoria(){
		return $this->belongsTo('\Webmall\Categoria');
	}

	public function Local(){
		return $this->belongsTo('\Webmall\Local');
	}

    public function Tag()
    {
        return $this->belongsToMany('Webmall\Tag');
    }


    public function movimientos()
    {
        return $this->morphMany('Webmall\Watchdog', 'movimiento');
    } 

    public function Imagen(){
        return $this->hasMany('\Webmall\ImagenTienda');
    }    

    public function Ctienda(){
        return $this->hasMany('\Webmall\Ctienda');
    }    


    public function User(){
        return $this->belongsTo('\Webmall\User');
    }

    public function Owner(){
        return $this->belongsTo('\Webmall\Owner');
    }

    public $sortable = [
        'nombre',
        'cierre',
        'apertura',
        'categoria_id',
        'local_id',
        'user_id'
    ];
}
