<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Kyslik\ColumnSortable\Sortable;

class Estado extends Model
{
    use SoftDeletes, Sortable;//

    public $timestamps = 'true';

	public function Ciudad(){
		return $this->hasMany('\Webmall\Ciudad');
	}

    public $sortable = [
        'nombre'
        ];
}
