<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Kyslik\ColumnSortable\Sortable;

class Plaza extends Model
{
	use SoftDeletes, Sortable;
    public $timestamps = 'true';


	public function Ubicacion(){
		return $this->belongsTo('\Webmall\Ubicacion');
	}

	public function Local(){
		return $this->hasMany('\Webmall\Local');
	}

	public function Noticia(){
		return $this->hasMany('\Webmall\Noticia');
	}

	public function Imagen(){
		return $this->hasMany('\Webmall\ImagenPlaza');
	}

	public function Contacto(){
		return $this->hasMany('\Webmall\Contacto');
	}


    public function movimientos()
    {
        return $this->morphMany('Webmall\Watchdog', 'movimiento');
    }

    public $sortable = [
        'nombre',
        'cierre',
        'apertura',
        'categoria_id',
        'local_id',
        'ubicacion_id'
    ];
}
