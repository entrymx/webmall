<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ImagenTienda extends Model
{
    use SoftDeletes;
    public $timestamps = 'true';


	public function Tienda(){
		return $this->belongsTo('\Webmall\Tienda');
	}
}
