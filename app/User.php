<?php namespace Webmall;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Kyslik\ColumnSortable\Sortable;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, Sortable;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	
	//protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
							'username',//Agregado
							'name',
							'email',
							'password'
						];


	protected $loginNames = ['email', 'username'];


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function Tienda(){
		return $this->hasMany('\Webmall\Tienda');
	}

	public function fullName()
	{
		return ($this->first_name." ".$this->last_name);
	}
    public $sortable = [
		'first_name',
        'last_name',
		'email'
    ];
}
