<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ImagenPlaza extends Model
{
    use SoftDeletes;
    public $timestamps = 'true';


	public function Plaza(){
		return $this->belongsTo('\Webmall\Plaza');
	}

}
