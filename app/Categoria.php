<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Categoria extends Model
{
	use SoftDeletes, Sortable;
    public $timestamps = 'true';

	public function Tienda(){
		return $this->hasMany('\Webmall\Tienda');
	}

    public $sortable = [
        'nombre'
    ];
}
