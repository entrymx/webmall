<?php

namespace Webmall;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Kyslik\ColumnSortable\Sortable;

class Tag extends Model
{
    use SoftDeletes, Sortable;//

    public function Tienda()
    {
        return $this->belongsToMany('Webmall\Tienda');
    }

    public $sortable = ['nombre'];
	                    
}
