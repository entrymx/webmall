@extends('public.layout')
@section('css')

<style type="text/css">
    .blanco{
        background-color: white;
    }
    .alto{
        height:250px;
    }
</style>
@endsection

@section('contenido')
<section id="header">
    <div class="gap20vh"></div>
    <div class="container">
        <div class="row">                
            <div class="col-sm-2 col-sm-offset-5">
                <div class="card">
                    <div class="body ">
                        <img src="<?= IMG; ?>logo_ok.png" class="img-responsive" alt="logo" width="100%">
                    </div>
                </div>    
            </div>                
        </div>
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <h2>Todas las plazas, un solo Sitio</h2>
            </div>
        </div>
        <div class="gap2"></div>
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="form-group">
                    <span class="ti-search"></span>
                    <input type="text" name="buscar" id="buscar" placeholder="Tiendas, Marcas, Productos..." class="form-control">
                </div>
            </div>
        </div>
        <!--div class="row loader">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="card">
                    <div class="body ">
                        <img class="img-responsive" src="spinner.gif"></img>
                    </div>  
                </div>
            </div>
        </div-->

        <div class="row alto" >
            <div class="col-sm-2 col-sm-offset-2">
                <div class="card">
                    <div class="head blanco text-center">
                        <h3>Sugerencias</h3>
                    </div>
                    <div class="body sugerencia ">

                    </div>  
                </div>
            </div>
            <div class="col-sm-6 ">
                <div class="card">
                    <div class="head blanco text-center">
                        <h3>Resultados</h3>
                    </div>
                    <div class="body resultado">

                    </div>  
                </div>
            </div>

        </div>
        <div class="gap2"></div>
        <div class="row">
            <div class="col-sm-1">
            </div>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-3">
                        <img src="<?= IMG; ?>logos/ae.png" class="img-responsive">
                    </div>
                    <div class="col-sm-3">
                        <img src="<?= IMG; ?>logos/cinemex.png" class="img-responsive">
                    </div>
                    <div class="col-sm-3">
                        <img src="<?= IMG; ?>logos/gandhi.png" class="img-responsive">
                    </div>
                    <div class="col-sm-3">
                        <img src="<?= IMG; ?>logos/kfc.png" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>
        
    </div>
</section>
<section id="plazas">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="center">PLAZAS</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table class="table-responsive table">
                    <tr>
                    @foreach($plazas as $plaza)                            
                        <td>

                        @if($plaza->logo)
                            <a data-id={{$plaza->id}} class="plaza">
                                <img class="img-responsive " src="/pla/{{($plaza->logo) }}">
                            </a>
                        @else
                            <a data-id={{$plaza->id}} class="plaza">{{$plaza->nombre}}</a>
                        @endif
                        </td>
                    @endforeach
                    </table>
                </div>
            </div>
            <div class="row tiendas">
                <div class="col-sm-8">
                    <h2 class="center">Ubicación</h2>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3643.9884944336786!2d-104.67233338469332!3d24.031470883896887!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x869bc824cc0d9ff7%3A0x67d1003a5b88976a!2sVictoria+Nte.+395%2C+Zona+Centro%2C+34000+Durango%2C+Dgo.!5e0!3m2!1ses-419!2smx!4v1503435584274" width="100%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col-sm-4">
                    <h2 class="center">Tiendas</h2>
                    <ul>
                        <li>Tienda 1</li>
                        <li>Tienda 1</li>
                        <li>Tienda 1</li>
                        <li>Tienda 1</li>
                        <li>Tienda 1</li>
                        <li>Tienda 1</li>
                        <li>Tienda 1</li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="gap2"></div>
    </section>
    <section id="promociones">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="center">PROMOCIONES</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="<?= IMG; ?>green.jpg" alt="yellow">
                        </div>

                        <div class="item">
                          <img src="<?= IMG; ?>red.jpg" alt="blue">
                      </div>

                      <div class="item">
                          <img src="<?= IMG; ?>blue.jpg" alt="green">
                      </div>
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="gap2"></div>
</section>
<section id="eventos">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="center">EVENTOS</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <h2>Evento 1</h2>
                <img src="<?= IMG; ?>noti/01.jpg" class="img-responsive">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
            </div>
            <div class="col-sm-4">
                <h2>Evento 2</h2>
                <img src="<?= IMG; ?>noti/02.jpg" class="img-responsive">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
            </div>
            <div class="col-sm-4">
                <h2>Evento 3</h2>
                <img src="<?= IMG; ?>noti/03.jpg" class="img-responsive">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <p class="center">&copy; 2017 | Webmall</p>
            </div>
        </div>
    </div>
</footer>

@section('scripts')


<script type="text/javascript">

    ///////////   Funciones    ////////////////////////////////////////////////        

        function sugerencia(text) {

            $.ajax({
                url: "/sugerencia",
                type: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    text:text
                },
                beforeSend: function() {
                    //$('.loader').fadeIn();
                    //$('.resultado').fadeOut();
                }
            })            
            .done(function(data) {
                //$('.loader').fadeOut();

                if($('.sugerencia').html() != data){
                    $('.sugerencia').html(data).fadeIn();
                }

            })
            .error(function(data) {
                //swal("Error", "Ocurrió un problema", "error");
                alert("Error, Ocurrió un problema, error");
            });
        }

        function buscar(text) {

            $.ajax({
                url: "/buscar",
                type: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    text:text,
                    lat: lat,
                    lng: lng
                },
                beforeSend: function() {
                    //$('.loader').fadeIn();
                    //$('.resultado').fadeOut();
                }
            })            
            .done(function(data) {
                //$('.loader').fadeOut();

                if($('.resultado').html() != data){
                    $('.resultado').html(data).fadeIn();
                }

            })
            .error(function(data) {
                //swal("Error", "Ocurrió un problema", "error");
                alert("Error, Ocurrió un problema, error");
            });
        }

        function getTiendas(id) {

            $.ajax({
                url: "/getTiendas",
                type: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    id: id,
                    lat: lat,
                    lng: lng
                }
            })
            .done(function(data) {
                $('.tiendas').html(data);
            })
            .error(function(data) {
                    //swal("Error", "Ocurrió un problema", "error");
                    alert("Error, Ocurrió un problema, error");
                });

        }

        function getUserLocation() { //user clicks button
            if (navigator.geolocation){ //check geolocation available 
                //try to get user current location using getCurrentPosition() method
                navigator.geolocation.getCurrentPosition(function(position){ 
                    lat = position.coords.latitude;
                    lng = position.coords.longitude;

                    //alert(lat+" "+lng);
                    getTiendas({{$plaza->first()->id}});
                        //$("#result").html("Found your location <br />Lat : "+position.coords.latitude+" </br>Lang :"+ position.coords.longitude);
                });
            }else{
                console.log("Browser doesn't support geolocation!");
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

    ///////////   Eventos      //////////////////////////////////////////////// 

        $('#buscar').on('input',function(e){
            //var t = $('#buscar').val();
            //alert();    
            if(  $('#buscar').val().length >= 3 ){
                buscar($('#buscar').val());
                sugerencia($('#buscar').val());
            }else{
                $('.resultado').html('<div class="body resultado"><h2>Intenta buscando otra cosa</h2></div>').fadeIn();
                $('.sugerencia').html('<div class="body sugerencia"><h2>Intenta buscando otra cosa</h2></div>').fadeIn();
            }
        });

        $(document).on('click', '.sug', function(){             
            $('#buscar').val( $(this).text() );
            buscar($('#buscar').val());
            sugerencia($('#buscar').val());
        });

        $('.plaza').click(function(e){
            //alert('asdasdasd');
            getTiendas($(this).attr('data-id'));
        });

    ///////////   Inicio       ////////////////////////////////////////////////        

        var lat = 0;
        var lng = 0;
        $('.loader').hide();
        getUserLocation();

    </script>

    @endsection
