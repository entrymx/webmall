<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title><?= TITULO; ?></title>
    <?= BOOTSTRAPCSS; ?>
    <?= STYLE; ?>
    <?= STYLE_MOBILE; ?>
    <?= THEMIFY_ICONS; ?>
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans:400,800" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet"> 
    @yield('css')

</head>
<body>
    <?= NAVBAR; ?>
    @yield('contenido')
<?= JQUERY; ?>
<?= BOOTSTRAPJS; ?>

    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
        
    @yield('scripts')
</body>
</html>
