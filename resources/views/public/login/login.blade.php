@extends('public.login.layout')

@section('contenido')

<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        <div class="jumbotron login">
			@if (session('status'))
			    <div class="alert alert-success">
			        {{ session('status') }}
			    </div>
			@endif 
			    	
            <h2>Inicia Sesión</h2>
            <p>Ingresa tu E-mail y Contraseña</p>
            @if(count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="gap-2"></div>
                <form name="login" method="post" action="/login">
                    <div class="form-group">
                        {{ csrf_field() }}
                        <input name="email" type="text" class="form-control" placeholder="E-mail" value={{old('email')}} >
                    </div>
                    <div class="form-group">
                        <input name="password" type="password" class="form-control" placeholder="Contraseña" value="{{old('password')}}" >
                    </div>
                    <button type="submit" class="btn btn-primary form-control">Iniciar Sesión</button>
                    <br><br><br>
                    <a class="restaurar pull-center">¿Olvidaste tu contraseña?</a>
                </form>
        </div>

        <div class="jumbotron olvidada">
            <h2>Restaura tu contraseña</h2>
            <p>Ingresa tu E-mail para restaurar tu Contraseña</p>
            @if(count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="gap-2"></div>
                <form name="login" method="post" action="/login/olvidada">
                    <div class="form-group">
                        {{ csrf_field() }}
                        <input name="email" type="text" class="form-control" placeholder="E-mail" value={{old('email')}} >
                    </div>

                    <button type="submit" class="btn btn-primary form-control">Restaura tu contraseña</button>
                    <br><br><br>
                    <a class="inicio pull-center">Inicia Sesión</a>
                </form>
        </div>

    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){

    	$('.olvidada').hide();

    	$('.restaurar').click(function(){
			$('.login').hide();
    		$('.olvidada').show();
    	})

    	$('.inicio').click(function(){
			$('.login').show();
    		$('.olvidada').hide();
    	})
    });
</script>
@endsection 