<!doctype html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/favicon.png')}}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />


        <!-- Bootstrap core CSS     -->
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />

        <!-- Animation library for notifications   -->
        <link href="{{asset('css/animate.min.css')}}" rel="stylesheet"/>



        <!--  Fonts and icons     -->
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        

        @yield('head')

        <title>WebMall</title>
    </head>
    <body>
        <div class="container">
            <div class="gap-2"></div>
            <div class="row">
                @yield('contenido')
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>   
        <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
         @yield('scripts')
    </body>
</html>