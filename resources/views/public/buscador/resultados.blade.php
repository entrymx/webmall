			    
	
@if($tiendas->count() > 0)
    <div class="body resultado blanco">
        <table class="table table-hover">
            <tbody>
                @foreach($tiendas as $tienda)
                <tr>
                    <td class="text-right">
    		        	<a href="/t/{{$tienda->id}}" target="_blank">
            	            {{$tienda->nombre}}
	    	            </a> 
                    </td>

                    <td class="text-left">
    		        	<a href="/p/{{$tienda->local->plaza->id}}" target="_blank">
            	            {{$tienda->local->plaza->nombre}}
	    	            </a> 
                    </td>
                    @if( (count($distancia) != 0 ) )
                    <td>
        	                {{$distancia[$loop->index]}}                        
                        <!--a href="/t/{{$tienda->id}}" target="_blank">
                        </a--> 
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table> 
    </div>
@else
	<div class="body resultado">
		<h2>Intenta buscando otra cosa</h2>
	</div>
@endif    	