@extends('public.layout')

@section('breadcrums')

	<a href="">Buscador</a>
	
@endsection

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
			    <div class="body ">
					<fieldset>							
						<div class="form-group">
							<div class=" {{ $errors->has('buscar') ? 'has-error' : '' }}">
								{{ Form::label('buscar', 'Buscar') }}
    							{{ Form::text('buscar', old('buscar'), array('class' => 'form-control')) }}													
								<span class="text-danger">{{ $errors->first('buscar') }}</span>
							</div>	
						</div>
						<a class="btn btn-primary form-control buscar">Buscar</a>						
					</fieldset>
			    </div>  
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
			    <div class="body resultado">
					
			    </div>  
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
			    <div class="body">
					<div class="form-group">
						<div id="map"></div>
					</div>
			    </div>  
            </div>
        </div>
    </div>


							

@endsection

@section('scripts')


    <script type="text/javascript">
              
    ///////////   Funciones    ////////////////////////////////////////////////        

	    function buscar(text) {

            $.ajax({
                url: "/buscar",
                type: "POST",
                data:{
                	_token: "{{ csrf_token() }}",
                	text:text
                }
            })
            .done(function(data) {
                $('.resultado').html(data);
            })
            .error(function(data) {
                swal("Error", "Ocurrió un problema", "error");
            });

	    }
		
		function getUserLocation() { //user clicks button
		    if (navigator.geolocation){ //check geolocation available 
		        //try to get user current location using getCurrentPosition() method
		        navigator.geolocation.getCurrentPosition(function(position){ 
		                lat = position.coords.latitude;
		                lng = position.coords.longitude;

                    	alert(lat+" "+lng);

		                //$("#result").html("Found your location <br />Lat : "+position.coords.latitude+" </br>Lang :"+ position.coords.longitude);
		            });
		    }else{
		        console.log("Browser doesn't support geolocation!");
		        x.innerHTML = "Geolocation is not supported by this browser.";
		    }
		}

    ///////////   Eventos      //////////////////////////////////////////////// 

        $(document).on("click",'.buscar',function() {
            buscar($('#buscar').val());
        }); 
    
    ///////////   Inicio       ////////////////////////////////////////////////        


    	var lat = 0;
    	var lng = 0;

    	alert(lat+" "+lng);
		getUserLocation();
    	alert(lat+" "+lng);

    </script>
     
@endsection

