    
    <div class="row tiendas">
        <div class="col-sm-8">
            <h2 class="center">Ubicación</h2>
            <!--iframe src="https://www.google.com/maps/embed?center=24.0324975,-104.6817555" width="100%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>
            <h2 class="center">Ubicación</h2-->
            <iframe src="https://www.google.com/maps/embed/v1/directions?origin={{$lat}},{{$lng}}&destination={{$plaza->ubicacion->latitud}},{{$plaza->ubicacion->longitud}}&key={{KEY}}"            
             width="100%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <div class="col-sm-4">
            <h2 class="center">Tiendas</h2>
            <div class="grid">
                <ul>
                @foreach($tiendas as $t)
                    
                    @if($t->logo != "")
                        <a href="/t/{{($t->id) }}" target="_blank">
                            <img class="img-responsive " src="/tie/{{($t->logo) }}">
                        </a>
                    @else
                        {{$t->nombre}}
                    @endif 
                    
                @endforeach
                </ul>
            </div>
        </div>
    </div>

