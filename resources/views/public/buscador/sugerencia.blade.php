			    
	
@if($tags->count() > 0)
    <div class="body sugerencia blanco">
        <table class="table table-hover">
            <tbody>
                @foreach($tags as $tag)
                <tr>
                    <td class="text-center sug">{{$tag->nombre}}</td>
                </tr>
                @endforeach
            </tbody>
        </table> 
    </div>
@else
	<div class="body sugerencia">
		<h2>Intenta buscando otra cosa</h2>
	</div>
@endif    	