<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

    <div>
        <h1>Nueva contraseña</h1>
        <p>
            Has cambiado tu contraseña.
            <br>
            <strong>{{$user->first_name." ".$user->last_name}}</strong> 
            <br><br>
            En <a href="{{SITIO}}">Web Mall</a> podras ver contenido especialmente para ti.
            <br><br>
            Inicia sesión con tu correo y la siguente contraseña
            <br>
            <table>
                <tr>
                    <td>Correo</td>
                    <td> </td>
                    <td><strong>{{$user->email}}</strong></td>
                </tr>
                <tr>
                    <td>Contraseña</td>
                    <td> </td>
                    <td><strong>{{$pass}}</strong></td>
                </tr>
            </table>

            <br><br>

            Si tienes problemas manda un correo a <strong>{{HELP_MAIL}}</strong>
        </p>

    </div>
</body>
</html> 
