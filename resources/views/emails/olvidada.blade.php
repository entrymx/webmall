<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

    <div>
        <h1>Olvidaste tu contraseña</h1>
        <p>
            <br>
            Estimado <strong>{{$nombre." ".$apellido}}</strong>:
            <br>
            Te enviamos este correo por que solicistaste cambiar tu contraseña.
            <br>
            Si no fuiste tú, no hagas nada, tu contraseña no cambiará.
            <br>
            Si deseas cambiar tu contraseña da click en el siguiente enlace y te enviaremos tu nueva contraseña.

            <br><br>
            En <a href="{{SITIO.$link}}">Cambiar contraseña</a>
            <br><br>

            Si tienes problemas manda un correo a <strong>{{HELP_MAIL}}</strong>
        </p>

    </div>
</body>
</html> 
