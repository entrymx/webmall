<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

    <div>
        <h1>Bienvenid@ a WebMall</h1>
        <p>
            <br>
            Estimad@ <strong>{{$nombre." ".$apellido}}</strong> te damos la bienvenida a Web Mall.
            <br><br> 
            Has sido registrado como <strong>Dueño</strong> 
            <br><br>
            En <a href="{{SITIO}}">Web Mall</a> podras ver contenido especialmente para ti.
            <br><br>
            Inicia sesión con tu correo y la siguente contraseña
            <br><br>
            <table>
                <tr>
                    <td>Correo</td>
                    <td> </td>
                    <td><strong>{{$correo}}</strong></td>
                </tr>

                <tr>
                    <td>Contraseña</td>
                    <td> </td>
                    <td><strong>{{$pass}}</strong></td>
                </tr>
            </table>
            <br><br>
            Si tienes problemas manda un correo a <strong>{{HELP_MAIL}}</strong>.
        </p>

    </div>
</body>
</html> 
