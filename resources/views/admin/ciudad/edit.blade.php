@extends('admin.layout')

@section('breadcrums')

<a href="/ciudad">CIUDAD</a>
-
<a href="">{{$ciudad->nombre}}</a>

@endsection

@section('content')
<div class="row">
	<div class="col-sm-6">			
		<div class="content">
			<h2>Editar Ciudad</h2>			
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="content">
				{{Form::model($ciudad,['method'=>'PATCH','route'=>['ciudad.update',$ciudad->id]])}}

				<fieldset>							
					<div class="form-group">
						<div class=" {{ $errors->has('nombre') ? 'has-error' : '' }}">
							{{ Form::label('nombre', 'Nombre') }}
							{{ Form::text('nombre', old('nombre'), array('class' => 'form-control border-input')) }}													
							<span class="text-danger">{{ $errors->first('nombre') }}</span>
						</div>	
					</div>

					<div class="form-group">
						<div class=" {{ $errors->has('nombre') ? 'has-error' : '' }}">
							{{ Form::label('estado', 'Estados') }}
							<select id='estado' name='estado' title="Ningun estado seleccionado" class="selectpicker form-control show-tick border-input" data-live-search="true">
								@foreach($estados as $e)
								<option value={{$e->id}} >{{$e->nombre}}</option>
								@endforeach
							</select>
							<span class="text-danger">{{ $errors->first('estado') }}</span>
						</div>
					</div>							


					<button class="btn btn-primary form-control btn-fill">Guardar</button>
					<div class="gap-1"></div>
					<br>
					<a href="{{ADMIN_ROUTE}}ciudad" class="btn btn-danger form-control btn-fill">Cancelar</a>
				</fieldset>

				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">


	$('#estado').val({{$ciudad->estado->id}});

</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.nav li').removeClass('active');
		$('.ciudad').addClass('active');

	});
</script>
@endsection