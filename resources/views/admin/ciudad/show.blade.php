@extends('admin.layout')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
    <style>
		.carousel-inner img {
			margin: auto;
		}
	</style>
@endsection

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<div class="content">
				{{Form::model($ciudad,['method'=>'DELETE','route'=>['estado.destroy',$ciudad->id]])}}
					<h2>Ciudad - {{$ciudad->nombre}}
						<a class="btn btn-warning btn-fill pull-right" href="{{$ciudad->id}}/edit" >
							<span class="ti-pencil"></span>
						</a>
						<button class="btn btn-danger btn-fill pull-right" >
							<span class="ti-close"></span>
						</button>
					</h2>			
				{{Form::close()}}
			</div>
		</div>
	</div>

	<div class="card">
		<div class="header">
            <h4 class="title">PLAZAS - {{$ciudad->nombre}}</h4>
        </div>
		<div class="content table-responsive plazas ">
			
		</div>
	</div>

	<div class="card">
		<div class="header">
            <h4 class="title">TIENDAS - {{$ciudad->nombre}}</h4>
        </div>
		<div class="content table-responsive tiendas ">
			
		</div>
	</div>

@endsection

@section('scripts')


    <script type="text/javascript">

		function plazas(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}

			$.ajax({
				url: "{{ADMIN_ROUTE}}ciudad/plazas"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					id: {{$ciudad->id}}
				}
			})
			.done(function(data) {
				$('.plazas').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}

		function tiendas(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}

			$.ajax({
				url: "{{ADMIN_ROUTE}}ciudad/tiendas"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					id: {{$ciudad->id}}
				}
			})
			.done(function(data) {
				$('.tiendas').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}

		$(document).ready(function() {

			$(document).on('click', '.tiendas .pagination a, .tiendas th a', function (e) {
				tiendas($(this).attr('href').split('sort')[1]);
				e.preventDefault();
			});

			$(document).on('click', '.plazas .pagination a, .plazas th a', function (e) {
				plazas($(this).attr('href').split('sort')[1]);
				e.preventDefault();
			});

			$('.nav li').removeClass('active');
			$('.ciudad').addClass('active');

			plazas('');
			tiendas('');
	       
		});
	</script>



@endsection