
<div class="content table-responsive plazas ">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>@sortablelink('nombre','NOMBRE')</th>
				<th>@sortablelink('apertura','HORARIO')</th>
				<th>@sortablelink('ubicacion.domicilio','UBICACIÓN')</th>
				<th>CIUDAD</th>
				<th>CONTACTO</th>
				<th>LOGO</th>
				<th>FOTOS</th>
				<th>EDITAR</th>
				<th>ELIMINAR</th>
			</tr>
		</thead>
		<tbody>
			@foreach($plazas as $plaza)
			<tr>
				<td>
					<a href="{{ADMIN_ROUTE}}plaza/{{$plaza->id}}">{{$plaza->nombre}}</a>
				</td>
				<td>
					{{$plaza->apertura." - ".$plaza->cierre}}
				</td>
				<td>
					{{$plaza->ubicacion->domicilio}}
				</td>
				<td>
					{{$plaza->ubicacion->ciudad->nombre}}, 
					{{$plaza->ubicacion->ciudad->estado->nombre}}
				</td>
				<td>
				@if($plaza->contacto->count() > 0)
				@foreach($plaza->contacto as $contacto)
					&#9702; {{$contacto->nombres}}<br>
				@endforeach
				@else
					Sin contacto
				@endif
				</td>
				<td>
					@if($plaza->logo != "")
					<a href="/pla/{{($plaza->logo) }}" target="_blank"><img class="img-responsive" src="/pla/{{($plaza->logo) }}"></a>
					@else
					Sin Logo
					@endif
				</td>
				<td>	

					<div id="myCarousel-{{$loop->iteration}}" class="carousel slide" data-ride="carousel">

					<!-- Wrapper for slides -->
						<div class="carousel-inner">
						@foreach($plaza->imagen as $imagen)
							<div class="item @if($loop->index < 1) active @endif">
								<a href="/pla/{{($imagen->imagen) }}" target="_blank">
									<img src="/pla/{{($imagen->imagen) }}" class="img-responsive">
								</a>
								<br>									
							</div>										
						@endforeach
						</div>

				    <!-- Left and right controls -->
			    	@if($plaza->imagen->count() > 1)
					    <a class="left carousel-control" href="#myCarousel-{{$loop->iteration}}" data-slide="prev">
					    	<span class="glyphicon glyphicon-chevron-left"></span>
					    	<span class="sr-only">Previous</span>
					    </a>
					    <a class="right carousel-control" href="#myCarousel-{{$loop->iteration}}" data-slide="next">
					    	<span class="glyphicon glyphicon-chevron-right"></span>
					    	<span class="sr-only">Next</span>
					    </a>
					@endif
					</div>
				</td>
				<td>
					<a class="btn btn-warning btn-fill" href="plaza/{{$plaza->id}}/edit" >
						<span class="ti-pencil"></span>
					</a>
				</td>
				<td>
					{{Form::model($plaza,['method'=>'DELETE','route'=>['plaza.destroy',$plaza->id]])}}
					<button class="btn btn-danger btn-fill" >
						<span class="ti-close"></span>
					</button>
					{{Form::close()}}
				</td>
			</tr> 
			@endforeach
		</tbody>
	</table> 		
	{{ $plazas->appends(\Request::except('page'))->render() }}	    
</div>