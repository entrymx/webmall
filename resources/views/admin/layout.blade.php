<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/favicon.png')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Bootstrap core CSS     -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="{{asset('css/paper-dashboard.css')}}" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css" href="{{asset('plugins/sweetalert/sweetalert.min.css')}}">


    <!--  Fonts and icons     -->
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="{{asset('css/public/themify-icons.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{asset('plugins/lightbox/css/lightbox.css')}}">

	<style type="text/css">
		/*Cropit style*/
			.cropit-preview {
				background-color: #f8f8f8;
				background-size: cover;
				border: 3px solid #ccc;
				border-radius: 3px;
				margin-top: 7px;
			}

			.cropit-preview-background {
				opacity: .2;
			}
			.cropit-preview-image-container {
				cursor: move;
			}

			.cropit-image-input{
				visibility: hidden;
			}

			.cropit-image-zoom-input{
				width:90% !important; 
				display:inline-block !important; 
			}

			.image-size-label {
				margin-top: 10px;
			}

			.image-cropper input, .export {
				display: block;
			}

			.image-cropper button {
				margin-top: 10px;
			}
		/* //////// */
	</style>


    @yield('head')

    <title>WebMall</title>
</head>

<body>
    @php($user = Webmall\User::find(\Sentinel::getUser()->id))
    @php($noti = Webmall\Tienda::where('visto',0)->get()))
    <div class="wrapper">
        <div class="sidebar" data-background-color="white" data-active-color="danger">
            <!--
                Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
                Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
            -->
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="{{ ADMIN_ROUTE }}" class="simple-text">
                        Webmall Admin
                    </a>
                </div>

                <ul class="nav">
                    <li class="contacto">
                        <a href="{{ ADMIN_ROUTE }}contacto">
                            <i class="ti-user"></i>
                            <p>Contacto Plaza</p>
                        </a>
                    </li>
                    <li class="ctienda">
                        <a href="{{ ADMIN_ROUTE }}ctienda">
                            <i class="ti-user"></i>
                            <p>Contacto Tienda</p>
                        </a>
                    </li>
                    <li class="owner">
                        <a href="{{ ADMIN_ROUTE }}owner">
                            <i class="ti-user"></i>
                            <p>Dueño de Tienda</p>
                        </a>
                    </li>
                    <li class="plaza">
                        <a href="{{ ADMIN_ROUTE }}plaza">
                            <i class="ti-home"></i>
                            <p>Plazas</p>
                        </a>
                    </li>
                    <li class="local">
                        <a href="{{ ADMIN_ROUTE }}local">
                            <i class="ti-harddrives"></i>
                            <p>Locales</p>
                        </a>
                    </li>
                    <li class="tipo">
                        <a href="{{ ADMIN_ROUTE }}tipo">
                            <i class="ti-panel"></i>
                            <p>Tipos de Local</p>
                        </a>
                    </li>
                    <li class="tienda">
                        <a href="{{ ADMIN_ROUTE }}tienda">
                            <i class="ti-bag"></i>
                            <p>Tiendas</p>
                        </a>
                    </li>
                    <li class="categoria">
                        <a href="{{ ADMIN_ROUTE }}categoria">
                            <i class="ti-layers"></i>
                            <p>Categorías de Tienda</p>
                        </a>
                    </li>
                    <li class="tag">
                        <a href="{{ ADMIN_ROUTE }}tag">
                            <i class="ti-pin-alt"></i>
                            <p>Tags de Tienda</p>
                        </a>
                    </li>
                    <li class="estado">
                        <a href="{{ ADMIN_ROUTE }}estado">
                            <i class="ti-layout-grid2"></i>
                            <p>Estados</p>
                        </a>
                    </li>
                    <li class="ciudad">
                        <a href="{{ ADMIN_ROUTE }}ciudad">
                            <i class="ti-layout-grid3"></i>
                            <p>Ciudades</p>
                        </a>
                    </li>
                    <!--li class="ubicacion">
                        <a href="{{ ADMIN_ROUTE }}ubicacion">
                            <i class="ti-location-pin"></i>
                            <p>Ubicaciones</p>
                        </a>
                    </li-->
                    <li class="noticia">
                        <a href="{{ ADMIN_ROUTE }}noticia">
                            <i class="ti-comment-alt"></i>
                            <p>Noticias</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar bar1"></span>
                            <span class="icon-bar bar2"></span>
                            <span class="icon-bar bar3"></span>
                        </button>
                        <a class="navbar-brand" href="#"></a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <!--li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-panel"></i>
                                    <p>Stats</p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="ti-settings"></i>
                                    <p>Configuración</p>
                                </a>
                            </li-->

                            <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-bell"></i>
                                    <p class="notification">{{$noti->count()}}</p>
                                    <p>Notificaciones</p>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    @foreach($noti as $n)
                                    <li><a href="{{ADMIN_ROUTE.'tienda/'.$n->id}}">{{$n->nombre}}</a></li>
                                    @endforeach
                                </ul>
                            </li>

                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-user"></i>
                                    <p>{{$user->fullName()}}</p>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="/logout">Cerrar Sesión</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        @yield('breadcrumbs')
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        @yield('content')
                    </div>
                </div>
            </div>

            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright pull-right">
                        &copy; <script>document.write(new Date().getFullYear())</script>, Desarrollo: <a href="http://www.entrymx.com" target="_blank">Entry</a>
                    </div>
                </div>
            </footer>

        </div>
    </div>

    <!--   Core JS Files   -->
    <script src="{{asset('js/jquery-1.10.2.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>

    <!--  Notifications Plugin    -->
    <script src="{{asset('js/bootstrap-notify.js')}}"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="{{asset('js/paper-dashboard.js')}}"></script>

    <script src="{{ asset('plugins/lightbox/js/lightbox.js')}}"></script>

    <script src="{{ asset('plugins/cropit/dist/jquery.cropit.js')}}"></script>

    <script src="{{ asset('plugins/sweetalert/sweetalert.min.js')}}"></script>

    <script>
        lightbox.option({
          'albumLabel': "%1 de %2"
        })

        // Botones de girar

			$(document).on('click', '.rotate-cw', function() {
	            $(this).parent().cropit('rotateCW');
	        });
			$(document).on('click', '.rotate-ccw', function() {
	            $(this).parent().cropit('rotateCCW');
	        });

			$(document).on('click', '.select-image-btn', function() {
	            $(this).siblings('.cropit-image-input').click();
	        });

        //Ajax token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>

@yield('scripts')
</body>
</html>
