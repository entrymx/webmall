@extends('admin.layout')

@section('head')
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" />

    <style type="text/css">
    	.ui-select-choices {
	        position: fixed;
	        top: auto;
	        left: auto;
	        width: inherit;
      	}
    </style>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">			
			<div class="content">
				<h2>Editar Local</h2>			
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="content">
					{{Form::model($local,['method'=>'PATCH','route'=>['local.update',$local->id]])}}

					<fieldset>
						<div class="form-group">
							<div class=" {{ $errors->has('nombre') ? 'has-error' : '' }}">
								{{ Form::label('nombre', 'Nombre') }}
								{{ Form::text('nombre', old('nombre'), array('class' => 'form-control border-input')) }}													
								<span class="text-danger">{{ $errors->first('nombre') }}</span>
							</div>	
						</div>
						
						<div class="form-group">
							<div class=" {{ $errors->has('tipo') ? 'has-error' : '' }}">
								{{ Form::label('tipo', 'Tipo') }}
								<select id='tipo' name='tipo' title="Ningun tipo seleccionado" class="selectpicker form-control show-tick border-input" data-live-search="true">
									@foreach($tipos as $u)
									<option value={{$u->id}} >{{$u->nombre}}</option>
									@endforeach
								</select>
								<span class="text-danger">{{ $errors->first('tipo') }}</span>
							</div>
						</div>								

						<div class="form-group">
							<div class=" {{ $errors->has('plaza') ? 'has-error' : '' }}">
								{{ Form::label('plaza', 'Plaza') }}
								<select id='plaza' name='plaza' title="Ninguna plaza seleccionada" class="selectpicker form-control show-tick border-input" data-live-search="true">
									@foreach($plazas as $u)
									<option value={{$u->id}} >{{$u->nombre}}</option>
									@endforeach
								</select>
								<span class="text-danger">{{ $errors->first('plaza') }}</span>
							</div>
						</div>								

						<button class="btn btn-primary form-control btn-fill">Guardar</button>
						<div class="gap-1"></div>
						<br>
						<a href="{{ADMIN_ROUTE}}local" class="btn btn-danger form-control btn-fill">Cancelar</a>
					</fieldset>


					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
    <script src="{{ asset('plugins/select2/js/select2.min.js')}}"></script>

	<script type="text/javascript">

		$("#plaza, #tipo").select2({
		  //tags: true,
		  casesensitive: true,
		  placeholder: "Teclea para buscar, enter para selecionar",
		  allowClear: true
		});


		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.local').addClass('active');


			$('#plaza').select2("val",{{$local->plaza->id}});

			$('#tipo').select2("val",{{$local->tipo->id}});


		});
	</script>

@endsection