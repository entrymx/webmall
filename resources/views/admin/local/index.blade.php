@extends('admin.layout')

@section('head')
		<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-6">			
				<h2>Locales</h2>
				<a href="{{ADMIN_ROUTE}}local/create" class="btn btn-info btn-fill btn-wd ">Nuevo Local</a>
		</div>
		<div class="col-sm-6">			
			<div class="pull-right">
				<br><br><br><br>
				{{ Form::label('buscar', 'Buscar'   , array('class' => '')) }}
				{{ Form::text('buscar', old('buscar'), array('class' => ' border-input ', 'placeholder' => 'Teclea para buscar')) }}
				<a class="btn btn-info btn-fill btn-wd buscar ">Buscar</a>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="content table-responsive locales">
					
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script type="text/javascript">

		function locales(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}
			$.ajax({
				url: "{{ADMIN_ROUTE}}local/locales"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					text: $('#buscar').val()
				}
			})
			.done(function(data) {
				$('.locales').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}


        function deleteLocal(id, name) {
            swal({
                title: "Confirmar eliminación", 
                text: "¿Está seguro de que quiere eliminar el local "+name+"?", 
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,

                cancelButtonText: "No, Cancelar!",
                confirmButtonText: "Sí, ¡eliminalo!",
                confirmButtonColor: "#3f51b5"
            }, function() {
                $.ajax({
                    url: "{{ADMIN_ROUTE}}local/"+id,
                    type: "DELETE"
                })
                .done(function(data) {
                	if(data == 0){
                		swal("Error", "Ocurrió un error, intenta más tarde", "error");
                	}else{
                		swal("¡Eliminado!", "¿El local fue eliminado correctamente!", "success");
                    	
                    	setTimeout(function() {
							location.reload();
						}, 1000);
                	}
                    
                })
                .error(function(data) {
                	swal("Error", "Ocurrió un error, intenta más tarde", "error");
                });
            });
        }

        $(document).on("click",'.eliminar',(function() {
            var id   = $(this).attr("data-id");
            var name = $(this).attr("data-name");
            deleteLocal(id, name);
        }));


		$(document).on('keypress', '#buscar', function (e) {
			if(e.which == 13) {
				e.preventDefault();
				locales('');
			}
		});

		$(document).on('click', '.buscar', function (e) {
				locales('');
				e.preventDefault();
			});

		$(document).on('click', '.locales .pagination a, .locales th a', function (e) {
				locales($(this).attr('href').split('sort')[1]);
				e.preventDefault();
			});

		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.local').addClass('active');
			locales('');
		});
	</script>
@endsection
