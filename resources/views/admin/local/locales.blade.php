	

	<div class="content table-responsive locales">
		<table id="tabla_alumno" class="table table-hover">
			<thead>
				<tr>
					<th>@sortablelink('nombre','NOMBRE')</th>
					<!--th>ESTADO</th-->
					<th>@sortablelink('tipo.nombre','TIPO')</th>
					<th>@sortablelink('plaza.nombre','PLAZA')</th>
					<th>TIENDA</th>
					<th>EDITAR</th>
					<th>ELIMINAR</th>
				</tr>
			</thead>
			<tbody>
				@foreach($locals as $local)
				<tr>
					<td>
						{{$local->nombre}}
					</td>
					<!--td>
						{{$local->estado}}
					</td-->
					<td>
						{{$local->tipo->nombre}}
					</td>
					<td>
						{{$local->plaza->nombre}}
					</td>
					<td>
						@if($local->tienda->count() > 0)
							{{$local->tienda->first()->nombre}}
						@else
							Sin Tienda
						@endif
					</td>
					<td>
						<a class="btn btn-warning btn-fill" href="{{ADMIN_ROUTE}}local/{{$local->id}}/edit" >
							<span class="ti-pencil"></span>
						</a>
					</td>
					<td>
						<a class="btn btn-danger btn-fill eliminar" data-id="{{$local->id}}" data-name="{{$local->nombre}}">
							<span class="ti-close"></span>
						</a>
					</td>
				</tr> 
				@endforeach
			</tbody>
		</table> 
		{{ $locals->appends(\Request::except('page'))->render() }}
	</div>