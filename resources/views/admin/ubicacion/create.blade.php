@extends('admin.layout')

@section('breadcrums')

<a href="/ubicacion">UBICACIÓN</a>
-
<a href="">Nuevo</a>

@endsection

@section('head')

	<style>

		#map {
			height: 350px;   
		}

	</style>

@endsection

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<h2>Nueva Ubicación</h2>
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="content">
				{{Form::open(['url' => ADMIN_ROUTE . 'ubicacion'])}}

				<fieldset>							
					<div class="form-group">
						<div class=" {{ $errors->has('domicilio') ? 'has-error' : '' }}">
							{{ Form::label('domicilio', 'Domicilio') }}
							{{ Form::text('domicilio', old('domicilio'), array('class' => 'form-control border-input')) }}													
							<span class="text-danger">{{ $errors->first('domicilio') }}</span>
						</div>	
					</div>

					<div class="form-group">
						<!--div class=" {{ $errors->has('ciudad') ? 'has-error' : '' }}">
							{{ Form::label('estado', 'Estado') }}
							<select id='estado' name='estado' title="Ninguna estado seleccionada" class="selectpicker form-control show-tick border-input" data-live-search="true">
								@foreach($estados as $c)
								<option value={{$c->id}} data-subtext="{{$c->nombre}}">{{$c->nombre}}</option>
								@endforeach
							</select>
							<span class="text-danger">{{ $errors->first('estado') }}</span>
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('ciudad') ? 'has-error' : '' }}">
								{{ Form::label('ciudad', 'Ciudad') }}
								<select id='ciudad' name='ciudad' title="Ninguna ciudad seleccionada" class="selectpicker form-control show-tick border-input" data-live-search="true">
									@foreach($ciudades as $c)
									<option value={{$c->id}} data-subtext="{{$c->estado->nombre}}">{{$c->nombre}}</option>
									@endforeach
								</select>
								<span class="text-danger">{{ $errors->first('ciudad') }}</span>
							</div>
						</div-->							

						<div class="form-group">
							<div class=" {{ $errors->has('estado') ? 'has-error' : '' }}">
								{{ Form::label('estado', 'Estado') }}
								{{ Form::text('estado', old('estado'), array('class' => 'form-control border-input')) }}													
								<span class="text-danger">{{ $errors->first('estado') }}</span>
							</div>	
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('ciudad') ? 'has-error' : '' }}">
								{{ Form::label('ciudad', 'Ciudad') }}
								{{ Form::text('ciudad', old('ciudad'), array('class' => 'form-control border-input')) }}													
								<span class="text-danger">{{ $errors->first('ciudad') }}</span>
							</div>	
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('latitud') ? 'has-error' : '' }}">
								{{ Form::label('searchBox', 'Buscar...') }}
								{{ Form::text('searchBox', old('searchBox'), array('class' => 'form-control border-input')) }}													
								<span class="text-danger">{{ $errors->first('searchBox') }}</span>
							</div>	
						</div>

						<div class="form-group">
							<div id="map"></div>
						</div>

						{{ Form::hidden('latitud', old('latitud'), array('id' => 'latitud')) }}
						{{ Form::hidden('longitud', old('longitud'), array('id' => 'longitud')) }}


						<button class="btn btn-primary form-control btn-fill">Guardar</button>
						<div class="gap-1"></div>
						<br>
						<a href="{{ADMIN_ROUTE}}ubicacion" class="btn btn-danger form-control btn-fill">Cancelar</a>
					</fieldset>

					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>


@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.ubicacion').addClass('active');

			$('#latitud').val(24.0359155);
			$('#longitud').val(-104.6518712);

		});
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHZgOIitZq4ZAuC9kZxb2a3s80PYITTds&libraries=places" ></script>
	<script>
		var map = new google.maps.Map(document.getElementById('map'),{
			center:{
				lat: 24.0359155, 
				lng: -104.6518712
			},
			zoom: 17
		});

		var marker = new google.maps.Marker({
			position:{
				lat: 24.0359155,
				lng: -104.6518712
			},
			map: map,
			draggable:true			
		});

		var searchBox = new google.maps.places.SearchBox(document.getElementById('searchBox'));

		google.maps.event.addListener(searchBox,'places_changed',function(){

			var places = searchBox.getPlaces();
			var bounds = new google.maps.LatLngBounds();
			var i, place;

			for(i=0; place=places[i];i++){
				bounds.extend(place.geometry.location);
				marker.setPosition(place.geometry.location);
			}

			map.fitBounds(bounds);
			map.setZoom(15);

		})

		var geocoder = new google.maps.Geocoder;

		google.maps.event.addListener(marker,'position_changed',function(){

			var lat = marker.getPosition().lat();
			var lng = marker.getPosition().lng();

			var latlng = {lat:lat, lng:lng};
			
			geocoder.geocode({'location': latlng}, function(results, status) {

				var estado    = "";
				var municipio = "";
				var domicilio = "";
				if (status === 'OK') {
					if (results[1]) {
						
						domicilio = results[1].formatted_address;
						$(results[1]['address_components']).each(function(){
							var $super = this;
							//console.log(this.long_name);
							$(this['types']).each(function(){
								if(this == "administrative_area_level_1"){
									console.log('administrative_area_level_1: '+$super.long_name);
									estado += " "+ $super.long_name;
								}
							});
							$(this['types']).each(function(){
								if(this == "administrative_area_level_2"){
									console.log('administrative_area_level_2: '+$super.long_name);
									municipio += " "+ $super.long_name;
								}
							});

							$(this['types']).each(function(){
								if(this == "administrative_area_level_3"){
									console.log('administrative_area_level_3: '+$super.long_name);

									municipio += " "+ $super.long_name;
								}
							});

							$(this['types']).each(function(){
								if(this == "locality"){
									console.log('locality: '+$super.long_name);

									estado += " "+ $super.long_name;
								}
							});


							$(this['types']).each(function(){
								if(this == "sublocality"){
									console.log('sublocality: '+$super.long_name);

									municipio += " "+ $super.long_name;
								}
							});

						});
						

					$('#estado').val(estado);
					$('#ciudad').val(municipio);
					$('#domicilio').val(domicilio);

	
					} else {
					console.log('No results found');
					}
				} else {
					console.log('Geocoder failed due to: ' + status);
				}
			});

			$('#latitud').val(lat);
			$('#longitud').val(lng);

		})


	</script>



@endsection