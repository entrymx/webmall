@extends('admin.layout')

@section('breadcrums')

<a href="/ubicacion">UBICACIÓN</a>

@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">			
		<div class="content">
			<h2>Ubicaciones</h2>
			<a href="{{ADMIN_ROUTE}}ubicacion/create" class="btn btn-info btn-fill btn-wd">Nueva Ubicación</a>					
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="content">
				<table id="tabla_alumno" class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>DOMICILIO</th>
							<th>CIUDAD</th>
							<th>EDITAR</th>
							<th>ELIMINAR</th>
						</tr>
					</thead>
					<tbody>
						@foreach($ubicaciones as $ubicacion)
						<tr>
							<th scope="row">{{$loop->iteration}}</th>
							<td>
								{{$ubicacion->domicilio}}
							</td>
							<td>
								{{$ubicacion->ciudad->nombre.", ".$ubicacion->ciudad->estado->nombre}}
							</td>
							<td>
								<a class="btn btn-warning btn-fill" href="{{ADMIN_ROUTE}}ubicacion/{{$ubicacion->id}}/edit" >
									<span class="ti-pencil"></span>
								</a>
							</td>
							<td>
								{{Form::model($ubicacion,['method'=>'DELETE','route'=>['ubicacion.destroy',$ubicacion->id]])}}
								<button class="btn btn-danger btn-fill" >
									<span class="ti-close"></span>
								</button>
								{{Form::close()}}
							</td>
						</tr> 
						@endforeach


					</tbody>
				</table> 
			</div>
		</div>
	</div>
</div>


@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.nav li').removeClass('active');
		$('.ubicacion').addClass('active');

	});
</script>
@endsection	