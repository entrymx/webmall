	@extends('admin.layout')

@section('breadcrums')

<a href="/ubicacion">UBICACIÓN</a>
-
<a href="">{{$ubicacion->domicilio}}</a>

@endsection

@section('head')

<style>

	#map {
		height: 350px;   
	}

</style>

@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">			
			<div class="content">
				<h2>Editar Ubicación</h2>			
			</div>
		</div>
	</div>
	<br>
	<div class="row">    
		<div class="col-sm-12">
			<div class="card">
				<div class="content">

					{{Form::model($ubicacion,['method'=>'PATCH','route'=>['ubicacion.update',$ubicacion->id]])}}

					<fieldset>							
						<div class="form-group">
							<div class=" {{ $errors->has('domicilio') ? 'has-error' : '' }}">
								{{ Form::label('domicilio', 'Domicilio') }}
								{{ Form::text('domicilio', old('domicilio'), array('class' => 'form-control border-input')) }}													
								<span class="text-danger">{{ $errors->first('domicilio') }}</span>
							</div>	
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('ciudad') ? 'has-error' : '' }}">
								{{ Form::label('ciudad', 'Ciudad') }}
								<select id='ciudad' name='ciudad' title="Ninguna ciudad seleccionada" class="selectpicker form-control show-tick border-input" data-live-search="true">
									@foreach($ciudades as $c)
									<option value={{$c->id}} data-subtext="{{$c->estado->nombre}}">{{$c->nombre}} {{$c->id}}</option>
									@endforeach
								</select>
								<span class="text-danger">{{ $errors->first('ciudad') }}</span>
							</div>
						</div>							

						<div class="form-group">
							<div class=" {{ $errors->has('latitud') ? 'has-error' : '' }}">
								{{ Form::label('searchBox', 'Buscar...') }}
								{{ Form::text('searchBox', old('searchBox'), array('class' => 'form-control border-input')) }}													
								<span class="text-danger">{{ $errors->first('searchBox') }}</span>
							</div>	
						</div>

						<div class="form-group">
							<div id="map"></div>
						</div>

						{{ Form::hidden('latitud', old('latitud'), array('id' => 'latitud')) }}													
						{{ Form::hidden('longitud', old('longitud'), array('id' => 'longitud')) }}											


						<button class="btn btn-primary form-control btn-fill">Guardar</button>
						<div class="gap-1"></div>
						<br>
						<a href="{{ADMIN_ROUTE}}ubicacion" class="btn btn-danger form-control btn-fill">Cancelar</a>
					</fieldset>

					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.ubicacion').addClass('active');

		});
	</script>

	<script type="text/javascript">
		
		//$('#ciudad').selectpicker('render');		
		$('#ciudad').val({{$ubicacion->ciudad->id}});
		//$('#ciudad').selectpicker('render');


	</script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHZgOIitZq4ZAuC9kZxb2a3s80PYITTds&libraries=places" type="text/javascript"></script>
	<script>
		var map = new google.maps.Map(document.getElementById('map'),{
			center:{
				lat: {{$ubicacion->latitud}}, 
				lng: {{$ubicacion->longitud}}
			},
			zoom: 17
		});

		var marker = new google.maps.Marker({
			position:{
				lat: {{$ubicacion->latitud}}, 
				lng: {{$ubicacion->longitud}}
			},
			map: map,
			draggable:true			
		});

		var searchBox = new google.maps.places.SearchBox(document.getElementById('searchBox'));

		google.maps.event.addListener(searchBox,'places_changed',function(){

			var places = searchBox.getPlaces();
			var bounds = new google.maps.LatLngBounds();
			var i, place;

			for(i=0; place=places[i];i++){
				bounds.extend(place.geometry.location);
				marker.setPosition(place.geometry.location);
			}

			map.fitBounds(bounds);
			map.setZoom(15);

		})

		google.maps.event.addListener(marker,'position_changed',function(){

			var lat = marker.getPosition().lat();
			var lng = marker.getPosition().lng();

			$('#latitud').val(lat);
			$('#longitud').val(lng);


		})
	</script>
@endsection