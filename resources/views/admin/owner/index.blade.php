@extends('admin.layout')

@section('head')
		<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-6">			
				<h2>Dueño</h2>
				<a href="{{ADMIN_ROUTE}}owner/create" class="btn btn-info btn-fill btn-wd ">Nuevo Dueño</a>
		</div>
		<!--div class="col-sm-6">			
			<div class="pull-right">
				<br><br><br><br>
				{{ Form::label('buscar', 'Buscar'   , array('class' => '')) }}
				{{ Form::text('buscar', old('buscar'), array('class' => ' border-input ', 'placeholder' => 'Teclea para buscar')) }}
				<a class="btn btn-info btn-fill btn-wd buscar ">Buscar</a>
			</div>
		</div-->
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="content table-responsive owners">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>NOMBRE</th>
								<th>APELLIDOS</th>
								<th>CORREO</th>
								<th>TIENDA</th>
								<th>EDITAR</th>
								<th>ELIMINAR</th>
							</tr>
						</thead>
						<tbody>
							@foreach($owners as $owner)
							<tr>
								<td>
									{{$owner->first_name}}
								</td>
								<td>
									{{$owner->last_name}}
								</td>
								<td>
									{{$owner->email}}
								</td>
								<td><p>
									@foreach($owner->tienda as $t)
										<a href="{{ADMIN_ROUTE.'tienda/'.$t->id}}">{{$t->nombre}}</a><br>
									@endforeach
									</p>
								</td>
								<td>
									<a class="btn btn-warning btn-fill" href="{{ADMIN_ROUTE}}owner/{{$owner->id}}/edit" >
										<span class="ti-pencil"></span>
									</a>
								</td>
								<td>
									{{Form::model($owner,['method'=>'DELETE','route'=>['owner.destroy',$owner->id]])}}
									<button class="btn btn-danger btn-fill" >
										<span class="ti-close"></span>
									</button>
									{{Form::close()}}
								</td>
							</tr> 
							@endforeach
						</tbody>
					</table> 
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script type="text/javascript">

		function owners(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}
			$.ajax({
				url: "{{ADMIN_ROUTE}}owner/owners"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					text: $('#buscar').val()
				}
			})
			.done(function(data) {
				$('.owners').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}

		$(document).on('keypress', '#buscar', function (e) {
			if(e.which == 13) {
				e.preventDefault();
				owners('');
			}
		});

		$(document).on('click', '.buscar', function (e) {
				owners('');
				e.preventDefault();
			});

		$(document).on('click', '.owners .pagination a, .owners th a', function (e) {
				owners($(this).attr('href').split('sort')[1]);
				e.preventDefault();
			});

		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.owner').addClass('active');
			//owners('');
		});
	</script>
@endsection
