	

	<div class="content table-responsive owners">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>@sortablelink('first_name','NOMBRE')</th>
					<th>@sortablelink('last_name','APELLIDOS')</th>
					<th>@sortablelink('email','CORREO')</th>
					<th>TIENDA</th>
					<th>EDITAR</th>
					<th>ELIMINAR</th>
				</tr>
			</thead>
			<tbody>
				@foreach($owners as $owner)
				<tr>
					<td>
						{{$owner->first_name}}
					</td>
					<td>
						{{$owner->last_name}}
					</td>
					<td>
						{{$owner->email}}
					</td>
					<td><p>
						@foreach($owner->tienda as $t)
							<a href="{{ADMIN_ROUTE.'tienda/'.$t->id}}">{{$t->nombre}}</a><br>
						@endforeach
						</p>
					</td>
					<td>
						<a class="btn btn-warning btn-fill" href="{{ADMIN_ROUTE}}owner/{{$owner->id}}/edit" >
							<span class="ti-pencil"></span>
						</a>
					</td>
					<td>
						{{Form::model($owner,['method'=>'DELETE','route'=>['local.destroy',$owner->id]])}}
						<button class="btn btn-danger btn-fill" >
							<span class="ti-close"></span>
						</button>
						{{Form::close()}}
					</td>
				</tr> 
				@endforeach
			</tbody>
		</table> 
		{{ $owners->appends(\Request::except('page'))->render() }}
	</div>