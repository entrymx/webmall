@extends('admin.layout')

@section('head')
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" />

    <style type="text/css">
    	.ui-select-choices {
	        position: fixed;
	        top: auto;
	        left: auto;
	        width: inherit;
      	}
    </style>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">			
			<div class="content">
				<h2>Editar Dueño</h2>			
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="content">
					{{Form::model($owner,['method'=>'PATCH','url' => ADMIN_ROUTE . 'owner/'.$owner->id])}} 
					<fieldset>
						<div class="form-group">
							<div class=" {{ $errors->has('first_name') ? 'has-error' : '' }}">
								{{ Form::label('first_name', 'Nombre') }}
								{{ Form::text('first_name', old('first_name'), array('class' => 'form-control border-input')) }}													
								<span class="text-danger">{{ $errors->first('first_name') }}</span>
							</div>	
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('last_name') ? 'has-error' : '' }}">
								{{ Form::label('last_name', 'Apellido') }}
								{{ Form::text('last_name', old('last_name'), array('class' => 'form-control border-input')) }}													
								<span class="text-danger">{{ $errors->first('last_name') }}</span>
							</div>	
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('email') ? 'has-error' : '' }}">
								{{ Form::label('email', 'Correo') }}
								{{ Form::text('email', old('email'), array('class' => 'form-control border-input')) }}													
								<span class="text-danger">{{ $errors->first('email') }}</span>
							</div>	
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('tienda') ? 'has-error' : '' }}">
								{{ Form::label('tienda', 'Tienda') }}
								<select id='tienda' name='tienda[]' multiple title="Ninguna tienda seleccionado" class="selectpicker form-control show-tick border-input" data-live-search="true">
									@foreach($tiendas as $u)
									<option value={{$u->id}} >{{$u->nombre}}</option>
									@endforeach
								</select>
								<span class="text-danger">{{ $errors->first('tienda') }}</span>
								
							</div>
						</div>														

						<button class="btn btn-primary form-control btn-fill">Guardar</button>
						<div class="gap-1"></div>
						<br>
						<a href="{{ADMIN_ROUTE}}owner" class="btn btn-danger form-control btn-fill">Cancelar</a>
					</fieldset>


					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
    <script src="{{ asset('plugins/select2/js/select2.min.js')}}"></script>

	<script type="text/javascript">

		$("#tienda").select2({
		  //tags: true,
		  casesensitive: true,
		  placeholder: "Teclea para buscar, enter para selecionar",
		  allowClear: true
		});


		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.owner').addClass('active');


			$('#tienda').select2("val",{!!$owner->tienda->pluck('id')!!});


		});
	</script>

@endsection