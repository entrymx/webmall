@extends('admin.layout')

@section('head')
		<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('content')
<div class="row">
	<div class="col-sm-6">			
		<div class="content">
			<h2>Tags</h2>
			<a href="{{ADMIN_ROUTE}}tag/create" class="btn btn-info btn-fill btn-wd">Nueva Tag</a>					
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="content">
				<table id="tabla_alumno" class="table table-hover">
					<thead>
						<tr>
							<th>@sortablelink('nombre','NOMBRE')</th>
							<th>EDITAR</th>
							<th>ELIMINAR</th>
						</tr>
					</thead>
					<tbody>
						@foreach($tags as $tag)
						<tr>
							<td>
								<a href="{{ADMIN_ROUTE}}tag/{{$tag->id}}">{{$tag->nombre}}</a>
							</td>

							<td>
								<a class="btn btn-warning btn-fill" href="{{ADMIN_ROUTE}}tag/{{$tag->id}}/edit" >
									<span class="ti-pencil"></span>
								</a>
							</td>
							<td>
								<a class="btn btn-danger btn-fill eliminar" data-id="{{$tag->id}}" data-name="{{$tag->nombre}}" >
									<span class="ti-close"></span>
								</a>
							</td>
						</tr> 
						@endforeach


					</tbody>
				</table> 
				{{ $tags->appends(\Request::except('page'))->render() }}
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
	<script type="text/javascript">

        function deleteTag(id, name) {
            swal({
                title: "Confirmar eliminación", 
                text: "¿Está seguro de que quiere eliminar el tipo de local "+name+"?", 
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,

                cancelButtonText: "No, Cancelar!",
                confirmButtonText: "Sí, ¡eliminalo!",
                confirmButtonColor: "#3f51b5"
            }, function() {
                $.ajax({
                    url: "{{ADMIN_ROUTE}}tag/"+id,
                    type: "DELETE"
                })
                .done(function(data) {
                	if(data == 0){
	                    swal("Error", "Primero elimina o cambia las tiendas que tienen este tag", "error");
                	}else{
                		swal("¡Eliminado!", "¡el Tag fue eliminado correctamente!", "success");
                    	setTimeout(function() {
							location.reload();
						}, 1000);
                	}
                    
                })
                .error(function(data) {
                });
            });
        }

        $(document).on("click",'.eliminar',(function() {
            var id   = $(this).attr("data-id");
            var name = $(this).attr("data-name");
            deleteTag(id, name);
        }));

		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.tag').addClass('active');

		});
</script>
@endsection