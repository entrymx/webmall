@extends('admin.layout')

@section('breadcrums')

<a href="">TAG</a>
-
<a href="">{{$tag->nombre}}</a>

@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">			
		<div class="content">
			<h2>Editar Tag</h2>			
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="content">
				{{Form::model($tag,['method'=>'PATCH','route'=>['tag.update',$tag->id]])}}

				<fieldset>							
					<div class="form-group">
						<div class=" {{ $errors->has('nombre') ? 'has-error' : '' }}">
							{{ Form::label('nombre', 'Nombre') }}
							{{ Form::text('nombre', old('nombre'), array('class' => 'form-control')) }}													
							<span class="text-danger">{{ $errors->first('nombre') }}</span>
						</div>	
					</div>

					<button class="btn btn-primary form-control btn-fill">Guardar</button>
					<div class="gap-1"></div>
					<br>
					<a href="{{ADMIN_ROUTE}}tag" class="btn btn-danger form-control btn-fill">Cancelar</a>
				</fieldset>

				{{Form::close()}}
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.nav li').removeClass('active');
		$('.tag').addClass('active');

	});
</script>
@endsection