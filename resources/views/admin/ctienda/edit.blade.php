@extends('admin.layout')

@section('head')
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" />

    <style type="text/css">
    	
    	.ui-select-choices {
	        position: fixed;
	        top: auto;
	        left: auto;
	        width: inherit;
      	}

    </style>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">			
			<div class="content">
				<h2>Editar Contacto de Tienda</h2>			
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="content">
					{{Form::model($ctienda,['method'=>'PATCH','route'=>['ctienda.update',$ctienda->id]])}}

						<fieldset>							
							<div class="form-group">
								<div class=" {{ $errors->has('nombres') ? 'has-error' : '' }}">
									{{ Form::label('nombres', 'Nombres') }}
									{{ Form::text('nombres', old('nombres'), array('class' => 'form-control border-input')) }}													
									<span class="text-danger">{{ $errors->first('nombres') }}</span>
								</div>	
							</div>
							<div class="form-group">
								<div class=" {{ $errors->has('apellidos') ? 'has-error' : '' }}">
									{{ Form::label('apellidos', 'Apellidos') }}
									{{ Form::text('apellidos', old('apellidos'), array('class' => 'form-control border-input')) }}													
									<span class="text-danger">{{ $errors->first('apellidos') }}</span>
								</div>	
							</div>
							<div class="form-group">
								<div class=" {{ $errors->has('correo') ? 'has-error' : '' }}">
									{{ Form::label('correo', 'Correo') }}
									{{ Form::text('correo', old('correo'), array('class' => 'form-control border-input')) }}													
									<span class="text-danger">{{ $errors->first('correo') }}</span>
								</div>	
							</div>
							<div class="form-group">
								<div class=" {{ $errors->has('telefono') ? 'has-error' : '' }}">
									{{ Form::label('telefono', 'Teléfono') }}
									{{ Form::text('telefono', old('telefono'), array('class' => 'form-control border-input')) }}													
									<span class="text-danger">{{ $errors->first('telefono') }}</span>
								</div>	
							</div>

							<div class="form-group">
								<div class=" {{ $errors->has('tienda') ? 'has-error' : '' }}">
									{{ Form::label('tienda', 'Tienda') }}
									<select id='tienda' name='tienda' title="Ninguna ubicación seleccionada" class="selectpicker form-control show-tick border-input" data-live-search="true">
										@foreach($tiendas as $p)
										<option value={{$p->id}} data-subtext="" >{{$p->nombre}}</option>
										@endforeach
									</select>
									<span class="text-danger">{{ $errors->first('tienda') }}</span>
								</div>
							</div>								

									


							<button class="btn btn-info btn-fill form-control">Guardar</button>
							<div class="gap-1"></div>
							<br>
							<a href="{{ADMIN_ROUTE}}ctienda" class="btn btn-danger btn-fill form-control">Cancelar</a>
						</fieldset>

					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
    <script src="{{ asset('plugins/select2/js/select2.min.js')}}"></script>

	<script type="text/javascript">
		$("#tienda").select2({
		  //tags: true,
		  casesensitive: true,
		  placeholder: "Teclea para buscar, enter para selecionar",
		  allowClear: true
		});
		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.ctienda').addClass('active');

			$("#tienda").select2('val',{{$ctienda->tienda_id}});
		});
	</script>
@endsection	