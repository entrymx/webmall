@extends('admin.layout')

@section('head')
		<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12">			
		<div class="content">
			<h2>Contactos de Tiendas</h2>					
			<a href="{{ADMIN_ROUTE}}ctienda/create" class="btn btn-info btn-fill btn-wd">Nuevo Ctienda</a>					
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="content">                	
				<div class="row">
					<div class="body table-responsive all">
						<table id="tabla_alumno" class="table table-hover">
							<thead>
								<tr>
									<th>@sortablelink('nombres','NOMBRES')</th>
									<th>@sortablelink('apellidos','APELLIDOS')</th>
									<th>@sortablelink('correo','CORREO')</th>
									<th>@sortablelink('telefono','TELÉFONO')</th>
									<th>@sortablelink('tienda.nombre','TIENDA')</th>
									<th>EDITAR</th>
									<th>ELIMINAR</th>
								</tr>
							</thead>
							<tbody>
								@foreach($ctiendas as $ctienda)
								<tr>
									<td>
										{{$ctienda->nombres}}
									</td>

									<td>
										{{$ctienda->apellidos}}
									</td>

									<td>
										{{$ctienda->correo}}
									</td>

									<td>
										{{$ctienda->telefono}}
									</td>

									<td>
										@if($ctienda->tienda)
										{{$ctienda->tienda->nombre}}
										@else
										Sin Tienda
										@endif
									</td>

									<td>
										<a class="btn btn-warning btn-fill" href="{{ADMIN_ROUTE}}ctienda/{{$ctienda->id}}/edit" >
											<span class="ti-pencil"></span>
										</a>
									</td>
									<td>
										{{Form::model($ctienda,['method'=>'DELETE','route'=>['ctienda.destroy',$ctienda->id]])}}
										<button class="btn btn-danger btn-fill" >
											<span class="ti-close"></span>
										</button>
										{{Form::close()}}
									</td>
								</tr> 
								@endforeach
							</tbody>
						</table> 
						{{ $ctiendas->appends(\Request::except('page'))->render() }}
					</div>
				</div>
			</div>
			<div class="card-footer">
			</div> 
		</div>
	</div>
</div>


@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.nav li').removeClass('active');
		$('.ctienda').addClass('active');

	});
</script>
@endsection	