@extends('admin.layout')

@section('head')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <style type="text/css">
    	.ui-select-choices {
	        position: fixed;
	        top: auto;
	        left: auto;
	        width: inherit;
      	}
    </style>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">			
			<div class="content">
				<h2>Nueva Tienda</h2>			
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">	
			<div class="card">
				<div class="content">
					{{Form::open(['url' => ADMIN_ROUTE . 'tienda', 'files' => true])}}

					<fieldset>


						<div class="form-group">
							<div class=" {{ $errors->has('nombre') ? 'has-error' : '' }}">
								{{ Form::label('nombre', 'Nombre') }}
								{{ Form::text('nombre', old('nombre'), array('class' => 'form-control border-input')) }}													
								<span class="text-danger">{{ $errors->first('nombre') }}</span>
							</div>	
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('plaza') ? 'has-error' : '' }}">
								{{ Form::label('plaza', 'Plaza') }}
								<select id='plaza' name='plaza' title="Ningún plaza seleccionada" class="tags-select form-control show-tick border-input plazas" data-live-search="true">
									@foreach($plazas as $u)
									<option value={{$u->id}} >{{$u->nombre}}</option>
									@endforeach
								</select>
								<span class="text-danger">{{ $errors->first('plaza') }}</span>
							</div>
						</div>	

						<div class="form-group">
							<div class=" {{ $errors->has('local') ? 'has-error' : '' }}">
								{{ Form::label('local', 'Local') }}
								<select id='local' name='local' title="Ningún local seleccionada" class="tags-select form-control show-tick border-input locales" data-live-search="true">
									
								</select>
								<span class="text-danger">{{ $errors->first('local') }}</span>
							</div>
						</div>	

						<div class="form-group">
							<div class=" {{ $errors->has('apertura') ? 'has-error' : '' }}">
								{{ Form::label('apertura', 'Apertura') }}
								{{ Form::text('apertura', old('apertura'), array('id'=>'apertura' ,'class' => 'form-control border-input datepicker', 'placeholder' => '24Hrs Ejemplo: 06:00 - 23:00')) }}													
								<span class="text-danger">{{ $errors->first('apertura') }}</span>
							</div>	
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('horario') ? 'has-error' : '' }}">
								{{ Form::label('cierre', 'Cierre') }}
								{{ Form::text('cierre', old('cierre'), array('id'=>'cierre' ,'class' => 'form-control border-input datepicker', 'placeholder' => '24Hrs Ejemplo: 06:00 - 23:00')) }}													
								<span class="text-danger">{{ $errors->first('horario') }}</span>
							</div>	
						</div>
						
						<div class="form-group">
							<div class=" {{ $errors->has('tags') ? 'has-error' : '' }}">
								{{ Form::label('tags', 'Tags') }}<br>
								<select id='tags' name='tags[]' class="tags-select form-control border-input" multiple="multiple" >
								@foreach($tags as $t)
									<option value="{{$t->text}}" >{{$t->text}}</option>
								@endforeach
								</select>
								<span class="text-danger">{{ $errors->first('tags') }}</span>
							</div>	
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('categoria') ? 'has-error' : '' }}">
								{{ Form::label('categoria', 'Categoria') }}
								<select id='categoria' name='categoria' title="Ningún categoria seleccionado" class="tags-select form-control show-tick border-input">
									@foreach($categorias as $u)
									<option value='{{$u->nombre}}' >{{$u->nombre}}</option>
									@endforeach
								</select>
								<span class="text-danger">{{ $errors->first('categoria') }}</span>
							</div>
						</div>	


						<strong><p>Imágenes</p></strong>
						
						<div class="form-group">
							<div class=" {{ $errors->has('logo') ? 'has-error' : '' }}">
								{{ Form::label('logo', 'Logo') }}
								<span class="text-danger">{{ $errors->first('logo') }}</span>
							</div>	
							<div id="logo-cropper" class="image-cropper">
								<div class="cropit-preview"></div>
									
								<input type="file" name="logo" id="logo" class="cropit-image-input">

								<a class="btn btn-primary btn-fill select-image-btn">Seleccionar imagen</a>
								<a class="btn rotate-ccw"> <span class="ti-angle-left"> </a>
								<a class="btn rotate-cw"> <span class="ti-angle-right"> </span> </a>

								<div class="slider-wrapper">
									<h3> - 
									<input type="range" class="cropit-image-zoom-input custom"  min="0" max="1" step="0.01">
									 + </h3>
								</div>

								<input type="hidden" name="logo-img" id="logo-img">
							</div>
						</div>

						<div class="form-group fotos">
							<div class=" {{ $errors->has('foto') ? 'has-error' : '' }}">
								{{ Form::label('foto', 'Fotos') }}
								<span class="text-danger">{{ $errors->first('foto') }}</span>
							</div>	
							<div id="foto-cropper-1" class="image-cropper">
								<div class="cropit-preview"></div>
									
								<input type="file" name="foto-1'" id="foto-1" multiple class="cropit-image-input">			

								<a class="btn btn-primary btn-fill select-image-btn">Seleccionar imagen</a>
								<a class="btn rotate-ccw"> <span class="ti-angle-left"> </a>
								<a class="btn rotate-cw"> <span class="ti-angle-right"> </span> </a>

								<div class="slider-wrapper">
									<h3> - 
									<input type="range" class="cropit-image-zoom-input custom" min="0" max="1" step="0.01">
									 + </h3>
								</div>

								<input type="hidden" name="foto-1-img" id="foto-1-img">
							</div>

						</div>

						<a class="btn btn-primary btn-fill otraFoto">Agregar otra foto</a>
						<br><br>
						<input type="hidden" name="foto-id" id="foto-id">						

						<button class="btn btn-primary form-control btn-fill">Guardar</button>
						<div class="gap-1"></div>
						<br>
						<a href="{{ADMIN_ROUTE}}tienda" class="btn btn-danger form-control btn-fill">Cancelar</a>
					</fieldset>

					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')

    <script src="{{ asset('plugins/select2/js/select2.min.js')}}"></script>
										
    <!-- Moment Plugin Js -->
    <script src="{{ asset('plugins/momentjs/moment.js')}}"></script>
    <script src="{{ asset('plugins/momentjs/es.js')}}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

	<script type="text/javascript">

        function locales(id) {
            $.ajax({
                url: "{{ADMIN_ROUTE}}tienda/locales",
                type: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    id: id
                }
            })
            .done(function(data) {
                $('.locales').html(data);

                $("#local").select2( "val",[]);
            })
            .error(function(data) {
                    alert("Error, Ocurrió un problema, error");
                });
        }

        $('.plazas').on('change',function(e){
            locales($('.plazas').val());
        });


		$(document).ready(function(){
			//$('.nav li').removeClass('active');
			$('.tienda').addClass('active');
			
	        $('#apertura, #cierre ').bootstrapMaterialDatePicker({
	            format: 'HH:mm',
	            clearButton: true,
	            weekStart: 1,
	            lang : 'es',
	            date: false,
	            currentDate: "00:00"
	        });

		});
	</script>

	<script type="text/javascript">
		$("#tags").select2({
		  tags: true,
		  casesensitive: true,
		  placeholder: "Teclea para buscar, enter para selecionar",
		  allowClear: true
		});

		$("#categoria").select2({
		  tags: true,
		  casesensitive: true,
		  placeholder: "Teclea para buscar, enter para selecionar",
		  allowClear: true
		});

		$("#plaza, #local, #contacto").select2({
		  //tags: true,
		 	"language": {
				"noResults": function(){
				return "Sin resultados";
				}
			},

			//containerCssClass: 'form-control',
			width: 'resolve',
			
			casesensitive: true,
		  	placeholder: "Teclea para buscar, enter para selecionar",
		  	allowClear: true
		});
		
		$("#plaza").select2( "val",['']);


			var fotoId = 1; 

            //Inicializa Logo
            $('#logo-cropper').cropit({
                
                allowDragNDrop: true,
                imageBackground: true,
                imageBackgroundBorderWidth: 15, // Width of background border
                freeMoveboolean: true,
                minZoom: 'fit',
                smallImagestring: 'stretch',
                onImageError: function(error) {
                    if(error.code){
                        alert('La imagen es muy pequeña \n'+
                               'Debe ser de {{logoTiendaW}} X {{logoTiendaH}}');
                    }
                },
                width: {{logoTiendaW}},
                height: {{logoTiendaH}}
			});
            
            //Inicializa foto
            $('#foto-cropper-1').cropit({
                
                allowDragNDrop: true,
                imageBackground: true,
                imageBackgroundBorderWidth: 15, // Width of background border
                freeMoveboolean: true,
                minZoom: 'fit',
                smallImagestring: 'stretch',
                onImageError: function(error) {
                    if(error.code){
                        alert('La imagen es muy pequeña \n'+
                               'Debe ser de {{fotoTiendaW}} X {{fotoTiendaH}}');
                    }
                },
                width: {{fotoTiendaW}},
                height: {{fotoTiendaH}}
			});

	    	////////////////

			$('.otraFoto').click(function(){
				otraFoto();
			});
			
			//Agrega un nuevo selector de foto
			function otraFoto() {
				fotoId = fotoId + 1;

				var code =	'<hr>'+
							'<div id="foto-cropper-'+fotoId+'" class="image-cropper">'+
								'<div class="cropit-preview"></div>'+
								'<input type="file" name="foto-'+fotoId+'" id="foto-'+fotoId+'" multiple class="cropit-image-input">'+
								'<a class="btn btn-primary btn-fill select-image-btn">Seleccionar imagen</a>'+
								'<a class="btn rotate-ccw"> <span class="ti-angle-left"> </a>'+
								'<a class="btn rotate-cw"> <span class="ti-angle-right"> </span> </a>'+
								'<div class="slider-wrapper">'+
									'<h3> - <input type="range" class="cropit-image-zoom-input custom" min="0" max="1" step="0.01"> + </h3>'+
								'</div>'+
								'<input type="hidden" name="foto-'+fotoId+'-img" id="foto-'+fotoId+'-img">'+
							'</div>';

				$('.fotos').append(code)

	            $('#foto-cropper-'+fotoId).cropit({
	                
	                allowDragNDrop: true,
	                imageBackground: true,
	                imageBackgroundBorderWidth: 15, // Width of background border
	                freeMoveboolean: true,
	                minZoom: 'fit',
	                smallImagestring: 'stretch',
	                onImageError: function(error) {
                    if(error.code){
                        alert('La imagen es muy pequeña \n'+
                               'Debe ser de {{fotoTiendaW}} X {{fotoTiendaH}}');
		                }
		            },
		            width: {{fotoTiendaW}},
		            height: {{fotoTiendaH}}
				});
			}

			$('form').submit(function() {
				var imagenLogo = $('#logo-cropper').cropit('export');
				$('#logo-img').val(imagenLogo);
				
				for (i = 1; i <= fotoId; i++) { 
					$('#foto-'+i+'-img').val( $('#foto-cropper-'+i).cropit('export') );
				}

				$('#foto-id').val(fotoId);
				
				var formValue = $(this).serialize();
				$('#result-data').text(formValue);

				return true;

			})




	</script>


@endsection