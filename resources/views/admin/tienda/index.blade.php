@extends('admin.layout')

@section('head')
		<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-6">			
			<div class="content">
				<h2>Tiendas</h2>
				<a href="{{ADMIN_ROUTE}}tienda/create" class="btn btn-info btn-fill btn-wd">Nueva Tienda</a>					
			</div>
		</div>
		<div class="col-sm-6">			
			<div class="pull-right">
				<br><br><br><br>
				{{ Form::label('buscar', 'Buscar'   , array('class' => '')) }}
				{{ Form::text('buscar', old('buscar'), array('class' => ' border-input ', 'placeholder' => 'Teclea para buscar')) }}
				<a class="btn btn-info btn-fill btn-wd buscar ">Buscar</a>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="content table-responsive tiendas">
					
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">

        function deleteTienda(id, name) {
            swal({
                title: "Confirmar eliminación", 
                text: "¿Está seguro de que quiere eliminar la tienda "+name+"?", 
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,

                cancelButtonText: "No, Cancelar!",
                confirmButtonText: "Sí, ¡eliminalo!",
                confirmButtonColor: "#3f51b5"
            }, function() {
                $.ajax({
                    url: "{{ADMIN_ROUTE}}tienda/"+id,
                    type: "DELETE"
                })
                .done(function(data) {
                	if(data == 0){
                		swal("Error", "Ocurrió un error, intenta más tarde", "error");
                	}else{
                		swal("¡Eliminado!", "¡La Tienda fue eliminada correctamente!", "success");
                    	tiendas('');/*
                    	setTimeout(function() {
							location.reload();
						}, 1000);*/
                	}
                    
                })
                .error(function(data) {
                	swal("Error", "Ocurrió un error, intenta más tarde", "error");
                });
            });
        }

		function tiendas(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}
			$.ajax({
				url: "{{ADMIN_ROUTE}}tienda/tiendas"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					text: $('#buscar').val()
				}
			})
			.done(function(data) {
				$('.tiendas').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}

		$(document).on('click', '.buscar', function (e) {
			tiendas('');
			e.preventDefault();
		});

		$(document).on('keypress', '#buscar', function (e) {
			if(e.which == 13) {
				e.preventDefault();
				tiendas('');
			}
		});

		$( ".buscar" ).keypress(function() {
			console.log( "Handler for .keypress() called." );
		});

		$(document).on('click', '.tiendas .pagination a, .tiendas th a', function (e) {
			tiendas($(this).attr('href').split('sort')[1]);
			e.preventDefault();
		});


        $(document).on("click",'.eliminar',(function() {
            var id   = $(this).attr("data-id");
            var name = $(this).attr("data-name");
            deleteTienda(id, name);
        }));





		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.tienda').addClass('active');
			tiendas('');
		});
	</script>
@endsection