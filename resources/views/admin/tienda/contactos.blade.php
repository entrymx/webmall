	<div class="body table-responsive contactos">
		<table id="tabla_alumno" class="table table-hover">
			<thead>
				<tr>
					<th>@sortablelink('nombres','NOMBRES')</th>
					<th>@sortablelink('apellidos','APELLIDOS')</th>
					<th>@sortablelink('correo','CORREO')</th>
					<th>@sortablelink('telefono','TELÉFONO')</th>
					<th>@sortablelink('tienda.nombre','TIENDA')</th>
					<th>EDITAR</th>
					<th>ELIMINAR</th>
				</tr>
			</thead>
			<tbody>
				@foreach($ctiendas as $ctienda)
				<tr>
					<td>
						{{$ctienda->nombres}}
					</td>

					<td>
						{{$ctienda->apellidos}}
					</td>

					<td>
						{{$ctienda->correo}}
					</td>

					<td>
						{{$ctienda->telefono}}
					</td>

					<td>
						@if($ctienda->tienda)
						{{$ctienda->tienda->nombre}}
						@else
						Sin Tienda
						@endif
					</td>

					<td>
						<a class="btn btn-warning btn-fill" href="{{ADMIN_ROUTE}}ctienda/{{$ctienda->id}}/edit" >
							<span class="ti-pencil"></span>
						</a>
					</td>
					<td>
						{{Form::model($ctienda,['method'=>'DELETE','route'=>['ctienda.destroy',$ctienda->id]])}}
						<button class="btn btn-danger btn-fill" >
							<span class="ti-close"></span>
						</button>
						{{Form::close()}}
					</td>
				</tr> 
				@endforeach
			</tbody>
		</table> 
		{{ $ctiendas->appends(\Request::except('page'))->render() }}
	</div>