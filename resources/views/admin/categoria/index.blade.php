@extends('admin.layout')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
	
	<style>
		img {
			max-height: 100px;
		}
	</style>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-6">
			<div class="content">
				<h2>Categoría de Tienda</h2>
				<a href="{{ADMIN_ROUTE}}categoria/create" class="btn btn-info btn-fill btn-wd">Nueva Categoría</a>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="content">
					<table id="tabla_alumno" class="table table-hover">
						<thead>
							<tr>
								<th>@sortablelink('nombre','NOMBRE')</th>
								<th>IMAGEN</th>
								<th>EDITAR</th>
								<th>ELIMINAR</th>
							</tr>
						</thead>
						<tbody>
							@foreach($categorias as $categoria)
							<tr>
								<td>
									<a href="{{ADMIN_ROUTE}}categoria/{{$categoria->id}}">{{$categoria->nombre}}</a>
								</td>
								<td>
									@if($categoria->imagen != "")
									<a href="/cat/{{($categoria->imagen) }}" data-lightbox="cat-{{($categoria->id)}}" data-title="{{$categoria->nombre}}" ><img class="img-responsive" src="/cat/{{($categoria->imagen) }}"></a>
									@else
									Sin imagen
									@endif
								</td>
								<td>
									<a class="btn btn-warning btn-fill" href="categoria/{{$categoria->id}}/edit" >
										<span class="ti-pencil"></span>
									</a>
								</td>
								<td>
									<a class="btn btn-danger btn-fill eliminar" data-id="{{$categoria->id}}" data-name="{{$categoria->nombre}}" >
										<span class="ti-close"></span>
									</a>
								</td>
							</tr> 
							@endforeach
						</tbody>
					</table>
					{{ $categorias->appends(\Request::except('page'))->render() }}
				</div>
			</div>
		</div>
	</div>
@endsection


@section('scripts')

	<script type="text/javascript">

        function deleteCategoria(id, name) {
            swal({
                title: "Confirmar eliminación", 
                text: "¿Está seguro de que quiere eliminar el tipo de local "+name+"?", 
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,

                cancelButtonText: "No, Cancelar!",
                confirmButtonText: "Sí, ¡eliminalo!",
                confirmButtonColor: "#3f51b5"
            }, function() {
                $.ajax({
                    url: "{{ADMIN_ROUTE}}categoria/"+id,
                    type: "DELETE"
                })
                .done(function(data) {
                	if(data == 0){
	                    swal("Error", "Primero elimina o cambia las tiendas que tiene esta categoria", "error");
                	}else{
                		swal("¡Eliminado!", "¡La categoríal fue eliminada correctamente!", "success");
                    	setTimeout(function() {
							location.reload();
						}, 1000);
                	}
                    
                })
                .error(function(data) {
                });
            });
        }

        $(document).on("click",'.eliminar',(function() {
            var id   = $(this).attr("data-id");
            var name = $(this).attr("data-name");
            deleteCategoria(id, name);
        }));

		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.categoria').addClass('active');

		});
	</script>
@endsection