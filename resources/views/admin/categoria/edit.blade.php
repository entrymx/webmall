@extends('admin.layout')

@section('breadcrums')

<a href="/categoria">CATEGORIA</a>
-
<a href="">{{$categoria->nombre}}</a>

@endsection

@section('content')
<div class="row">
	<div class="col-sm-6">			
		<div class="content">
			<h2>Editar Categoría</h2>			
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="content">
				{{Form::model($categoria,['method'=>'PATCH','route'=>['categoria.update',$categoria->id], 'files' => true])}}

				<fieldset>							
					<div class="form-group">
						<div class=" {{ $errors->has('nombre') ? 'has-error' : '' }}">
							{{ Form::label('nombre', 'Nombre') }}
							{{ Form::text('nombre', old('nombre'), array('class' => 'form-control border-input')) }}													
							<span class="text-danger">{{ $errors->first('nombre') }}</span>
						</div>	
					</div>

					<div class="form-group">
						<div class=" {{ $errors->has('imagen') ? 'has-error' : '' }}">
							@if($categoria->imagen != "")
							{{ Form::label('img', 'Imagen') }}
							<br>
							<a href="{{ADMIN_ROUTE}}categoria/borrarImagen/{{($categoria->id) }}" class="btn btn-danger ">Borrar</a>
							<a href="/cat/{{($categoria->imagen) }}" data-lightbox="imagen-{{($categoria->id)}}" data-title="{{$categoria->nombre}}">
								<img height="100px" src="/cat/{{($categoria->imagen) }}">
							</a>
							<br>
							<a class="btn btn-success cambio">Cambiar Imagen</a>							
							@else
							{{ Form::label('imagen', 'Agregar Imagen') }}
							@endif
							<span class="text-danger">{{ $errors->first('imagen') }}</span>
						</div>

						<div id="image-cropper" class="image-cropper">
							<div class="cropit-preview"></div>

							<input type="file" name='imagen' class="cropit-image-input" />
							<a class="btn btn-primary btn-fill select-image-btn">Seleccionar imagen</a>
							<a class="btn rotate-ccw"> <span class="ti-angle-left"> </a>
							<a class="btn rotate-cw"> <span class="ti-angle-right"> </span> </a>

							<div class="slider-wrapper">
								<h3> - 
								<input type="range" class="cropit-image-zoom-input custom" style="width:90%; display:inline-block;" min="0" max="1" step="0.01">
								 + </h3>
							</div>						


							<input type="hidden" name="imagen-cropped" id="imagen-cropped">
						</div>	
					</div>


					<button class="btn btn-primary form-control btn-fill">Guardar</button>
					<div class="gap-1"></div>
					<br>
					<a href="{{ADMIN_ROUTE}}categoria" class="btn btn-danger form-control btn-fill">Cancelar</a>
				</fieldset>

				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.nav li').removeClass('active');
		$('.categoria').addClass('active');

		@if($categoria->imagen != "")
			$('#image-cropper').hide();
		@endif

		$('.cambio').click(function() {
			$('#image-cropper').show();
		});

        // Croppit Inicializa
        $('#image-cropper').cropit({
            allowDragNDrop: true,
            imageBackground: true,
            imageBackgroundBorderWidth: 15, // Width of background border
            freeMoveboolean: true,
            minZoom: 'fit',
            smallImagestring: 'stretch',
            onImageError: function(error) {
                if(error.code){
                    alert('La imagen es muy pequeña \n'+
                           'Debe ser de {{imgCatW}} X {{imgCatH}}');
                }
            },
            width: {{imgCatW}},
            height: {{imgCatH}}
        });
		

		$('form').submit(function() {
			var imageData = $('#image-cropper').cropit('export');
			$('#imagen-cropped').val(imageData);
			
			var formValue = $(this).serialize();
			$('#result-data').text(formValue);

			return true;

		})

	});
</script>
@endsection