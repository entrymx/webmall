@extends('admin.layout')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
    <style>
		.carousel-inner img {
			margin: auto;
		}
	</style>
@endsection

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<div class="content">
				<h2>Categoría - {{$categoria->nombre}}
					<a class="btn btn-warning btn-fill pull-right" href="{{$categoria->id}}/edit" >
						<span class="ti-pencil"></span>
					</a>
					<a class="btn btn-danger btn-fill pull-right eliminar" data-id="{{$categoria->id}}" data-name="{{$categoria->nombre}}" >
						<span class="ti-close"></span>
					</a>
				</h2>			
			</div>
		</div>
	</div>

	<div class="card">
		<div class="header">
            <h4 class="title">TIENDAS - {{$categoria->nombre}}</h4>
        </div>
		<div class="content table-responsive tiendas ">
			<table id="" class="table table-hover">
				<thead>
					<tr>
						<th>@sortablelink('nombre','NOMBRE')</th>
						<th>@sortablelink('local.nombre','LOCAL')</th>
						<th>@sortablelink('apertura','HORARIO')</th>
						<th>@sortablelink('categoria.nombre','CATEGORÍA')</th>
						<th>@sortablelink('local.plaza_id','PLAZA')</th>
						<th>TAGS</th>
						<th>CONTACTO</th>
						<th>LOGO</th>
						<th>FOTOS</th>
						<th>EDITAR</th>
						<th>ELIMINAR</th>
					</tr>
				</thead>
				<tbody>
					@foreach($tiendas as $tienda)
					<tr>
						<td>
							<a href="{{ADMIN_ROUTE}}tienda/{{$tienda->id}}">{{$tienda->nombre}}</a>
						</td>
						<td>
							@if($tienda->local)
							{{$tienda->local->nombre}}
							@else
							Sin Local
							@endif
						</td>
						<td>
							{{$tienda->apertura." - ".$tienda->cierre}}
						</td>
						<td>
							{{$tienda->categoria->nombre}}
						</td>
						<td>
							{{$tienda->local->plaza->nombre}}
						</td>
						<td>	
							@foreach($tienda->tag->sortBy('nombre') as $tag)
							{{$tag->nombre}}
							@endforeach			                            
						</td>
						<td>
						@if($tienda->ctienda->count() > 0)
						@foreach($tienda->ctienda as $contacto)
							&#9702; {{$contacto->nombres." ".$contacto->apellidos}}<br>
						@endforeach
						@else
							Sin contacto
						@endif							
						</td>
						<td class="logo">
							@if($tienda->logo != "")
							<a href="/tie/{{($tienda->logo) }}" data-lightbox="logo-{{($tienda->id)}}" data-title="{{$tienda->nombre}}" ><img class="img-responsive" src="/tie/{{($tienda->logo) }}"></a>
							@else
							Sin Logo
							@endif
						</td>
						<td>	
							<div id="myCarousel-{{$loop->iteration}}" class="carousel slide" data-ride="carousel">

								<!-- Wrapper for slides -->
									<div class="carousel-inner">
									@foreach($tienda->imagen as $imagen)
										<div class="item @if($loop->index < 1) active @endif">
											<a href="/tie/{{($imagen->imagen) }}" target="_blank">
												<img src="/tie/{{($imagen->imagen) }}" class="img-responsive">
											</a>
											<br>									
										</div>										
									@endforeach
									</div>

							    <!-- Left and right controls -->
						    	@if($tienda->imagen->count() > 1)
								    <a class="left carousel-control" href="#myCarousel-{{$loop->iteration}}" data-slide="prev">
								    	<span class="glyphicon glyphicon-chevron-left"></span>
								    	<span class="sr-only">Previous</span>
								    </a>
								    <a class="right carousel-control" href="#myCarousel-{{$loop->iteration}}" data-slide="next">
								    	<span class="glyphicon glyphicon-chevron-right"></span>
								    	<span class="sr-only">Next</span>
								    </a>
								@endif
							</div>
						</td>
						
						<td>
							<a class="btn btn-warning btn-fill" href="{{ADMIN_ROUTE}}tienda/{{$tienda->id}}/edit" >
								<span class="ti-pencil"></span>
							</a>
						</td>
						
						<td>
							{{Form::model($tienda,['method'=>'DELETE','route'=>['tienda.destroy',$tienda->id]])}}
							<button class="btn btn-danger btn-fill" >
								<span class="ti-close"></span>
							</button>
							{{Form::close()}}
						</td>
					</tr> 
					@endforeach
				</tbody>
			</table> 
			{{ $tiendas->appends(\Request::except('page'))->render() }}
		</div>
	</div>

@endsection

@section('scripts')


    <script type="text/javascript">

        function deleteCategoria(id, name) {
            swal({
                title: "Confirmar eliminación", 
                text: "¿Está seguro de que quiere eliminar el tipo de local "+name+"?", 
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,

                cancelButtonText: "No, Cancelar!",
                confirmButtonText: "Sí, ¡eliminalo!",
                confirmButtonColor: "#3f51b5"
            }, function() {
                $.ajax({
                    url: "{{ADMIN_ROUTE}}categoria/"+id,
                    type: "DELETE"
                })
                .done(function(data) {
                	if(data == 0){
	                    swal("Error", "Primero elimina o cambia las tiendas que tiene esta categoria", "error");
                	}else{
                		swal("¡Eliminado!", "¡La categoríal fue eliminada correctamente!", "success");
                    	setTimeout(function() {
							window.location.replace('{{ADMIN_ROUTE."categoria"}}');
						}, 1000);
                	}
                    
                })
                .error(function(data) {
                });
            });
        }

        $(document).on("click",'.eliminar',(function() {
            var id   = $(this).attr("data-id");
            var name = $(this).attr("data-name");
            deleteCategoria(id, name);
        }));


		$(document).ready(function() {

			$('.nav li').removeClass('active');
			$('.categoria').addClass('active');

			plazas('');
			tiendas('');
	       
		});
	</script>



@endsection