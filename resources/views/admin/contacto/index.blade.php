@extends('admin.layout')

@section('head')
		<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('content')

<div class="row">
	<div class="col-sm-12">			
		<div class="content">
			<h2>Contactos de Plazas	</h2>				
			<a href="{{ADMIN_ROUTE}}contacto/create" class="btn btn-info btn-fill btn-wd ">Nuevo Contacto</a>

		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="content">                	
				<div class="row">
					<div class="body table-responsive all">
						<table id="tabla_alumno" class="table table-hover">
							<thead>
								<tr>
									<th>@sortablelink('nombres','NOMBRES')</th>
									<th>@sortablelink('apellidos','APELLIDOS')</th>
									<th>@sortablelink('correo','CORREO')</th>
									<th>@sortablelink('telefono','TELÉFONO')</th>
									<th>@sortablelink('plaza.nombre','PLAZA')</th>
									<th>EDITAR</th>
									<th>ELIMINAR</th>
								</tr>
							</thead>
							<tbody>
								@foreach($contactos as $contacto)
								<tr>
									<td>
										{{$contacto->nombres}}
									</td>

									<td>
										{{$contacto->apellidos}}
									</td>

									<td>
										{{$contacto->correo}}
									</td>

									<td>
										{{$contacto->telefono}}
									</td>

									<td>
										@if($contacto->plaza)
										{{$contacto->plaza->nombre}}
										@else
										Sin Plaza
										@endif
									</td>

									<td>
										<a class="btn btn-warning btn-fill" href="{{ADMIN_ROUTE}}contacto/{{$contacto->id}}/edit" >
											<span class="ti-pencil"></span>
										</a>
									</td>
									<td>
										{{Form::model($contacto,['method'=>'DELETE','route'=>['contacto.destroy',$contacto->id]])}}
										<button class="btn btn-danger btn-fill" >
											<span class="ti-close"></span>
										</button>
										{{Form::close()}}
									</td>
								</tr> 
								@endforeach
							</tbody>
						</table> 
						{{ $contactos->appends(\Request::except('page'))->render() }}
					</div>
				</div>
			</div>
			<div class="card-footer">
			</div> 
		</div>
	</div>
</div>


@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.nav li').removeClass('active');
		$('.contacto').addClass('active');
	});
</script>
@endsection	