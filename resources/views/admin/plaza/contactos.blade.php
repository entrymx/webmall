		<div class="content table-responsive contactos">
			<table class="table table-hover">
							<thead>
								<tr>
									<th>@sortablelink('nombres','NOMBRES')</th>
									<th>@sortablelink('apellidos','APELLIDOS')</th>
									<th>@sortablelink('correo','CORREO')</th>
									<th>@sortablelink('telefono','TELÉFONO')</th>
									<th>@sortablelink('plaza.nombre','PLAZA')</th>
									<th>EDITAR</th>
									<th>ELIMINAR</th>
								</tr>
							</thead>
							<tbody>
								@foreach($contactos as $contacto)
								<tr>
									<td>
										{{$contacto->nombres}}
									</td>

									<td>
										{{$contacto->apellidos}}
									</td>

									<td>
										{{$contacto->correo}}
									</td>

									<td>
										{{$contacto->telefono}}
									</td>

									<td>
										@if($contacto->plaza)
										{{$contacto->plaza->nombre}}
										@else
										Sin Plaza
										@endif
									</td>

									<td>
										<a class="btn btn-warning btn-fill" href="{{ADMIN_ROUTE}}contacto/{{$contacto->id}}/edit" >
											<span class="ti-pencil"></span>
										</a>
									</td>
									<td>
										{{Form::model($contacto,['method'=>'DELETE','route'=>['contacto.destroy',$contacto->id]])}}
										<button class="btn btn-danger btn-fill" >
											<span class="ti-close"></span>
										</button>
										{{Form::close()}}
									</td>
								</tr> 
								@endforeach
							</tbody>
			</table> 
			{{ $contactos->appends(\Request::except('page'))->render() }}
		</div>