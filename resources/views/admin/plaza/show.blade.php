@extends('admin.layout')

@section('head')
    <style>
		#map {
			height: 350px;
		}

		.logo, .carousel-inner img {
			margin: auto;
		}
	</style>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">			
			<div class="content">
				{{Form::model($plaza,['method'=>'DELETE','route'=>['plaza.destroy',$plaza->id]])}}
					<h2>Plaza - {{$plaza->nombre}}
						<a class="btn btn-warning btn-fill pull-right" href="{{ADMIN_ROUTE}}plaza/{{$plaza->id}}/edit" >
							<span class="ti-pencil"></span>
						</a>
						<button class="btn btn-danger btn-fill pull-right" >
							<span class="ti-close"></span>
						</button>
					</h2>			
				{{Form::close()}}
			</div>
		</div>
	</div>

	<div class="card">
		<div class="content">
			<div class="row">
				<div class="col-sm-1">				
					<label>Apertura</label>
					<p>{{$plaza->apertura}}</p>
				</div>
				<div class="col-sm-1">
					<label>Cierre</label>
					<p>{{$plaza->cierre}}</p>
				</div>
				<div class="col-sm-4">
					<label>Domicilio</label>
					<p>{{$plaza->ubicacion->domicilio}}</p>
				</div>
				<div class="col-sm-3">
					<label>Ciudad</label>
					<p>{{$plaza->ubicacion->ciudad->nombre}}</p>
				</div>
				<div class="col-sm-3">
					<label>Estado</label>
					<p>{{$plaza->ubicacion->ciudad->estado->nombre}}</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<div class="card">
				<div class="header">
					<h4 class="title">Logo</h4>
				</div>
				<div class="content">
					@if($plaza->logo != "")
					<a href="/pla/{{($plaza->logo) }}" data-lightbox="logo-{{($plaza->id)}}" data-title="{{$plaza->nombre}}">
						<img class="img-responsive logo" src="/pla/{{($plaza->logo) }}">
					</a>
					@else
					<p>Sin logo</p>
					@endif
				</div>
			</div>

			<div class="card">
				<div class="header">
					<h4 class="title">Imágenes</h4>
				</div>
				<div class="content">
					@if($plaza->imagen->count() > 1)
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Wrapper for slides -->
							<div class="carousel-inner">
							@foreach($plaza->imagen as $imagen)
								<div class="item @if($loop->index < 1) active @endif">
									<a href="/pla/{{($imagen->imagen) }}" data-lightbox="imagen-{{($plaza->id)}}" data-title="{{$plaza->nombre}}">
										<img src="/pla/{{($imagen->imagen) }}" class="img-responsive pull-center">
									</a>
									<br>									
								</div>										
							@endforeach
							</div>

					    <!-- Left and right controls -->
				    	
						    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
						    	<span class="glyphicon glyphicon-chevron-left"></span>
						    	<span class="sr-only">Previous</span>
						    </a>
						    <a class="right carousel-control" href="#myCarousel" data-slide="next">
						    	<span class="glyphicon glyphicon-chevron-right"></span>
						    	<span class="sr-only">Next</span>
						    </a>
					</div>
					@else
					<p>Sin Imágenes</p>
					@endif
				</div>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="card">
				<div class="header">
					<h4 class="title">Mapa</h4>
				</div>
				<div class="content">
					<div id="map"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="card ">
		<div class="header">
			<h4 class="title">CONTACTOS - {{$plaza->nombre}}</h4>
		</div>
		<div class="content table-responsive contactos">			
		</div>
	</div>

	<div class="card">
		<div class="header">
            <h4 class="title">NOTICIAS - {{$plaza->nombre}}</h4>
        </div>
		<div class="content table-responsive noticias">
			
		</div>
	</div>

	<div class="card">
		<div class="header">
            <h4 class="title">TIENDAS - {{$plaza->nombre}}</h4>
        </div>
		<div class="content table-responsive tiendas ">
			
		</div>
	</div>

	<div class="card ">
		<div class="header">
			<h4 class="title">LOCALES - {{$plaza->nombre}}</h4>
		</div>
		<div class="content table-responsive locales">
			
		</div>
	</div>

@endsection

@section('scripts')

	<script src="https://maps.googleapis.com/maps/api/js?key={{KEY}}&libraries=places" type="text/javascript"></script>
	<script>
		var map = new google.maps.Map(document.getElementById('map'),{
			center:{
				lat: {{$plaza->ubicacion->latitud}}, 
				lng: {{$plaza->ubicacion->longitud}}
			},
			zoom: 17
		});

		var marker = new google.maps.Marker({
			position:{
				lat: {{$plaza->ubicacion->latitud}}, 
				lng: {{$plaza->ubicacion->longitud}}
			},
			map: map,
			draggable:false			
		});
	</script>	

    <script type="text/javascript">

		function locales(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}
			$.ajax({
				url: "{{ADMIN_ROUTE}}plaza/locales"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					id: {{$plaza->id}}
				}
			})
			.done(function(data) {
				$('.locales').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}

		function tiendas(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}

			$.ajax({
				url: "{{ADMIN_ROUTE}}plaza/tiendas"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					id: {{$plaza->id}}
				}
			})
			.done(function(data) {
				$('.tiendas').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}

		function noticias(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}

			$.ajax({
				url: "{{ADMIN_ROUTE}}plaza/noticias"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					id: {{$plaza->id}}
				}
			})
			.done(function(data) {
				$('.noticias').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}

		function contactos(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}

			$.ajax({
				url: "{{ADMIN_ROUTE}}plaza/contactos"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					id: {{$plaza->id}}
				}
			})
			.done(function(data) {
				$('.contactos').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}

		$(document).ready(function() {

			$(document).on('click', '.tiendas .pagination a, .tiendas th a', function (e) {
				tiendas($(this).attr('href').split('sort')[1]);
				e.preventDefault();
			});

			$(document).on('click', '.locales .pagination a, .locales th a', function (e) {
				locales($(this).attr('href').split('sort')[1]);
				e.preventDefault();
			});

			$(document).on('click', '.noticias .pagination a, .noticias th a', function (e) {
				noticias($(this).attr('href').split('sort')[1]);
				e.preventDefault();
			});

			$(document).on('click', '.contactos .pagination a, .contactos th a', function (e) {
				contactos($(this).attr('href').split('sort')[1]);
				e.preventDefault();
			});

			$('.nav li').removeClass('active');
			$('.plaza').addClass('active');

			locales('');
			noticias('');
			tiendas('');
			contactos('');

		});
	</script>



@endsection