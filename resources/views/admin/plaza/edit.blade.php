@extends('admin.layout')

@section('head')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<style>
		#map {
			height: 350px;
		}
	</style>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">			
			<div class="content">
				<h2>Editar Plaza</h2>			
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="content">
					{{Form::model($plaza,['method'=>'PATCH','route'=>['plaza.update',$plaza->id], 'files' => true])}}
							<!-- $plaza->ubicacion->id -->
							<fieldset>
								<div class="form-group">
									<div class=" {{ $errors->has('nombre') ? 'has-error' : '' }}">
										{{ Form::label('nombre', 'Nombre') }}
	        							{{ Form::text('nombre', old('nombre'), array('class' => 'form-control border-input')) }}													
										<span class="text-danger">{{ $errors->first('nombre') }}</span>
									</div>	
								</div>
								

								<div class="form-group">
									<div class=" {{ $errors->has('apertura') ? 'has-error' : '' }}">
										{{ Form::label('apertura', 'Apertura') }}
										{{ Form::text('apertura', old('apertura'), array('id'=>'apertura' ,'class' => 'form-control border-input datepicker', 'placeholder' => '24Hrs Ejemplo: 06:00 - 23:00')) }}													
										<span class="text-danger">{{ $errors->first('apertura') }}</span>
									</div>	
								</div>
								<div class="form-group">
									<div class=" {{ $errors->has('horario') ? 'has-error' : '' }}">
										{{ Form::label('cierre', 'Cierre') }}
										{{ Form::text('cierre', old('cierre'), array('id'=>'cierre' ,'class' => 'form-control border-input datepicker', 'placeholder' => '24Hrs Ejemplo: 06:00 - 23:00')) }}													
										<span class="text-danger">{{ $errors->first('horario') }}</span>
									</div>	
								</div>

								<strong><p>Ubicación</p></strong>

								<div class="form-group">
									<div class=" {{ $errors->has('searchBox') ? 'has-error' : '' }}">
										{{ Form::label('searchBox', 'Busca la plaza aquí, será más simple') }}
										{{ Form::text('searchBox', old('searchBox'), array('class' => 'form-control border-input')) }}													
										<span class="text-danger">{{ $errors->first('searchBox') }}</span>
									</div>	
								</div>

								<div class="form-group">
									<div id="map"></div>
								</div>
								
								<div class="form-group">
									<div class=" {{ $errors->has('domicilio') ? 'has-error' : '' }}">
										{{ Form::label('domicilio', 'Domicilio') }}
										{{ Form::text('domicilio', $plaza->ubicacion->domicilio, array('class' => 'form-control border-input')) }}													
										<span class="text-danger">{{ $errors->first('domicilio') }}</span>
									</div>	
								</div>

								<div class="form-group">
									<div class=" {{ $errors->has('estado') ? 'has-error' : '' }}">
										{{ Form::label('estado', 'Estado') }}
										{{ Form::text('estado', $plaza->ubicacion->ciudad->estado->nombre, array('class' => 'form-control border-input')) }}													
										<span class="text-danger">{{ $errors->first('estado') }}</span>
									</div>	
								</div>

								<div class="form-group">
									<div class=" {{ $errors->has('ciudad') ? 'has-error' : '' }}">
										{{ Form::label('ciudad', 'Ciudad') }}
										{{ Form::text('ciudad', $plaza->ubicacion->ciudad->nombre, array('class' => 'form-control border-input')) }}													
										<span class="text-danger">{{ $errors->first('ciudad') }}</span>
									</div>
								</div>

								{{ Form::hidden('latitud', old('latitud'), array('id' => 'latitud')) }}
								{{ Form::hidden('longitud', old('longitud'), array('id' => 'longitud')) }}													

								<strong><p>Imágenes</p></strong>
						
								<div class="form-group">
									<div class=" {{ $errors->has('logo') ? 'has-error' : '' }}">
										{{ Form::label('logo', 'Logo') }}
										<br>
										@if($plaza->logo != "")
										<a href="{{ADMIN_ROUTE}}plaza/borrarLogo/{{($plaza->id) }}" class="btn btn-danger ">Borrar</a>
										<a href="/pla/{{($plaza->logo) }}" data-lightbox="logo-{{($plaza->id)}}" data-title="{{$plaza->nombre}}">
											<img height="100px" src="/pla/{{($plaza->logo) }}">
										</a>
										<br>
										<a class="btn btn-success cambio">Cambiar Imagen</a>
										
										@else								
										{{ Form::label('logo', 'Agregar Imagen') }}
										@endif
										<span class="text-danger">{{ $errors->first('logo') }}</span>
									</div>
									<div id="logo-cropper" class="image-cropper">
										<div class="cropit-preview"></div>

										<input type="file" name='imagen' class="cropit-image-input" />
										<a class="btn btn-primary btn-fill select-image-btn">Seleccionar imagen</a>
										<a class="btn rotate-ccw"> <span class="ti-angle-left"> </a>
										<a class="btn rotate-cw"> <span class="ti-angle-right"> </span> </a>

										<div class="slider-wrapper">
											<h3> - 
											<input type="range" class="cropit-image-zoom-input custom" min="0" max="1" step="0.01">
											 + </h3>
										</div>						


										<input type="hidden" name="logo-img" id="logo-img">
									</div>	
								</div>

								<div class="form-group fotos">

									<div class=" {{ $errors->has('fotos') ? 'has-error' : '' }}">
										{{ Form::label('fotos', 'Fotos') }}
										<br>
										@foreach($plaza->imagen as $imagen)
											<a href="{{ADMIN_ROUTE}}plaza/borrarImagen/{{($imagen->id) }}" class="btn btn-danger">Borrar</a>
											<a href="/pla/{{($imagen->imagen) }}" data-lightbox="imagen-{{($plaza->id)}}" data-title="{{$plaza->nombre}}">
												<img height="100px" src="/pla/{{($imagen->imagen) }}">
											</a>
											<br><br>
										@endforeach

										<div id="foto-cropper-1" class="image-cropper">
											<div class="cropit-preview"></div>
												
											<input type="file" name="foto-1'" id="foto-1" multiple class="cropit-image-input">			

											<a class="btn btn-primary btn-fill select-image-btn">Selcecionar imagen</a>
											<a class="btn rotate-ccw"> <span class="ti-angle-left"> </a>
											<a class="btn rotate-cw"> <span class="ti-angle-right"> </span> </a>

											<div class="slider-wrapper">
												<h3> - 
												<input type="range" class="cropit-image-zoom-input custom" min="0" max="1" step="0.01">
												 + </h3>
											</div>

											<input type="hidden" name="foto-1-img" id="foto-1-img">
										</div>
									</div>
								</div>	
														                    
								<a class="btn btn-primary btn-fill otraFoto">Agregar otra foto</a>
					
								<input type="hidden" name="foto-id" id="foto-id">							

			                    
								<button class="btn btn-fill btn-primary form-control">Guardar</button>
								<div class="gap-1"></div>
								<br>
								<a href="{{ADMIN_ROUTE}}plaza" class="btn btn-fill btn-danger form-control">Cancelar</a>
							</fieldset>

				    	{{Form::close()}}
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')

    <!-- Moment Plugin Js -->
    <script src="{{ asset('plugins/momentjs/moment.js')}}"></script>
    <script src="{{ asset('plugins/momentjs/es.js')}}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>


	<script type="text/javascript">
		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.plaza').addClass('active');

	        $('#apertura, #cierre ').bootstrapMaterialDatePicker({
	            format: 'HH:mm',
	            clearButton: true,
	            weekStart: 1,
	            lang : 'es',
	            date: false,
	        });

	        $('.cambio').click(function() {
				$('#logo-cropper').show();
			});

			@if($plaza->logo != "")
				$('#logo-cropper').hide();
			@endif


			var fotoId = 1; 
            //Inicializa Logo
            $('#logo-cropper').cropit({
                
                allowDragNDrop: true,
                imageBackground: true,
                imageBackgroundBorderWidth: 15, // Width of background border
                freeMoveboolean: true,
                minZoom: 'fit',
                smallImagestring: 'stretch',
                onImageError: function(error) {
                    if(error.code){
                        alert('La imagen es muy pequeña \n'+
                               'Debe ser de {{logoPlazaW}} X {{logoPlazaH}} pixeles');
                    }
                },
                width: {{logoPlazaW}},
                height: {{logoPlazaH}}
			});

            //Inicializa foto
            $('#foto-cropper-1').cropit({
                
                allowDragNDrop: true,
                imageBackground: true,
                imageBackgroundBorderWidth: 15, // Width of background border
                freeMoveboolean: true,
                minZoom: 'fit',
                smallImagestring: 'stretch',
                onImageError: function(error) {
                    if(error.code){
                        alert('La imagen es muy pequeña \n'+
                               'Debe ser de {{logoPlazaW}} X {{logoPlazaH}}');
                    }
                },
                width: {{fotoPlazaW}},
                height: {{fotoPlazaH}}
			});

	    	////////////////

			$('.otraFoto').click(function(){
				otraFoto();
			});
			
			//Agrega un nuevo selector de foto
			function otraFoto() {
				fotoId = fotoId + 1;

				var code =	'<hr>'+
							'<div id="foto-cropper-'+fotoId+'" class="image-cropper">'+
								'<div class="cropit-preview"></div>'+
								'<input type="file" name="foto-'+fotoId+'" id="foto-'+fotoId+'" multiple class="cropit-image-input">'+
								'<a class="btn btn-primary btn-fill select-image-btn">Seleccionar imagen</a>'+
								'<a class="btn rotate-ccw"> <span class="ti-angle-left"> </a>'+
								'<a class="btn rotate-cw"> <span class="ti-angle-right"> </span> </a>'+
								'<div class="slider-wrapper">'+
									'<h3> - <input type="range" class="cropit-image-zoom-input custom" min="0" max="1" step="0.01"> + </h3>'+
								'</div>'+
								'<input type="hidden" name="foto-'+fotoId+'-img" id="foto-'+fotoId+'-img">'+
							'</div>';

				$('.fotos').append(code)

	            $('#foto-cropper-'+fotoId).cropit({
	                
	                allowDragNDrop: true,
	                imageBackground: true,
	                imageBackgroundBorderWidth: 15, // Width of background border
	                freeMoveboolean: true,
	                minZoom: 'fit',
	                smallImagestring: 'stretch',
	                onImageError: function(error) {
	                    if(error.code){
	                        alert('La imagen es muy pequeña \n'+
	                               'Debe ser de {{logoPlazaW}} X {{logoPlazaH}}');
	                    }
	                },
	                width: {{fotoPlazaW}},
	                height: {{fotoPlazaH}}
				});
			}

			$('form').submit(function() {
				var imagenLogo = $('#logo-cropper').cropit('export');
				$('#logo-img').val(imagenLogo);
				
				for (i = 1; i <= fotoId; i++) { 
					$('#foto-'+i+'-img').val( $('#foto-cropper-'+i).cropit('export') );
				}

				$('#foto-id').val(fotoId);
				
				var formValue = $(this).serialize();
				$('#result-data').text(formValue);

				return true;

			})

		});
	</script>


	<script src="https://maps.googleapis.com/maps/api/js?key={{KEY}}&libraries=places" type="text/javascript"></script>
	<script>
		var map = new google.maps.Map(document.getElementById('map'),{
			center:{
				lat: {{$plaza->ubicacion->latitud}}, 
				lng: {{$plaza->ubicacion->longitud}}
			},
			zoom: 17
		});

		var marker = new google.maps.Marker({
			position:{
				lat: {{$plaza->ubicacion->latitud}}, 
				lng: {{$plaza->ubicacion->longitud}}
			},
			map: map,
			draggable:true			
		});

		var searchBox = new google.maps.places.SearchBox(document.getElementById('searchBox'));

		google.maps.event.addListener(searchBox,'places_changed',function(){

			var places = searchBox.getPlaces();
			var bounds = new google.maps.LatLngBounds();
			var i, place;

			for(i=0; place=places[i];i++){
				bounds.extend(place.geometry.location);
				marker.setPosition(place.geometry.location);
			}

			map.fitBounds(bounds);
			map.setZoom(15);

		})

		var geocoder = new google.maps.Geocoder;

		
		google.maps.event.addListener(marker,'position_changed',function(){

			var lat = marker.getPosition().lat();
			var lng = marker.getPosition().lng();

			var latlng = {lat:lat, lng:lng};

			geocoder.geocode({'location': latlng}, function(results, status) {

				var estado    = "";
				var municipio = "";
				var domicilio = "";
				if (status === 'OK') {
					if (results[1]) {
						
						domicilio = results[1].formatted_address;
						$(results[1]['address_components']).each(function(){
							var $super = this;
							//console.log(this.long_name);
							$(this['types']).each(function(){
								if(this == "administrative_area_level_1"){
									console.log('administrative_area_level_1: '+$super.long_name);
									estado = $super.long_name;
								}
							});
							
							$(this['types']).each(function(){
								if(this == "locality"){
									console.log('locality: '+$super.long_name);

									municipio = $super.long_name;
								}
							});
							/*
							$(this['types']).each(function(){
								if(this == "administrative_area_level_2"){
									console.log('administrative_area_level_2: '+$super.long_name);
									municipio += $super.long_name;
								}
							});
							*/
							$(this['types']).each(function(){
								if(this == "administrative_area_level_3"){
									console.log('administrative_area_level_3: '+$super.long_name);
									municipio = $super.long_name;
								}
							});

						});
						

					$('#estado').val(estado);
					$('#ciudad').val(municipio);
					$('#domicilio').val(domicilio);

	
					} else {
					console.log('No results found');
					}
				} else {
					console.log('Geocoder failed due to: ' + status);
				}
			});

			$('#latitud').val(lat);
			$('#longitud').val(lng);
		})

		$('#latitud').val({{$plaza->ubicacion->latitud}});
		$('#longitud').val({{$plaza->ubicacion->longitud}});
	</script>	

@endsection