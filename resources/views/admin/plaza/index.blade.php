@extends('admin.layout')

@section('head')
		<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('content')
<div class="row">
	<div class="col-sm-6">			
		<div class="content">
			<h2>Plazas</h2>
			<a href="{{ADMIN_ROUTE}}plaza/create" class="btn btn-info btn-fill btn-wd">Nueva Plaza</a>					
		</div>
	</div>
</div>
<br>
	@if(count($errors)>0)
        <div class="row alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
<div class="row">
	<div class="col-sm-12">
		<div class="card">
		<div class="content">				
				<table id="tabla_alumno" class="table table-hover">
					<thead>
						<tr>
							<th>@sortablelink('nombre','NOMBRE')</th>
							<th>@sortablelink('apertura','HORARIO')</th>
							<th>@sortablelink('ubicacion.domicilio','UBICACIÓN')</th>
							<th>CIUDAD</th>
							<th>CONTACTO</th>
							<th>LOGO</th>
							<th>FOTOS</th>
							<th>EDITAR</th>
							<th>ELIMINAR</th>
						</tr>
					</thead>
					<tbody>
						@foreach($plazas as $plaza)
						<tr>
							<td>
								<a href="{{ADMIN_ROUTE}}plaza/{{$plaza->id}}">{{$plaza->nombre}}</a>
							</td>
							<td>
								{{$plaza->apertura." - ".$plaza->cierre}}
							</td>
							<td>
								{{$plaza->ubicacion->domicilio}}
							</td>
							<td>
								{{$plaza->ubicacion->ciudad->nombre}}, 
								{{$plaza->ubicacion->ciudad->estado->nombre}}
							</td>
							<td>
							@if($plaza->contacto->count() > 0)
							@foreach($plaza->contacto as $contacto)
								&#9702; {{$contacto->nombres." ".$contacto->apellidos." - ".$contacto->email}}<br>
							@endforeach
							@else
								Sin contacto
							@endif
							</td>
							<td>
								@if($plaza->logo != "")
								<a href="/pla/{{($plaza->logo) }}" data-lightbox="logo-{{($plaza->id)}}" data-title="{{$plaza->nombre}}" ><img class="img-responsive" src="/pla/{{($plaza->logo) }}"></a>
								@else
								Sin Logo
								@endif
							</td>
							<td>	

								<div id="myCarousel-{{$loop->iteration}}" class="carousel slide" data-ride="carousel">

								<!-- Wrapper for slides -->
									<div class="carousel-inner">
									@foreach($plaza->imagen as $imagen)
										<div class="item @if($loop->index < 1) active @endif">
											<a href="/pla/{{($imagen->imagen) }}" data-lightbox="imagen-{{($plaza->id)}}" data-title="{{$plaza->nombre}}">
												<img src="/pla/{{($imagen->imagen) }}" class="img-responsive">
											</a>
											<br>									
										</div>										
									@endforeach
									</div>

							    <!-- Left and right controls -->
						    	@if($plaza->imagen->count() > 1)
								    <a class="left carousel-control" href="#myCarousel-{{$loop->iteration}}" data-slide="prev">
								    	<span class="glyphicon glyphicon-chevron-left"></span>
								    	<span class="sr-only">Previous</span>
								    </a>
								    <a class="right carousel-control" href="#myCarousel-{{$loop->iteration}}" data-slide="next">
								    	<span class="glyphicon glyphicon-chevron-right"></span>
								    	<span class="sr-only">Next</span>
								    </a>
								@endif
								</div>
							</td>
							<td>
								<a class="btn btn-warning btn-fill" href="plaza/{{$plaza->id}}/edit" >
									<span class="ti-pencil"></span>
								</a>
							</td>
							<td>
								{{Form::model($plaza,['method'=>'DELETE','route'=>['plaza.destroy',$plaza->id]])}}
								<button class="btn btn-danger btn-fill" >
									<span class="ti-close"></span>
								</button>
								{{Form::close()}}
							</td>
						</tr> 
						@endforeach
					</tbody>
				</table> 		
				{{ $plazas->appends(\Request::except('page'))->render() }}	    
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.nav li').removeClass('active');
		$('.plaza').addClass('active');

	});
</script>
@endsection