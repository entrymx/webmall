	
	<div class="content table-responsive noticias">
			<table id="tabla_alumno" class="table table-hover">
				<thead>
					<tr>
						<th>@sortablelink('titulo','TITULO')</th>
						<th>@sortablelink('descripcion','DESCRIPCIÓN')</th>
						<th>@sortablelink('fecha','FECHA')</th>
						<th>@sortablelink('plaza.nombre','PLAZA')</th>
						<th>IMAGEN</th>
						<th>EDITAR</th>
						<th>ELIMINAR</th>
					</tr>
				</thead>
				<tbody>
					@foreach($noticias as $noticia)
					<tr>
						<td>
							{{$noticia->titulo}}
						</td>
						<td>
							{{$noticia->descripcion}}
						</td>
						<td>
							{{$noticia->fecha}}
						</td>
						<td>
							{{$noticia->plaza->nombre}}
						</td>
						<td>
							@if($noticia->imagen != "")
							<a href="/noti/{{($noticia->imagen) }}"><img height="100px" src="/noti/{{($noticia->imagen) }}"></a>
							@else
							Sin imagen
							@endif
						</td>
						<td>
							<a class="btn btn-warning btn-fill" href="noticia/{{$noticia->id}}/edit" >
								<span class="ti-pencil"></span>
							</a>
						</td>
						<td>
							{{Form::model($noticia,['method'=>'DELETE','route'=>['noticia.destroy',$noticia->id]])}}
							<button class="btn btn-danger btn-fill" >
								<span class="ti-close"></span>
							</button>
							{{Form::close()}}
						</td>
					</tr> 
					@endforeach
				</tbody>
			</table> 
			{{ $noticias->appends(\Request::except('page'))->render() }}
		</div>