<!DOCTYPE html>
<html>
  <head>
    <title>Simple Map</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">


    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {

      }
      /* Optional: Makes the sample page fill the window. */

    </style>
  </head>
  <body>
  	
	
  	<div>
    	<div id="map"></div>
	</div>


    <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          //center: {lat: -34.397, lng: 150.644},
          center: {lat: 24.0207465, lng: -104.6831055},
          zoom: 8
        });
      }
    </script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHZgOIitZq4ZAuC9kZxb2a3s80PYITTds&callback=initMap" async defer></script>
  </body>
</html>