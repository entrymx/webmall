@extends('admin.layout')

@section('head')
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <style type="text/css">
    	.ui-select-choices {
	        position: fixed;
	        top: auto;
	        left: auto;
	        width: inherit;
      	}
    </style>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">			
			<div class="content">
				<h2>Editar Noticia</h2>			
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="content">
					{{Form::model($noticia,['method'=>'PATCH','route'=>['noticia.update',$noticia->id], 'files' => true])}}

					<fieldset>
						<div class="form-group">
							<div class=" {{ $errors->has('titulo') ? 'has-error' : '' }}">
								{{ Form::label('titulo', 'Titulo') }}
								{{ Form::text('titulo', old('titulo'), array('class' => 'form-control border-input border-input')) }}													
								<span class="text-danger">{{ $errors->first('titulo') }}</span>
							</div>	
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('descripcion') ? 'has-error' : '' }}">
								{{ Form::label('descripcion', 'Descripcion') }}
								{{ Form::text('descripcion', old('descripcion'), array('class' => 'form-control border-input')) }}													
								<span class="text-danger">{{ $errors->first('descripcion') }}</span>
							</div>	
						</div>


						<div class="form-group">
							<div class=" {{ $errors->has('fecha') ? 'has-error' : '' }}">
								{{ Form::label('fecha', 'Fecha') }}
								{{ Form::text('fecha', old('fecha'), array('class' => 'form-control border-input')) }}													
								<span class="text-danger">{{ $errors->first('fecha') }}</span>
							</div>	
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('tipo') ? 'has-error' : '' }}">
								{{ Form::label('tipo', 'Tipo') }}
								<select id='tipo' name='tipo' title="Ningúna tipo seleccionada" class="selectpicker form-control show-tick border-input" data-live-search="true">
									<option value='n' >Noticia</option>
									<option value='a' >Aviso</option>
								</select>
								<span class="text-danger">{{ $errors->first('plaza') }}</span>
							</div>
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('plaza') ? 'has-error' : '' }}">
								{{ Form::label('plaza', 'Plaza') }}
								<select id='plaza' name='plaza' title="Ningúna plaza seleccionada" class="selectpicker form-control show-tick border-input" data-live-search="true">
									@foreach($plazas as $u)
									<option value={{$u->id}} >{{$u->nombre}}</option>
									@endforeach
								</select>
								<span class="text-danger">{{ $errors->first('plaza') }}</span>
							</div>
						</div>

						<div class="form-group">
							<div class=" {{ $errors->has('foto') ? 'has-error' : '' }}">
								@if($noticia->imagen != "")
								{{ Form::label('img', 'Imagen') }}
								<br>
								<a href="{{ADMIN_ROUTE}}noticia/borrarImagen/{{($noticia->id) }}" class="btn btn-danger ">Borrar</a>
								<a href="/noti/{{($noticia->imagen) }}" data-lightbox="imagen-{{($noticia->id)}}" data-title="{{$noticia->titulo}}">
									<img height="100px" src="/noti/{{($noticia->imagen) }}">
								</a>
								<br>
								<a class="btn btn-success cambio">Cambiar Imagen</a>
								@else
								{{ Form::label('foto', 'Agregar Imagen') }}
								@endif
								<span class="text-danger">{{ $errors->first('foto') }}</span>
							</div>	

							<div id="imagen-cropper" class="image-cropper">
								<div class="cropit-preview"></div>
									
								<input type="file" name="logo" id="logo" class="cropit-image-input">

								<a class="btn btn-primary btn-fill select-image-btn">Seleccionar imagen</a>
								<a class="btn rotate-ccw"> <span class="ti-angle-left"> </a>
								<a class="btn rotate-cw"> <span class="ti-angle-right"> </span> </a>

								<div class="slider-wrapper">
									<h3> - 
									<input type="range" class="cropit-image-zoom-input custom"  min="0" max="1" step="0.01">
									 + </h3>
								</div>

								<input type="hidden" name="imagen-img" id="imagen-img">
							</div>
						</div>					


						<button class="btn btn-primary form-control btn-fill">Guardar</button>
						<div class="gap-1"></div>
						<br>
						<a href="{{ADMIN_ROUTE}}noticia" class="btn btn-danger form-control btn-fill">Cancelar</a>
					</fieldset>
					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
    <script src="{{ asset('plugins/select2/js/select2.min.js')}}"></script>

    <!-- Moment Plugin Js -->
    <script src="{{ asset('plugins/momentjs/moment.js')}}"></script>
    <script src="{{ asset('plugins/momentjs/es.js')}}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

	<script type="text/javascript">

		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.noticia').addClass('active');

			$('#fecha').bootstrapMaterialDatePicker({
				format: 'dddd DD MM YYYY',
				clearButton: true,
				weekStart: 1,
				lang : 'es',
				time: false,
				//currentDate: "00:00"
			});

			$("#plaza").select2({
			  //tags: true,
			 	"language": {
					"noResults": function(){
					return "Sin resultados";
					}
				},

				//containerCssClass: 'form-control',
				width: 'resolve',
				
				casesensitive: true,
			  	placeholder: "Teclea para buscar, enter para selecionar",
			  	allowClear: true
			});

			$('#plaza').select2("val",{{$noticia->plaza->id}});

			$("#tipo").select2({
			  //tags: true,
			 	"language": {
					"noResults": function(){
					return "Sin resultados";
					}
				},

				//containerCssClass: 'form-control',
				width: 'resolve',
				
				casesensitive: true,
			  	placeholder: "Teclea para buscar, enter para selecionar",
			  	allowClear: true
			});

			$('#tipo').select2("val",'{{$noticia->tipo}}');

			@if($noticia->imagen != "")
				$('#imagen-cropper').hide();
			@endif
			$('.cambio').click(function() {
				$('#imagen-cropper').show();
			});

            //Inicializa Logo
            $('#imagen-cropper').cropit({
                
                allowDragNDrop: true,
                imageBackground: true,
                imageBackgroundBorderWidth: 15, // Width of background border
                freeMoveboolean: true,
                minZoom: 'fit',
                smallImagestring: 'stretch',
                onImageError: function(error) {
                    if(error.code){
                        alert('La imagen es muy pequeña \n'+
                               'Debe ser de {{imagenNotiW}} X {{imagenNotiH}}');
                    }
                },
                width: {{imagenNotiW}},
                height: {{imagenNotiH}}
			});

			$('form').submit(function() {
				var imagen = $('#imagen-cropper').cropit('export');
				$('#imagen-img').val(imagen);				
				
				var formValue = $(this).serialize();
				$('#result-data').text(formValue);

				return true;

			})


		});


	</script>

@endsection