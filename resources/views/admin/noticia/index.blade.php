@extends('admin.layout')


@section('head')
	<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">			
		<div class="content">
			<h2>Noticias</h2>
			<a href="{{ADMIN_ROUTE}}noticia/create" class="btn btn-info btn-fill btn-wd">Nueva Noticia</a>
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="content">
				<table id="tabla_alumno" class="table table-hover">
					<thead>
						<tr>
							<th>@sortablelink('titulo','TITULO')</th>
							<th>@sortablelink('descripcion','DESCRIPCIÓN')</th>
							<th>@sortablelink('fecha','FECHA')</th>
							<th>@sortablelink('tipo','TIPO')</th>
							<th>@sortablelink('plaza.nombre','PLAZA')</th>
							<th>IMAGEN</th>
							<th>EDITAR</th>
							<th>ELIMINAR</th>
						</tr>
					</thead>
					<tbody>
						@foreach($noticias as $noticia)
						<tr>
							<td>
								{{$noticia->titulo}}
							</td>
							<td>
								{{$noticia->descripcion}}
							</td>
							<td>
								{{$noticia->fecha}}
							</td>
							<td>
								@if($noticia->tipo == "n")
									Noticia
								@endif
								@if($noticia->tipo == "a")
									Aviso
								@endif
							</td>
							<td>
								{{$noticia->plaza->nombre}}
							</td>
							<td>
								@if($noticia->imagen != "")
								<a href="/noti/{{($noticia->imagen) }}" data-lightbox="imagen-{{($noticia->id)}}" data-title="{{$noticia->titulo}}"><img class='img-responsive' src="/noti/{{($noticia->imagen) }}"></a>
								@else
								Sin imagen
								@endif
							</td>
							<td>
								<a class="btn btn-warning btn-fill" href="noticia/{{$noticia->id}}/edit" >
									<span class="ti-pencil"></span>
								</a>
							</td>
							<td>
								{{Form::model($noticia,['method'=>'DELETE','route'=>['noticia.destroy',$noticia->id]])}}
								<button class="btn btn-danger btn-fill" >
									<span class="ti-close"></span>
								</button>
								{{Form::close()}}
							</td>
						</tr> 
						@endforeach
					</tbody>
				</table> 
				{{ $noticias->appends(\Request::except('page'))->render() }}
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.nav li').removeClass('active');
		$('.noticia').addClass('active');

	});
</script>
@endsection
