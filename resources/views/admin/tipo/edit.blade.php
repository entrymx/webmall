@extends('admin.layout')

@section('content')
<div class="row">
	<div class="col-sm-12">			
		<div class="content">
			<h2>Editar Tipo de Local</h2>			
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-8">
		<div class="card">
			<div class="content">
				{{Form::model($tipo,['method'=>'PATCH','route'=>['tipo.update',$tipo->id]])}}

				<fieldset>							
					<div class="form-group">
						<div class=" {{ $errors->has('nombre') ? 'has-error' : '' }}">
							{{ Form::label('nombre', 'Nombre') }}
							{{ Form::text('nombre', old('nombre'), array('class' => 'form-control border-input')) }}													
							<span class="text-danger">{{ $errors->first('nombre') }}</span>
						</div>	
					</div>

					<button class="btn btn-primary form-control btn-fill">Guardar</button>
					<div class="gap-1"></div>
					<br>
					<a href="{{ADMIN_ROUTE}}tipo" class="btn btn-danger form-control btn-fill">Cancelar</a>
				</fieldset>

				{{Form::close()}}
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.nav li').removeClass('active');
		$('.tipo').addClass('active');

	});
</script>
@endsection