@extends('admin.layout')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('content')
<div class="row">
	<div class="col-sm-6">			
		<div class="content">
			<h2>Tipo de Local</h2>
			<a href="{{ADMIN_ROUTE}}tipo/create" class="btn btn-info btn-fill btn-wd">Nuevo Tipo</a>					
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="content">
				<table id="tabla_alumno" class="table table-hover">
					<thead>
						<tr>
							<th>@sortablelink('nombre','NOMBRE')</th>
							<th>EDITAR</th>
							<th>ELIMINAR</th>
						</tr>
					</thead>
					<tbody>
						@foreach($tipos as $tipo)
						<tr>
							<td>
								{{$tipo->nombre}}
							</td>

							<td>
								<a class="btn btn-warning btn-fill" href="{{ADMIN_ROUTE}}tipo/{{$tipo->id}}/edit" >
									<span class="ti-pencil"></span>
								</a>
							</td>
							<td>
								
								<a class="btn btn-danger btn-fill eliminar" data-id={{$tipo->id}} data-name={{$tipo->nombre}} >
									<span class="ti-close"></span>
								</a>
							</td>
						</tr> 
						@endforeach
					</tbody>
				</table> 
				{{ $tipos->appends(\Request::except('page'))->render() }}
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')

	<script type="text/javascript">

        function deleteTipo(id, name) {
            swal({
                title: "Confirmar eliminación", 
                text: "¿Está seguro de que quiere eliminar el tipo de local "+name+"?", 
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,

                cancelButtonText: "No, Cancelar!",
                confirmButtonText: "Sí, ¡eliminalo!",
                confirmButtonColor: "#3f51b5"
            }, function() {
                $.ajax({
                    url: "{{ADMIN_ROUTE}}tipo/"+id,
                    type: "DELETE"
                })
                .done(function(data) {
                	if(data == 0){
	                    swal("Error", "Primero elimina o cambia los locales que tiene este tipo", "error");
                	}else{
                		swal("¡Eliminado!", "¡El tipo de local fue eliminado correctamente!", "success");
                    	setTimeout(function() {
							location.reload();
						}, 1000);
                	}
                    
                })
                .error(function(data) {
                });
            });
        }

        $(document).on("click",'.eliminar',(function() {
            var id   = $(this).attr("data-id");
            var name = $(this).attr("data-name");
            deleteTipo(id, name);
        }));

		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.tipo').addClass('active');

		});
	</script>
@endsection