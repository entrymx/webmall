<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <!--meta http-equiv="X-UA-Compatible" content="IE=edge"-->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Web Mall</title>

  <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    
    <!-- Bootstrap Select Css -->
    <link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="{{ asset('plugins/dropzone/dropzone.css') }}" rel="stylesheet" />
 

  <!-- Fonts -->
  <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
  @yield('head')
</head>
<body>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle Navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Web Mall</a>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <!--ul class="nav navbar-nav">
          <li><a href="{{ url('/') }}">Inicio</a></li>
        </ul-->

        <ul class="nav navbar-nav navbar-right">
          @if (Auth::guest())
          <li><a href="{{ url('/auth/login') }}">Login</a></li>
          <li><a href="{{ url('/auth/register') }}">Register</a></li>
          @else
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
            </ul>
          </li>
          @endif
        </ul>
      </div>
    </div>
  </nav>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-heading">Web Mall</div>

            <div class="panel-body">
              <a href="/tipo"><h3>Tipo</h3></a>
              <a href="/categoria"><h3>Categoría</h3></a>
              <a href="/tag"><h3>Tag</h3></a>
              <a href="/estado"><h3>Estado</h3></a>
              <a href="/ciudad"><h3>Ciudad</h3></a>
              <a href="/ubicacion"><h3>Ubicación</h3></a>
              <a href="/plaza"><h3>Plaza</h3></a>
              <a href="/noticia"><h3>Noticia</h3></a>
              <a href="/local"><h3>Local</h3></a>
              <a href="/tienda"><h3>Tienda</h3></a>
              
            </div>
          </div>
        
      </div>

      <div class="col-md-9">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                @yield('breadcrums')</div>

                <div class="panel-body">
                  
                    <div id="map"></div>



                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>


  <!-- Scripts -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    
    <!-- Select Plugin Js -->
    <script src="{{ asset('/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Dropzone Plugin Js -->
    <script src="{{ asset('plugins/dropzone/dropzone.js')}}"></script>


  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHZgOIitZq4ZAuC9kZxb2a3s80PYITTds&callback=initMap" 
  async defer></script>
  <script>
    var map;
    function initMap() {
          alert('mapa');
      map = new google.maps.Map(document.getElementById('map123'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 8
      });
      alert('fin');
    }
  </script>

</body>
</html>
