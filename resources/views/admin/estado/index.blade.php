@extends('admin.layout')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('content')

<div class="row">
	<div class="col-sm-6">			
		<div class="content">
			<h2>Estados</h2>					
			<a href="{{ADMIN_ROUTE}}estado/create" class="btn btn-info btn-fill btn-wd">Nuevo Estado</a>					
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="content">                	
				<div class="row">
					<div class="body table-responsive all">
						<table id="tabla_alumno" class="table table-hover">
							<thead>
								<tr>
									<th>@sortablelink('nombre','NOMBRE')</th>
									<th>EDITAR</th>
									<th>ELIMINAR</th>
								</tr>
							</thead>
							<tbody>
								@foreach($estados as $estado)
								<tr>
									<td>
										<a href="{{ADMIN_ROUTE}}estado/{{$estado->id}}">{{$estado->nombre}}</a>
									</td>

									<td>
										<a class="btn btn-warning btn-fill" href="{{ADMIN_ROUTE}}estado/{{$estado->id}}/edit" >
											<span class="ti-pencil"></span>
										</a>
									</td>
									<td>
										{{Form::model($estado,['method'=>'DELETE','route'=>['estado.destroy',$estado->id]])}}
										<button class="btn btn-danger btn-fill" >
											<span class="ti-close"></span>
										</button>
										{{Form::close()}}
									</td>
								</tr> 
								@endforeach
							</tbody>
						</table>
						{{ $estados->appends(\Request::except('page'))->render() }}
					</div>
				</div>
			</div>
			<div class="card-footer">
			</div> 
		</div>
	</div>
</div>


@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.nav li').removeClass('active');
		$('.estado').addClass('active');

	});
</script>
@endsection	