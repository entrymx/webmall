@extends('admin.layout')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
    <style>
		.carousel-inner img {
			margin: auto;
		}
	</style>
@endsection

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<div class="content">
				{{Form::model($estado,['method'=>'DELETE','route'=>['estado.destroy',$estado->id]])}}
					<h2>Estado - {{$estado->nombre}}
						<a class="btn btn-warning btn-fill pull-right" href="{{$estado->id}}/edit" >
							<span class="ti-pencil"></span>
						</a>
						<button class="btn btn-danger btn-fill pull-right" >
							<span class="ti-close"></span>
						</button>
					</h2>			
				{{Form::close()}}
			</div>
		</div>
	</div>

	<div class="card ">
		<div class="header">
			<h4 class="title">CIUDADES - {{$estado->nombre}}</h4>
		</div>
		<div class="content table-responsive ciudades">			
		</div>
	</div>

	<div class="card">
		<div class="header">
            <h4 class="title">PLAZAS - {{$estado->nombre}}</h4>
        </div>
		<div class="content table-responsive plazas ">
			
		</div>
	</div>

	<div class="card">
		<div class="header">
            <h4 class="title">TIENDAS - {{$estado->nombre}}</h4>
        </div>
		<div class="content table-responsive tiendas ">
			
		</div>
	</div>

@endsection

@section('scripts')


    <script type="text/javascript">

		function ciudades(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}

			$.ajax({
				url: "{{ADMIN_ROUTE}}estado/ciudades"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					id: {{$estado->id}}
				}
			})
			.done(function(data) {
				$('.ciudades').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}

		function plazas(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}

			$.ajax({
				url: "{{ADMIN_ROUTE}}estado/plazas"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					id: {{$estado->id}}
				}
			})
			.done(function(data) {
				$('.plazas').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}

		function tiendas(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}

			$.ajax({
				url: "{{ADMIN_ROUTE}}estado/tiendas"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					id: {{$estado->id}}
				}
			})
			.done(function(data) {
				$('.tiendas').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}

		$(document).ready(function() {

			$(document).on('click', '.ciudades .pagination a, .ciudades th a', function (e) {
				ciudades($(this).attr('href').split('sort')[1]);
				e.preventDefault();
			});

			$(document).on('click', '.tiendas .pagination a, .tiendas th a', function (e) {
				tiendas($(this).attr('href').split('sort')[1]);
				e.preventDefault();
			});

			$(document).on('click', '.plazas .pagination a, .plazas th a', function (e) {
				plazas($(this).attr('href').split('sort')[1]);
				e.preventDefault();
			});

			$('.nav li').removeClass('active');
			$('.tienda').addClass('active');

			ciudades('');
			plazas('');
			tiendas('');
	       
		});
	</script>



@endsection