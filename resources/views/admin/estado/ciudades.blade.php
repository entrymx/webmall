
<div class="content table-responsive ciudades">			
	<table id="tabla_alumno" class="table table-hover">
		<thead>
			<tr>
				<th>@sortablelink('nombre','CIUDAD')</th>
				<th>@sortablelink('estado.nombre','ESTADO')</th>
				<th>EDITAR</th>
				<th>ELIMINAR</th>
			</tr>
		</thead>
		<tbody>
			@foreach($ciudades as $ciudad)
			<tr>
				<td>
					<a href="{{ADMIN_ROUTE}}ciudad/{{$ciudad->id}}">{{$ciudad->nombre}}</a>
				</td>

				<td>
					{{$ciudad->estado->nombre}}
				</td>

				<td>
					<a class="btn btn-warning btn-fill" href="{{ADMIN_ROUTE}}ciudad/{{$ciudad->id}}/edit" >
						<span class="ti-pencil"></span>
					</a>
				</td>
				<td>
					{{Form::model($ciudad,['method'=>'DELETE','route'=>['ciudad.destroy',$ciudad->id]])}}
					<button class="btn btn-danger btn-fill" >
						<span class="ti-close"></span>
					</button>
					{{Form::close()}}
				</td>
			</tr> 
			@endforeach
		</tbody>
	</table>
	{{ $ciudades->appends(\Request::except('page'))->render() }}		
</div>