@extends('admin.layout')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<h2>Agregar Estado</h2>
		<div class="card">
			<div class="content">
				<div class="body ">

					{{Form::open(['url' => ADMIN_ROUTE . 'estado'])}}

					<fieldset>							
						<div class="form-group">
							<div class=" {{ $errors->has('nombre') ? 'has-error' : '' }}">
								{{ Form::label('nombre', 'Nombre') }}
								{{ Form::text('nombre', old('nombre'), array('class' => 'form-control border-input')) }}													
								<span class="text-danger">{{ $errors->first('nombre') }}</span>
							</div>	
						</div>
						<button class="btn btn-info btn-fill form-control">Guardar</button>
						<div class="gap-1"></div>
						<br>
						<a href="{{ADMIN_ROUTE}}estado" class="btn btn-danger btn-fill form-control">Cancelar</a>
					</fieldset>

					{{Form::close()}}

				</div>  
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.nav li').removeClass('active');
		$('.estado').addClass('active');

	});
</script>
@endsection	