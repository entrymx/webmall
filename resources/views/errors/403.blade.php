<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>España Online</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: white;
                display: table;
                font-weight: 100;
                font-family: 'Roboto', sans-serif;

                background-color: #3f51b5;

            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">ACCESO DENEGADO</div>
                <h2>ERROR 403</h2>
                <h3>No cuenta con los permisos para acceder a este contenido.</h3>
                <br>
                <a class="btn btn-primary" href="/">Ir al Inicio</a>
                <br><br><br>
                    
                @if(\Sentinel::guest())
                    <a class="btn btn-primary" href="/login">Inicia Sesion</a>
                @else
                    <br>
                    <a class="btn btn-primary" href="/logout">Cerrar Sesion</a>                
                @endif
            </div>
        </div>
    </body>
</html>
