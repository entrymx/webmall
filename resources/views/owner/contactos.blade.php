	<div class="body table-responsive contactos">
		<table id="tabla_alumno" class="table table-hover">
			<thead>
				<tr>
					<th>@sortablelink('nombres','NOMBRES')</th>
					<th>@sortablelink('apellidos','APELLIDOS')</th>
					<th>@sortablelink('correo','CORREO')</th>
					<th>@sortablelink('telefono','TELÉFONO')</th>
					<th>@sortablelink('tienda.nombre','TIENDA')</th>
				</tr>
			</thead>
			<tbody>
				@foreach($ctiendas as $ctienda)
				<tr>
					<td>
						{{$ctienda->nombres}}
					</td>

					<td>
						{{$ctienda->apellidos}}
					</td>

					<td>
						{{$ctienda->correo}}
					</td>

					<td>
						{{$ctienda->telefono}}
					</td>

					<td>
						@if($ctienda->tienda)
						{{$ctienda->tienda->nombre}}
						@else
						Sin Tienda
						@endif
					</td>
				</tr> 
				@endforeach
			</tbody>
		</table> 
		{{ $ctiendas->appends(\Request::except('page'))->render() }}
	</div>