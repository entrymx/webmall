@extends('owner.layout')

@section('head')
    <style>
		#map {
			height: 350px;
		}

		.carousel-inner img {
			margin: auto;
		}
	</style>
@endsection

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<div class="content">
				<h2>Tienda - {{$tienda->nombre}}
					<a class="btn btn-warning btn-fill pull-right" href="{{$tienda->id}}/edit" >
						<span class="ti-pencil"></span>
					</a>
				</h2>
			</div>
		</div>
	</div>


	<div class="card">
		<div class="content">
			<div class="row">
				<div class="col-sm-2">
					<label>Plaza</label>
					<p>{{$tienda->local->plaza->nombre}}</p>
				</div>
				<div class="col-sm-2">
					<label>Local</label>
					<p>{{$tienda->local->nombre}}<br>{{$tienda->local->tipo->nombre}}</p>
				</div>
				<div class="col-sm-2">
					<label>Nombre</label>
					<p>{{$owner->nombre}}</p>
				</div>
				<div class="col-sm-2">
					<label>Tags</label>
					<p>
					@foreach( explode('","', substr ( $owner->tags , 2 , - 2 ) ) as $tag)
						{{$tag}}<br>
					@endforeach
					</p>
				</div>
				<div class="col-sm-2">
					<label>Categoría</label>
					<p>{{$owner->categoria}}</p>
				</div>
				<div class="col-sm-1">
					<label>Apertura</label>
					<p>{{$owner->apertura}}</p>
				</div>
				<div class="col-sm-1">
					<label>Cierre</label>
					<p>{{$owner->cierre}}</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-3">
			<div class="card">
				<div class="header">
					<h4 class="title">Logo</h4>
				</div>
				<div class="content">
					<div class="form-group">
						@if($owner->logo != "")
						<a href="/tie/{{($owner->logo)}}" data-lightbox="logo-o-{{($owner->id)}}" data-title="{{$owner->nombre}}">
							<img class="img-responsive" src="/tie/{{($owner->logo) }}">
						</a>
						@endif
					</div>
				</div>
			</div>			
		</div>

		<div class="col-sm-9">
			<div class="card">
				<div class="header">
					<h4 class="title">Imágenes</h4>
				</div>
				<div class="content">
					<div class="row">
						@if($owner->imagen->count() > 1)
							@foreach($owner->imagen as $imagen)
								<div class="col-sm-3">
									<a href="/tie/{{($imagen->imagen) }}" data-lightbox="o-imagen-{{($owner->id)}}" data-title="{{$imagen->nombre}}">
										<img src="/tie/{{($imagen->imagen) }}" class="img-responsive pull-center">
									</a>
								</div>
							@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="card ">
		<div class="header">
			<h4 class="title">COMENTARIOS</h4>
		</div>
		<div class="content table-responsive contactosOwner">
			<p>{!!$owner->comentarios!!}</p>
		</div>
	</div>



	<div class="row">
		<div class="col-sm-12">
			<div class="content">
				<h2>Estado Acutal de la Tienda - {{$tienda->nombre}}</h2>
			</div>
		</div>
	</div>


	<div class="card">
		<div class="content">
			<div class="row">
				<div class="col-sm-2">
					<label>Plaza</label>
					<p>{{$tienda->local->plaza->nombre}}</p>
				</div>
				<div class="col-sm-2">
					<label>Local</label>
					<p>{{$tienda->local->nombre}}<br>{{$tienda->local->tipo->nombre}}</p>
				</div>
				<div class="col-sm-2">
					<label>Nombre</label>
					<p>{{$tienda->nombre}}</p>
				</div>
				<div class="col-sm-2">
					<label>Tags</label>
					<p>
					@foreach($tienda->tag->sortBy('nombre') as $tag)
						{{$tag->nombre}}<br>
					@endforeach
					</p>
				</div>
				<div class="col-sm-2">
					<label>Categoría</label>
					<p>{{$tienda->categoria->nombre}}</p>
				</div>
				<div class="col-sm-1">
					<label>Apertura</label>
					<p>{{$tienda->apertura}}</p>
				</div>
				<div class="col-sm-1">
					<label>Cierre</label>
					<p>{{$tienda->cierre}}</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-3">
			<div class="card">
				<div class="header">
					<h4 class="title">Logo</h4>
				</div>
				<div class="content">
					<div class="form-group">
						@if($tienda->logo != "")
						<a href="/tie/{{($tienda->logo)}}" data-lightbox="logo-{{($tienda->id)}}" data-title="{{$tienda->nombre}}">
							<img class="img-responsive" src="/tie/{{($tienda->logo) }}">
						</a>
						@endif
					</div>
				</div>
			</div>			
		</div>

		<div class="col-sm-9">
			<div class="card">
				<div class="header">
					<h4 class="title">Imágenes</h4>
				</div>
				<div class="content">
					<div class="row">
						@if($tienda->imagen->count() > 1)
							@foreach($tienda->imagen as $imagen)
								<div class="col-sm-3">
									<a href="/tie/{{($imagen->imagen) }}" data-lightbox="imagen-{{($tienda->id)}}" data-title="{{$tienda->nombre}}">
										<img src="/tie/{{($imagen->imagen) }}" class="img-responsive pull-center">
									</a>
								</div>
							@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="card ">
		<div class="header">
			<h4 class="title">CONTACTOS - {{$tienda->nombre}}</h4>
		</div>
		<div class="content table-responsive contactos">			
		</div>
	</div>

@endsection

@section('scripts')


    <script type="text/javascript">

		function contactos(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}

			$.ajax({
				url: "{{OWNER_ROUTE}}tienda/contactos"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					id: {{$tienda->id}}
				}
			})
			.done(function(data) {
				$('.contactos').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}

		$(document).ready(function() {

			$(document).on('click', '.contactos .pagination a, .contactos th a', function (e) {
				contactos($(this).attr('href').split('sort')[1]);
				e.preventDefault();
			});

			$('.nav li').removeClass('active');
			$('.tienda').addClass('active');

			contactos('');
	       
		});
	</script>



@endsection