@extends('owner.layout')

@section('head')
		<link rel="stylesheet" type="text/css" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-6">			
			<div class="content">
				<h2>Tiendas</h2>
			</div>
		</div>
		<!--div class="col-sm-6">			
			<div class="pull-right">
				<br><br><br><br>
				{{ Form::label('buscar', 'Buscar'   , array('class' => '')) }}
				{{ Form::text('buscar', old('buscar'), array('class' => ' border-input ', 'placeholder' => 'Teclea para buscar')) }}
				<a class="btn btn-info btn-fill btn-wd buscar ">Buscar</a>
			</div>
		</div-->
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="content table-responsive tiendas">
					
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">

		function tiendas(sort) {

			if(sort == 'undefined' || sort == ''){
				sort = "";
			}else{
				sort = "?sort"+sort;
			}
			$.ajax({
				url: "{{OWNER_ROUTE}}tiendas"+sort,
				type: "POST",
				data:{
					_token: "{{ csrf_token() }}",
					text: $('#buscar').val()
				}
			})
			.done(function(data) {
				$('.tiendas').html(data);
			})
			.error(function(data) {
				alert("Error, Ocurrió un problema, error");
			});
		}


		$(document).on('click', '.buscar', function (e) {
			tiendas('');
			e.preventDefault();
		});

		$(document).on('keypress', '#buscar', function (e) {
			if(e.which == 13) {
				e.preventDefault();
				tiendas('');
			}
		});

		$( ".buscar" ).keypress(function() {
			console.log( "Handler for .keypress() called." );
		});

		$(document).on('click', '.tiendas .pagination a, .tiendas th a', function (e) {
			tiendas($(this).attr('href').split('sort')[1]);
			e.preventDefault();
		});

		$(document).ready(function(){
			$('.nav li').removeClass('active');
			$('.tienda').addClass('active');
			tiendas('');
		});
	</script>
@endsection