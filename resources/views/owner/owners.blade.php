	
	<div class="content table-responsive tiendas">
		<table id="tabla_alumno" class="table table-hover">
			<thead>
				<tr>
					<th>@sortablelink('nombre','NOMBRE')</th>
					<th>@sortablelink('local.nombre','LOCAL')</th>
					<th>@sortablelink('apertura','HORARIO')</th>
					<th>@sortablelink('categoria.nombre','CATEGORÍA')</th>
					<th>@sortablelink('local.plaza_id','PLAZA')</th>
					<th>TAGS</th>
					<th>CONTACTO</th>
					<th>LOGO</th>
					<th>FOTOS</th>
					<th>EDITAR</th>
				</tr>
			</thead>
			<tbody>
				@foreach($tiendas as $tienda)
				<tr>
					<td>
						<a href="{{OWNER_ROUTE}}tienda/{{$tienda->id}}">{{$tienda->nombre}}</a>
					</td>
					<td>
						@if($tienda->local)
						{{$tienda->local->nombre}}
						@else
						Sin Local
						@endif
					</td>
					<td>
						{{$tienda->apertura." - ".$tienda->cierre}}
					</td>
					<td>
						{{$tienda->categoria->nombre}}
					</td>
					<td>
						{{$tienda->local->plaza->nombre}}
					</td>
					<td>	
						@foreach($tienda->tag->sortBy('nombre') as $tag)
						{{$tag->nombre}}
						@endforeach			                            
					</td>
					<td>
					@if($tienda->ctienda->count() > 0)
					@foreach($tienda->ctienda as $contacto)
						&#9702; {{$contacto->nombres." ".$contacto->apellidos}}<br>
					@endforeach
					@else
						Sin contacto
					@endif							
					</td>
					<td class="logo">
						@if($tienda->logo != "")
						<a href="/tie/{{($tienda->logo) }}" data-lightbox="logo-{{($tienda->id)}}" data-title="{{$tienda->nombre}}" ><img class="img-responsive" src="/tie/{{($tienda->logo) }}"></a>
						@else
						Sin Logo
						@endif
					</td>
					<td>	
						<div id="myCarousel-{{$loop->iteration}}" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
							@foreach($tienda->imagen as $imagen)
								<div class="item @if($loop->index < 1) active @endif">
									<a href="/tie/{{($imagen->imagen) }}" data-lightbox="imagen-{{($tienda->id)}}" data-title="{{$tienda->nombre}}" >
										<img src="/tie/{{($imagen->imagen) }}" class="img-responsive">
									</a>
									<br>									
								</div>										
							@endforeach
							</div>
						</div>
					</td>
					
					<td>
						<a class="btn btn-warning btn-fill" href="{{OWNER_ROUTE}}{{$tienda->id}}/edit" >
							<span class="ti-pencil"></span>
						</a>
					</td>
				</tr> 
				@endforeach
			</tbody>
		</table> 
		{{ $tiendas->appends(\Request::except('page'))->render() }}
	</div>