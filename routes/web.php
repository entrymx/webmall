<?php

	Route::get('/test',function(){

		//return \view('public.test');

		if ($user = \Sentinel::getUser())
		{
			dump($user->first_name);
			dump($user->last_name);
			dump($user->email);
			dump($user->id);

			dump(Webmall\User::find($user->id));
		}
		else{
		
			dump('admin@asd.com');
			dump('zx4aF61M');

	    	return ('sin sesión');		
		}

	});

	Route::get('/migrate',function(){
		
		//Creación de Roles
			$admin = Sentinel::getRoleRepository()->createModel()->create(
				[
			    'name' => 'Administrador',
			    'slug' => 'admin',
				]);
	
			$owner = Sentinel::getRoleRepository()->createModel()->create(
				[
			    'name' => 'Owner',
			    'slug' => 'owner',
				]);
	
		//Creación de Admins

			$credentials = [
			    'first_name'    => 'Rojo',
			    'last_name'    => 'Advertising',
			    'email'    => 'web@rojoad.com',
			    'password' => 'pass',
			];
			$user = Sentinel::registerAndActivate($credentials);
			$admin->users()->attach($user);

			$credentials = [
			    'first_name'    => 'Dueño',
			    'last_name'    => 'De tienda',
			    'email'    => 'owner@rojoad.com',
			    'password' => 'pass',
			];
			$user = Sentinel::registerAndActivate($credentials);
			$owner->users()->attach($user);
	});	


/*
	PUBLIC SECTION
*/


	Route::get('/', 'BuscadorController@index'); 
	Route::get('/p/{id}','BuscadorController@plaza');
	Route::get('/t/{id}','BuscadorController@tienda');
	Route::post('/buscar','BuscadorController@buscar');
	Route::post('/sugerencia','BuscadorController@sugerencia');
	Route::post('/getTiendas','BuscadorController@getTiendas');



	//Login

	Route::get('/login' , 'SesionController@loginGet');
	Route::post('/login', 'SesionController@loginPost');
	Route::get('logout','SesionController@logout');


	Route::post('login/olvidada', 'SesionController@olvidada');
	Route::get('/login/{id}/{hash}', 'SesionController@restaurar');
	Route::get('/login/{id}', 'SesionController@restaurarAdmin');


//Owner

	Route::group(['middleware' => 'owner'], function(){
		
		Route::get(OWNER_ROUTE ,function(){
			return redirect(OWNER_ROUTE.'tienda');
		});

		Route::resource(OWNER_ROUTE.'tienda' ,'OwnerController');
		Route::PATCH(OWNER_ROUTE.'tienda/{tienda}' ,'OwnerController@update')->name('o.update');
		Route::post    (OWNER_ROUTE . 'tiendas','OwnerController@tiendas');
		Route::post    (OWNER_ROUTE . 'tienda/contactos','OwnerController@contactos');

	});
//Admin

	Route::group(['middleware' => 'admin'], function(){

		Route::resource(ADMIN_ROUTE ,'TiendaController');

		Route::resource(ADMIN_ROUTE . 'categoria','CategoriaController');
		Route::get     (ADMIN_ROUTE . 'categoria/borrarImagen/{id}','CategoriaController@borrarImagen');

		Route::resource(ADMIN_ROUTE . 'ciudad','CiudadController');
		Route::post    (ADMIN_ROUTE . 'ciudad/plazas','CiudadController@plazas');
		Route::post    (ADMIN_ROUTE . 'ciudad/tiendas','CiudadController@tiendas');
		
		Route::resource(ADMIN_ROUTE . 'contacto','ContactoController');

		Route::resource(ADMIN_ROUTE . 'ctienda','CtiendaController');

		Route::resource(ADMIN_ROUTE . 'owner','OwnerAdminController');
		Route::post    (ADMIN_ROUTE . 'owner/owners','OwnerAdminController@owners');

		Route::resource(ADMIN_ROUTE . 'estado','EstadoController');
		Route::post    (ADMIN_ROUTE . 'estado/ciudades','EstadoController@ciudades');
		Route::post    (ADMIN_ROUTE . 'estado/plazas','EstadoController@plazas');
		Route::post    (ADMIN_ROUTE . 'estado/tiendas','EstadoController@tiendas');

		Route::resource(ADMIN_ROUTE . 'local','LocalController');
		Route::post    (ADMIN_ROUTE . 'local/locales','LocalController@locales');

		Route::resource(ADMIN_ROUTE . 'noticia','NoticiaController');
		Route::get     (ADMIN_ROUTE . 'noticia/borrarImagen/{id}','NoticiaController@borrarImagen');

		Route::post    (ADMIN_ROUTE . 'plaza/contactos','PlazaController@contactos');
		Route::post    (ADMIN_ROUTE . 'plaza/locales','PlazaController@locales');
		Route::post    (ADMIN_ROUTE . 'plaza/noticias','PlazaController@noticias');
		Route::post    (ADMIN_ROUTE . 'plaza/tiendas','PlazaController@tiendas');
		Route::resource(ADMIN_ROUTE . 'plaza','PlazaController');
		Route::get     (ADMIN_ROUTE . 'plaza/borrarImagen/{id}','PlazaController@borrarImagen');
		Route::get     (ADMIN_ROUTE . 'plaza/borrarLogo/{id}','PlazaController@borrarLogo');

		Route::resource(ADMIN_ROUTE . 'tag','TagController');
		
		Route::post    (ADMIN_ROUTE . 'tienda/contactos','TiendaController@contactos');
		Route::post    (ADMIN_ROUTE . 'tienda/locales','TiendaController@locales');
		Route::resource(ADMIN_ROUTE . 'tienda','TiendaController');
		Route::get     (ADMIN_ROUTE . 'tienda/borrarImagen/{id}','TiendaController@borrarImagen');
		Route::get     (ADMIN_ROUTE . 'tienda/borrarLogo/{id}','TiendaController@borrarLogo');
		Route::get     (ADMIN_ROUTE . 'tienda/t/tags' ,'TiendaController@tags');
		Route::get     (ADMIN_ROUTE . 'tienda/tags' ,'TiendaController@tags');
		Route::post    (ADMIN_ROUTE . 'tienda/tiendas','TiendaController@tiendas');

		Route::resource(ADMIN_ROUTE . 'tipo','TipoController');

		Route::resource(ADMIN_ROUTE . 'ubicacion','UbicacionController');
	});
